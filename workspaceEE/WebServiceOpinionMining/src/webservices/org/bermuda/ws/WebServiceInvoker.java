package webservices.org.bermuda.ws;

import webservices.org.bermuda.ws.NlpStub.ProcessText;
import webservices.org.bermuda.ws.NlpStub.ProcessTextE;
import webservices.org.bermuda.ws.NlpStub.ProcessTextResponse;

public class WebServiceInvoker {
	
	public String queryServer(String input) throws Exception {
		System.out.println("input: " + input);
		NlpStub serviceTextStub = new NlpStub();
		ProcessTextE process = new NlpStub.ProcessTextE();
		NlpStub.ProcessTextResponseE processResp = new NlpStub.ProcessTextResponseE();
		ProcessTextResponse output = null;
	 
		try {
			ProcessText pt = new ProcessText();
			pt.setText(input);
			pt.setModel("ro");
			process.setProcessText(pt);
			processResp = serviceTextStub.processText(process);
			output = processResp.getProcessTextResponse();
		} catch (org.apache.axis2.AxisFault e) {
			System.out.println(e.toString());
		}
		
		return output.get_return();
	}
	
	public static void main(String[] args) throws Exception {
		WebServiceInvoker wbs = new WebServiceInvoker();
//		String str = "Ponta a făcut aluzie la ultimele declaraţii ale premierului şi ale preşedintelui, primul vorbind despre reducerea CAS în"
//				+ " timp ce şeful statului a spus că soluţia corectă este \\\„reîntregirea salariilor bugetarilor\\\„. În cele din urmă,"
//				+ " Coaliţia de guvernământ a ales să susţină majorarea salariilor bugetarilor cu 15% începând de la 1 iunie.;;;";
//		System.out.println(wbs.queryServer(" Terapia cu celule stem poate aduce beneficii în neurologie, prin încetinirea evoluţiei bolilor degenerative Alzheimer şi Parkinson"));
		System.out.println(wbs.queryServer("Traian Băsescu a fost ales președintele României chiar dacă locuitorii țării nu își doreau acest lucru."));
	}
}
