package dependencyutils;

public class DependencyConstants {
	/**
	 * Here are all the possible features. A feature is represented by a word.
	 * The feature type is determined depending on its score in the polarity
	 * database
	 * @author andreea.badoiu
	 *
	 */
	public static enum Features {
		NOT_RELEVANT {
			@Override
			public String toString() {
				return "Not_relevant";
			}
		},
		
		ENTITY {
			@Override
			public String toString() {
				return "Entity";
			}
		},
		
		NEUTRAL {
			@Override
			public String toString() {
				return "Neutral";
			}
		}, 
		
		POSITIVE {
			@Override
			public String toString() {
				return "Positive";
			}
		}, 
		
		NEGATIVE {
			@Override
			public String toString() {
				return "Negative";
			}
		}
	}
}
