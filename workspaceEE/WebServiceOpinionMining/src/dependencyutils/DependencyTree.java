package dependencyutils;

import java.util.ArrayList;

/**
 * 
 * @author andreea.badoiu
 *
 */
public class DependencyTree {
	private String 			uniqueId;
	private String			word;
	private String 			lemma;
	private String 			pos;
	private String 			gender;
	private String 			number;
	private String 			type;	//common etc.
	private String			person;
	private String			deprel;
	private int				phraseIndex;
	private int 			wordIndex;
	private int				head;
	private DependencyTree 	parent;
	private boolean			visited;
	
	private ArrayList<DependencyTree> children;
	
	public DependencyTree() {
		this.word = null;
		this.lemma = null;
		this.pos = null;
		this.gender = null;
		this.number = null;
		this.type = null;
		this.person = null;
		this.deprel = null;
		children = new ArrayList<>();
		this.parent = null;
		this.visited = false;
	}
	
	public DependencyTree(String word, String lemma, String pos, String gender,
						  String number, String type, String person,
						  String deprel, int phraseIndex, int wordIndex,
						  int head, String uniqueId) {
		this.uniqueId = uniqueId;
		this.word = word;
		this.lemma = lemma;
		this.pos = pos;
		this.gender = gender;
		this.number = number;
		this.type = type;
		this.person = person;
		this.deprel = deprel;
		this.phraseIndex = phraseIndex;
		this.wordIndex = wordIndex;
		this.head = head;
		this.visited = false;
		children = new ArrayList<>();
	}
	
	@Override
	public String toString() {
		return "[" + "word: " + word + ", " +
				"uniqueId: " + uniqueId + ", " +
				"lemma: " + lemma + ", " +
				"pos: " + pos + ", " +
				"gender: " + gender + ", " + 
				"number: " + number + ", " +
				"type: " + type + ", " + 
				"person: " + person + ", " +
				"deprel: " + deprel + ", " +
				"phraseIndex: " + phraseIndex + ", " + 
				"wordIndex: " + wordIndex + ", " +
				"head: " + head + ", " +
				"visited: " + visited + "]";
	}
	
	public String getLemma() {
		return lemma;
	}

	public void setLemma(String lemma) {
		this.lemma = lemma;
	}

	public String getPos() {
		return pos;
	}

	public void setPos(String pos) {
		this.pos = pos;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPerson() {
		return person;
	}

	public void setPerson(String person) {
		this.person = person;
	}

	public String getDeprel() {
		return deprel;
	}

	public void setDeprel(String deprel) {
		this.deprel = deprel;
	}
	
	public int getWordIndex() {
		return wordIndex;
	}

	public void setWordIndex(int wordIndex) {
		this.wordIndex = wordIndex;
	}

	public ArrayList<DependencyTree> getChildren() {
		return children;
	}

	public void setChildren(ArrayList<DependencyTree> children) {
		this.children = children;
	}
	
	public void addChild(DependencyTree child) {
		children.add(child);
	}
	
	public void addChildren(ArrayList<DependencyTree> children) {
		this.children.addAll(children);
	}
	
	public DependencyTree getChild(int index) {
		return children.get(index);
	}
	
	public int getNumberOfChildren() {
		return children.size();
	}

	public int getPhraseIndex() {
		return phraseIndex;
	}

	public void setPhraseIndex(int phraseIndex) {
		this.phraseIndex = phraseIndex;
	}

	public int getHead() {
		return head;
	}

	public void setHead(int head) {
		this.head = head;
	}
	
	public boolean hasChildren() {
		return !children.isEmpty();
	}

	public String getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}

	public DependencyTree getParent() {
		return parent;
	}

	public void setParent(DependencyTree parent) {
		this.parent = parent;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public boolean isVisited() {
		return visited;
	}

	public void setVisited(boolean visited) {
		this.visited = visited;
	}
}
