package dependencyutils;

import java.util.ArrayList;
import java.util.Iterator;

import org.apache.commons.lang.builder.HashCodeBuilder;

import dependencyutils.DependencyConstants.Features;

public class DependencyPattern {
	private ArrayList<FeatureConnection> pattern = new ArrayList<>();

	public ArrayList<FeatureConnection> getPattern() {
		return pattern;
	}

	public void setPattern(ArrayList<FeatureConnection> pattern) {
		this.pattern = pattern;
	}

	public void addFeatureConnection(FeatureConnection feature) {
		pattern.add(feature);
	}

	@Override
	public boolean equals(Object obj) {
		@SuppressWarnings("unchecked")
		//ArrayList<FeatureConnection> features = (ArrayList<FeatureConnection>) obj;
		DependencyPattern p = (DependencyPattern) obj;
		ArrayList<FeatureConnection> features = p.getPattern();
		if (features.size() != pattern.size())
			return false;

		for (int i = 0; i < features.size(); i++) {
			FeatureConnection feature = pattern.get(i);
			FeatureConnection feature1 = features.get(i);
			if (feature1 == null && feature == null)
				return false;
			if (feature == null && feature1 != null ||
					feature != null && feature == null)
				return false;
			if (!feature1.equals(feature))
				return false;	
		}
		return true;
	}

	@Override
	public int hashCode() {
		HashCodeBuilder hashCodeBuilder = new HashCodeBuilder(17, 31);
		for (int i = 0; i < pattern.size(); i++) {
			hashCodeBuilder
			.append(pattern.get(i).getFeature().toString())
			.append(pattern.get(i).getConnection());
		}
		return hashCodeBuilder.toHashCode();
	}
	
	/**
	 * Remove all the neutral features from the end of the pattern
	 * 
	 */
	public void trimLastNeutral() {
		int n = pattern.size();
		for (int i = n - 1; i >= 0; i--) {
			FeatureConnection feature = pattern.get(i);
			Features f = feature.getFeature();
			if (f == Features.NEUTRAL || f == Features.NOT_RELEVANT) {
				pattern.remove(i);
			} else {
				return;
			}
		}
	}
	
	/**
	 * Remove all the neutral features from the beginning of the pattern
	 * 
	 */
	public void trimFirstNeutral() {
		Iterator<FeatureConnection> it = pattern.iterator();
		while (it.hasNext()) {
			FeatureConnection feature = it.next();
			Features f = feature.getFeature();
			if (f.equals(Features.NEUTRAL.toString())) {
				it.remove();
			} else {
				return;
			}
		}
	}
	
	/**
	 * Check if the pattern contains only neutral components
	 * @return
	 */
	public boolean isAllNeutral() {
		Iterator<FeatureConnection> it = pattern.iterator();
		while (it.hasNext()) {
			FeatureConnection feature = it.next();
			Features f = feature.getFeature();
			if (!f.equals(Features.NEUTRAL.toString())) {
				return false;
			} 
		}
		return true;
	}

	@Override
	public String toString() {
		return pattern.toString();
	}
}
