package dependencyutils;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.TreeMap;

import javax.xml.parsers.ParserConfigurationException;

import general.GeneralConstants;
import general.GeneralConstants.Parsers;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

import utils.UtilsConstants;
import utils.XMLUtils;

/**
 * Get the text and generate the dependency tree
 * @author andreea.badoiu
 *
 */
public class DependencyTreeGenerator {
	private static Integer uniqueId = 0; 
	private static Integer edgeid = 0;
	private static int width = 0;
	
	public static TreeMap<Integer, ArrayList<DependencyTree>> getDepHashMap(NodeList sentences, int sentence) {
		NodeList nodes = sentences.item(sentence).getChildNodes();
		TreeMap<Integer, ArrayList<DependencyTree>> wordsDependencies;
		wordsDependencies = new TreeMap<>();
		
		for (int i = 0; i < nodes.getLength(); i++) {
			Node node = nodes.item(i);
			NamedNodeMap nnm;
			if (node instanceof Text && ((Text) node).getData().trim().length() == 0) {
				continue;
			} else {
				nnm = node.getAttributes();
				uniqueId++;
			}
			String word = null;
			String lemma = null;
			String pos = null;
			String gender = null;
			String number = null;
			String type = null;
			String person = null;
			String deprel = null;
			
			word = node.getTextContent();
			
			if (nnm.getNamedItem("LEMMA") != null) {
				lemma = nnm.getNamedItem("LEMMA").getNodeValue();
			}
			if (nnm.getNamedItem("POS") != null) {
				pos = nnm.getNamedItem("POS").getNodeValue();
			}
			if (nnm.getNamedItem("Gender") != null) {
				gender = nnm.getNamedItem("Gender").getNodeValue();
			}
			if (nnm.getNamedItem("Number") != null) {
				number = nnm.getNamedItem("Number").getNodeValue();
			}
			if (nnm.getNamedItem("Type") != null) {
				type = nnm.getNamedItem("Type").getNodeValue();	
			}
			if (nnm.getNamedItem("Person") != null) {
				person = nnm.getNamedItem("Person").getNodeValue();
			}
			if (nnm.getNamedItem("deprel") != null) {
				deprel = nnm.getNamedItem("deprel").getNodeValue();
			}

			String id = nnm.getNamedItem("id").getNodeValue();
			int phraseIndex = Integer.parseInt(id.substring(0, id.indexOf(".")));
			int wordIndex = Integer.parseInt(id.substring(id.indexOf(".") + 1));

			int head = Integer.parseInt(nnm.getNamedItem("head").getNodeValue());

			DependencyTree depTree = new DependencyTree(word, lemma, pos, gender,
					number, type, person,
					deprel, phraseIndex,
					wordIndex, head, uniqueId.toString());
			ArrayList<DependencyTree> depTrees = new ArrayList<>();
			if (wordsDependencies.containsKey(head)) {
				depTrees = wordsDependencies.get(head);
			}
			depTrees.add(depTree);
			wordsDependencies.put(head, depTrees);
		}
		
		return wordsDependencies;
	}

	public static void createTreeFromTreeMap(
			TreeMap<Integer, ArrayList<DependencyTree>> wordsDep,
			DependencyTree tree) {
		
		ArrayList<DependencyTree> children = new ArrayList<>();
		if (tree.getLemma() != null && tree.getLemma().equals("ROOT-NODE")) {
			children = wordsDep.get(0);
			tree.addChildren(children);
			//save the root node for each child
			for (int j = 0; j < children.size(); j++) {
				children.get(j).setParent(tree);
			}
		} else {
			children = tree.getChildren();
		}
		
		for (int i = 0; i < children.size(); i++) {
			int wordIndex = children.get(i).getWordIndex();
			if (wordsDep.containsKey(wordIndex)) {
				children.get(i).addChildren(wordsDep.get(wordIndex));
				for (int j = 0; j < wordsDep.get(wordIndex).size(); j++) {
					wordsDep.get(wordIndex).get(j).setParent(children.get(i));
				}
				createTreeFromTreeMap(wordsDep, children.get(i));
			}
		}
		
	}

	public static DependencyTree generateDepTree(Document doc) {
		Parsers parser = GeneralConstants.PARSER;

		if (!parser.toString().equals(Parsers.UAIC.toString())) {
			throw new RuntimeException("The UAIC parser must be used in order to get dependencies!");
		}

		doc.getDocumentElement().normalize();
		NodeList sentences = doc.getFirstChild().getChildNodes();
		
		DependencyTree tree = new DependencyTree();
		tree.setLemma("ROOT-NODE");
		tree.setUniqueId("ROOT_NODE");

		uniqueId = 1;
		for (int sentence = 0; sentence < sentences.getLength(); sentence++) {
			Node child = sentences.item(sentence);
			if (child instanceof Text && ((Text) child).getData().trim().length() == 0) {
				continue;
			} else {
				TreeMap<Integer, ArrayList<DependencyTree>> wordsDep = getDepHashMap(sentences, sentence);
				DependencyTree sentenceTree = new DependencyTree();
				sentenceTree.setLemma("ROOT-NODE");
				sentenceTree.setUniqueId("SENTENCE-ROOT" + sentence);
				createTreeFromTreeMap(wordsDep, sentenceTree);
				tree.addChild(sentenceTree);
			}
		}

		return tree;
	}

	public static Document getLocalXml(int articleIndex) {
		Parsers parser = GeneralConstants.PARSER;

		String fileName = UtilsConstants.ROOT_FOLDER;

		if (parser.toString().equals(Parsers.UAIC.toString())) {
			fileName += UtilsConstants.UAIC_FOLDER;
		}
		else {
			fileName += UtilsConstants.RACAI_FOLDER;
		}

		fileName += articleIndex + ".xml";
		Document doc = null;
		try {
			System.out.println(fileName);
			doc = XMLUtils.loadXMLFromFile(fileName);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

//		System.out.println(doc.getFirstChild().getAttributes().getNamedItem("id"));
		
		return doc;
	}
	
	public static Document getLocalReviewXml(String filePath) {
		Parsers parser = GeneralConstants.PARSER;

		String fileName = filePath;
		Document doc = null;
		try {
			System.out.println(fileName);
			doc = XMLUtils.loadXMLFromFile(fileName);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

//		System.out.println(doc.getFirstChild().getAttributes().getNamedItem("id"));
		
		return doc;
	}

	/**
	 * 
	 * @param articleText
	 * 			the article of the text for which the dependency tree is going to be generated
	 * @return
	 * 			the xml document corresponding to the parsed output
	 */
	public static Document getXmlFromWeb(String articleText) {
		Parsers parser = GeneralConstants.PARSER;
		articleText = articleText.replace("ş", "ș");

		uaic.webfdgro.WebServiceInvoker wsiUaic = null;
		webservices.org.bermuda.ws.WebServiceInvoker wsiRacai = null;

		if (parser.toString().equals(Parsers.UAIC.toString())) {
			wsiUaic = new uaic.webfdgro.WebServiceInvoker();
		}
		else {
			wsiRacai = new webservices.org.bermuda.ws.WebServiceInvoker();
		}

		articleText = articleText.replaceAll("\\s+", " ");

		String xml = null;
		try {
			xml = wsiUaic != null ? wsiUaic.queryServer(articleText) : wsiRacai.queryServer(articleText);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Document doc = null;
		try {
			if (xml != null) {
				doc = XMLUtils.loadXMLFromString(xml);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return doc;
	}
	
	/**
	 * Create a JSON tree. This is going to be displayed on a web interface using 
	 * sigma-js
	 * @param tree
	 * @param nodes
	 * @param edges
	 * @param level
	 * @throws JSONException
	 */
	public static void exportTreeToJson(DependencyTree tree, JSONArray nodes,
										JSONArray edges, int level) throws JSONException {

		//add the current node to the nodes list
		JSONObject node = new JSONObject();
		node.put("id", tree.getUniqueId());
		String parentLemma = null;
		/*if (tree.getParent() != null) {
			parentLemma = tree.getParent().getLemma();
		}
		node.put("label", tree.getLemma() + ", " + parentLemma);*/
		String deprel = tree.getDeprel();
		String pos = tree.getPos();
		node.put("label", tree.getLemma() + ", " + deprel + ", " + pos);
		node.put("x", width);
		node.put("y", level);
		node.put("size", 2);
		nodes.put(node);
		
		//add the edge with the parent if it is the case
		for (int i = 0; i < tree.getChildren().size(); i++) {
			DependencyTree child = tree.getChild(i);
			JSONObject edge = new JSONObject();
			edge.put("id", edgeid.toString());
			edgeid++;
			edge.put("source", tree.getUniqueId());
			edge.put("target", child.getUniqueId());
			edges.put(edge);
			exportTreeToJson(child, nodes, edges, level + 5);
			if (i < tree.getChildren().size() - 1)
				width += 5;
		}
	}
	
	public static void main(String[] args) throws JSONException {
//		Document doc = getLocalXml(7823);
		Document doc = getXmlFromWeb("Ana are mere frumoase. Copilul merge fericit la școală.");
		DependencyTree tree = generateDepTree(doc);
//		System.out.println(tree.getLemma());
//		System.out.println(tree.getChildren().get(0).getChildren());
		JSONArray nodes = new JSONArray();
		JSONArray edges = new JSONArray();
		exportTreeToJson(tree, nodes, edges, 0);
		JSONObject json = new JSONObject();
		json.put("nodes", nodes);
		json.put("edges", edges);
		System.out.println(json);
		try {
			FileWriter file = new FileWriter("tree.json");
			file.write(json.toString());
			file.flush();
			file.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
