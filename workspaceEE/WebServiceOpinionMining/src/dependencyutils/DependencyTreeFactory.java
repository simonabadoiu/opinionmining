package dependencyutils;

import org.w3c.dom.Document;

public class DependencyTreeFactory {
	/**
	 * Used for live texts
	 * @param articleText
	 * @return
	 */
	public static DependencyTree getDepTree(String articleText) {
		Document doc = DependencyTreeGenerator.getXmlFromWeb(articleText);
		DependencyTree tree = DependencyTreeGenerator.generateDepTree(doc);
		return tree;
	}
	
	/**
	 * Used for testing and computing accuracy
	 * It takes the parsed text from a local file instead of
	 * making a request to the webService
	 * @param articleIndex
	 * @return
	 */
	public static DependencyTree getDepTree(int articleIndex) {
		Document doc = DependencyTreeGenerator.getLocalXml(articleIndex);
		DependencyTree tree = DependencyTreeGenerator.generateDepTree(doc);
		return tree;
	}
	
	public static DependencyTree getReviewDepTree(String filePath) {
		Document doc = DependencyTreeGenerator.getLocalReviewXml(filePath);
		DependencyTree tree = DependencyTreeGenerator.generateDepTree(doc);
		return tree;
	}
}
