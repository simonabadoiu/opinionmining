package dependencyutils;

import dependencyutils.DependencyConstants.Features;

/**
 * A pair <Feature, The type of dependency>
 * @author andreea.badoiu
 *
 */
public class FeatureConnection {
	private Features	feature;
	private String 		connection;

	public String getConnection() {
		return connection;
	}

	public void setConnection(String connection) {
		this.connection = connection;
	}

	public Features getFeature() {
		return feature;
	}

	public void setFeature(Features feature) {
		this.feature = feature;
	}

	public FeatureConnection(Features feature, String connection) {
		this.feature = feature;
		this.connection = connection;
	}

	@Override
	public boolean equals(Object featureConnection) {
		FeatureConnection fc = (FeatureConnection)featureConnection;

		if (fc == null)
			return false;
		
		if (fc.getConnection() == null && connection != null ||
				fc.getConnection() != null && connection == null)
			return false;

		if (fc.getFeature() == null && feature != null ||
				fc.getFeature() != null && feature == null)
			return false;

		if (!fc.getFeature().equals(feature))
			return false;
		
		String conn = fc.getConnection();
		if (conn == null && connection != null ||
				conn != null && connection == null)
			return false;
		if (conn == null && connection == null)
			return true;
		if (!conn.equals(connection))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "[" + feature + ", " + connection + "]";
	}
}
