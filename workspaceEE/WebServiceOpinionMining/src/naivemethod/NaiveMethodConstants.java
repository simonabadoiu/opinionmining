package naivemethod;

public class NaiveMethodConstants {
	/**
	 * 
	 * @author andreea.badoiu
	 * There are 4 tables that contain words polarity
	 * Each table contains different parts of speech
	 */
	public enum OpinionClass {
		ADJECTIVES {
			public String toString() {
				return "adjectives_ro_scores";
			}
		},
		
		NOUNS {
			public String toString() {
				return "nouns_ro_scores";
			}
		},
		
		VERBS {
			public String toString() {
				return "verbs_ro_scores";
			}
		},
		
		ADVERBS {
			public String toString() {
				return "adverbs_ro_scores";
			}
		}
	}
	
}

