package naivemethod;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

import general.ArticleAndEntities;
import general.GeneralConstants;
import general.GeneralConstants.Parsers;
import general.WordPolarity;
import utils.BaseWordsUtils;
import utils.EntitiesUtils;
import utils.PolarityCalculator;
import utils.WordPos;

public class NaiveMethodOpinionExtractor {
	public static final int SIDE_WORDS = 10;
	public static final int LEFT_SIDE_WORDS = 10;
	public static final int RIGHT_SIDE_WORDS = 10;

	String text;
	ArrayList<String> entities;

	public NaiveMethodOpinionExtractor() {

	}

	public NaiveMethodOpinionExtractor(ArticleAndEntities articleAndEntities) {
		this.text = articleAndEntities.getText();
		this.entities = articleAndEntities.getEntitati();
	}

	public NaiveMethodOpinionExtractor(String articleText) {
		this.text = articleText;
		this.entities = null;
	}

	/**
	 * parcurge toate cuvintele din text(forma de baza a cuvintelor) si
	 * calculeaza un scor total facand suma polaritatilor fiecarui cuvant
	 */
	public void computeNaivePolarity() {

	}

	public double computePolarityForEntity(String entityName,
			Parsers parser, int articleIndex) 
					throws Exception {
		String baseWordsArticle = new String();
		if (articleIndex == -1) {
			baseWordsArticle = BaseWordsUtils.getBaseWordsArticle(text);
		} else {
			baseWordsArticle = BaseWordsUtils.getBaseWordsArticle(articleIndex);
		}
		System.out.println("Base words \n" + baseWordsArticle);

		ArrayList<WordPos> entityWordPos = new ArrayList<>();
		ArrayList<WordPos> wordsPos = new ArrayList<>();

		TreeMap<Integer, Integer> entityOccs;
		entityOccs = EntitiesUtils.entityOccurences(entityName,
				baseWordsArticle);

		wordsPos = PolarityCalculator.computeWordsPolarity(baseWordsArticle, entityOccs, entityWordPos);

		ArrayList<Double> polaritySum = new ArrayList<>();
		ArrayList<Double> weightSum = new ArrayList<>();
		for (int i = 0; i < entityWordPos.size(); i++) {
			polaritySum.add(0.0);
			weightSum.add(0.0);
		}

		//compute average polarity for each entity occurence
		for (WordPos wp : wordsPos) {
			for (int i = 0; i < entityWordPos.size(); i++) {
				double sum = polaritySum.get(i);
				double weight = weightSum.get(i);

				int wordIndex = wp.pos;
				int entityIndex = entityWordPos.get(i).pos;

				int dist = Math.abs(wordIndex - entityIndex);

				if (dist <= SIDE_WORDS || SIDE_WORDS == 0) {
					//					sum += wp.polarity.senticnet*1.0/Math.log(dist*2);
					//					weight += 1.0/Math.log(dist*2);
					sum += wp.polarity.senticnet;
					weight += 1;
					polaritySum.set(i, sum);
					weightSum.set(i, weight);
				}
			}
		}

		double sum = 0;
		double weight = 0;
		for (int i = 0; i < polaritySum.size(); i++) {
			sum += polaritySum.get(i);
			weight += weightSum.get(i);
		}

		return sum/weight;
	}

	public double computePolarityForReview(String filePath,
			Parsers parser, int articleIndex) 
					throws Exception {
		String baseWordsArticle = new String();
		baseWordsArticle = BaseWordsUtils.getReviewBaseWordsArticle(filePath);
		System.out.println("Base words \n" + baseWordsArticle);

		ArrayList<WordPos> wordsPos = new ArrayList<>();

		wordsPos = PolarityCalculator.computeReviewWordsPolarity(baseWordsArticle);

		Double polaritySum = 0.0;
		Double weightSum = 0.0;

		//compute average polarity for each entity occurence
		for (WordPos wp : wordsPos) {
			polaritySum += wp.polarity.senticnet;
			weightSum += 1;
		}

		return polaritySum/weightSum;
	}

	public Double getPolarity(String columnName, HashMap<String, WordPolarity> dbDump, String word) {
		if (!dbDump.containsKey(word))
			return null;
		if (columnName.equals("senticnet"))
			return dbDump.get(word).senticnet;

		if (columnName.equals("sentiwordnet"))
			return dbDump.get(word).sentiwordnet;

		if (columnName.equals("anew"))
			return dbDump.get(word).anew;

		System.err.println("Field nod existent!");
		return null;
	}

	public static void main(String[] args) throws Exception {
		NaiveMethodTextExtractor nmte = new NaiveMethodTextExtractor();
		ArticleAndEntities article = nmte.extractArticle("http://www.mediafax.ro/stiinta-sanatate/celulele-stem-pot-incetini-evolutia-bolilor-alzheimer-si-parkinson-11626028");	// foarte negativ
		//		ArticleAndEntities article = nmte.extractArticle("http://sport.hotnews.ro/stiri-sport-7004954-oscar-2010-sport-schumacher-favorit-filmul-banii-fugi-caster-semenya-nominalizata-cea-mai-buna-actrita-pentru-baietii-nu-plang.htm");	// foarte pozitiv
		//		ArticleAndEntities article = nmte.extractArticle("http://www.gandul.info/stiri/gandul-live-lista-de-medicamente-compensate-din-romania-nu-a-mai-fost-actualizata-de-mai-bine-de-5-ani-cine-este-vinovatul-11728735");	//Austria pozitiv
		//		NaiveMethodOpinionExtractor nmoe = new NaiveMethodOpinionExtractor(article);
		//				System.out.println(article.getEntitati());
		//		System.out.println(article.getText());
		/*nmoe.computePolarityForEntity(article.getEntitati().get(8));

		nmoe.entityOccurences(article.getEntitati().get(8), article.getText());
		ArrayList<WordPos> entityWordPos = new ArrayList<>();
		System.out.println(nmoe.computeWordsPolarity(article.getText(), nmoe.entityOccurences(article.getEntitati().get(8), article.getText()), entityWordPos));
		System.out.println(entityWordPos);*/
		//		nmoe.computePolarityForEntity(article.getEntitati().get(11));
		//		nmoe.computePolarityForEntity(article.getEntitati().get(9));
		//		nmoe.computePolarityForEntity(article.getEntitati().get(25));
		/*ArrayList<String> versions = new ArrayList<>();
		versions.add("bala");
		versions.add("mehedinti");
		versions.add("bala");
		System.out.println(nmoe.removeDuplicateVersions(versions));*/
	}
}
