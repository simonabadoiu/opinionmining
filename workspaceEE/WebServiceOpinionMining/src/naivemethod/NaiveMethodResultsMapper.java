package naivemethod;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

/**
 * Reads the results obtained with this method and the
 * manually annotated polarities and returns a mapping 
 * between this results
 * @author andreea.badoiu
 *
 */
public class NaiveMethodResultsMapper {
//	public static final String VOTES_FILE = "votes_prev.csv";
//	public static final String VOTES_FILE = "votes_uaic.csv";
//	public static final String VOTES_FILE = "reviews_uaic.csv";
//	public static final String VOTES_FILE = "reviews_emag_uaic.csv";
	public static final String VOTES_FILE = "news_deptree_uaic.csv";
//	public static final String VOTES_FILE = "carturesti_reviews_deptree_uaic.csv";

	int total = 0;
	int veryNegative = 0;
	int somewhatNegative = 0;
	int negative = 0;
	int neutral = 0;
	int positive = 0;
	int somewhatPositive = 0;
	int veryPositive = 0;
	float sumVeryNeg = 0;
	float sumSomewhatNeg = 0;
	float sumNeg = 0;
	float sumNeutral = 0;
	float sumSomewhatPos = 0;
	float sumPos = 0;
	float sumVeryPos = 0;

	public ArrayList<ArrayList<String>> readFromCsv(String fileName) {
		ArrayList<ArrayList<String>> lines = new ArrayList<>();

		try{
			FileInputStream fstream = new FileInputStream(fileName);
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;

			while ((strLine = br.readLine()) != null)   {
				ArrayList<String> line;
				String[] elements = strLine.split(",");
				line = new ArrayList<String>(Arrays.asList(elements));
				lines.add(line);
			}

			in.close();
		}catch (Exception e){
			System.err.println("Error: " + e.getMessage());
		}
		System.out.println("Numar de linii:" + lines.size());
		return lines;
	}

	public static boolean isNumeric(String str)  
	{  
		try  
		{  
			Float.parseFloat(str);  
		}  
		catch(NumberFormatException nfe)  
		{  
			return false;  
		}  
		return true;  
	}

	public TreeMap<Float, ArrayList<Integer>> getManualAnnotations(ArrayList<ArrayList<String>> data) {
		TreeMap<Float, ArrayList<Integer>> annotationValues = new TreeMap<>();			//values and indexes
		for (int i = 0; i < data.size(); i++) {
			int index = 2;
//			System.out.println(data.get(i));
			while (!isNumeric(data.get(i).get(index))) {
				index++;
			}

			Float manualAnnotation = (float) ((float) Math.round((Float.parseFloat(data.get(i).get(index)) * 10.0))/10.0);
			System.out.println(manualAnnotation + " | " + (Float.parseFloat(data.get(i).get(index))));
			Float computedAnnotation = Float.parseFloat(data.get(i).get(index + 1));
			if (!computedAnnotation.equals(Float.NaN)) {
				if (annotationValues.containsKey(manualAnnotation)) {
					ArrayList<Integer> indexes = annotationValues.get(manualAnnotation);
					indexes.add(i);
					annotationValues.put(manualAnnotation, indexes);
				} else {
					ArrayList<Integer> indexes = new ArrayList<>();
					indexes.add(i);
					annotationValues.put(manualAnnotation, indexes);
				}
			}
		}

		return annotationValues;
	}

	public TreeMap<Float, ArrayList<Integer>> getComputedPolarities(
			ArrayList<ArrayList<String>> data) {
		TreeMap<Float, ArrayList<Integer>> computedPolarities = new TreeMap<>();
		
		for (int i = 0; i < data.size(); i++) {
			int index = 2;
			while (!isNumeric(data.get(i).get(index))) {
				index++;
			}

			Float polarity = (float) ((float) Math.round((Float.parseFloat(data.get(i).get(index + 1)) * 10.0))/ 10.0);
			if (!polarity.equals(Float.NaN)) {
				if (computedPolarities.containsKey(polarity)) {
					ArrayList<Integer> indexes = computedPolarities.get(polarity);
					indexes.add(i);
					computedPolarities.put(polarity, indexes);
				} else {
					ArrayList<Integer> indexes = new ArrayList<>();
					indexes.add(i);
					computedPolarities.put(polarity, indexes);
				}
			}
		}
		
		return computedPolarities;
	}


	/**
	 * 
	 * @param annotations
	 * 			All the possible values for a mannualy annotated text
	 * @param computedPolarities
	 * 			The results computed automatically
	 * @return
	 * 			For each possible class of the manual annotations, the number of
	 * 			computed polarities that match with that class
	 */
	public Integer[] getMatchingArray(
			Float key,
			TreeMap<Float, ArrayList<Integer>> annotations,
			TreeMap<Float, ArrayList<Integer>> groupedPolarities									
			) {
		Integer[] matchingArray = new Integer[annotations.size()];
		for (int i = 0; i < annotations.size(); i++) {
			matchingArray[i] = 0;
		}

		ArrayList<ArrayList<Integer>> values = new ArrayList<ArrayList<Integer>>(groupedPolarities.values());
		ArrayList<Integer> value = annotations.get(key);
		for (Integer index : value) {
			for (int i = 0; i < values.size(); i++) {
				boolean ok = false;
				for (int j = 0; j < values.get(i).size(); j++) {
					if (values.get(i).get(j).equals(index)) {
						matchingArray[i]++;
						ok = true;
						break;
					}
				}
				if (ok)
					break;
			}
		}
		return matchingArray;
	}

	public TreeMap<Float, ArrayList<Integer>> regroupComputedPolarities(
			TreeMap<Float, ArrayList<Integer>> annotations,
			TreeMap<Float, ArrayList<Integer>> computedPolarities
			) {
		TreeMap<Float, ArrayList<Integer>> regroupedPolarities;
		regroupedPolarities = new TreeMap<>();

		//used to iterate over the values from computedPolarities
		int lasti = 0;
		ArrayList<ArrayList<Integer>> values = new ArrayList<ArrayList<Integer>>(computedPolarities.values());
		ArrayList<Integer> mergedValues = new ArrayList<>();
		for (int i = 0; i < values.size(); i++) {
			mergedValues.addAll(values.get(i));
		}

		for(Map.Entry<Float, ArrayList<Integer>> entry : annotations.entrySet()) {
			Float key = entry.getKey();
			ArrayList<Integer> value = entry.getValue();
			ArrayList<Integer> indexes = new ArrayList<>();
			
			int last = lasti + value.size();
			for (; lasti < last; lasti++) {
				indexes.add(mergedValues.get(lasti));
			}

			regroupedPolarities.put(key, indexes);
		}
		
		return regroupedPolarities;
	}


	public int[][] createConfusionMatrix(ArrayList<ArrayList<String>> data) {
		TreeMap<Float, ArrayList<Integer>> annotations;
		TreeMap<Float, ArrayList<Integer>> computedPolarities;
		TreeMap<Float, ArrayList<Integer>> regroupedPolarities;

		annotations = getManualAnnotations(data);
		computedPolarities = getComputedPolarities(data);
		regroupedPolarities = regroupComputedPolarities(annotations,
				computedPolarities);


		int size = annotations.size();
		int[][] confusionMatrix = new int[size][size];

		// pentru fiecare valoare(clasa din adnotarile manuale) - verifica
		// cate dintre polaritatile calculate automat coincid
		int index = 0;
		for(Map.Entry<Float, ArrayList<Integer>> entry : annotations.entrySet()) {
			Float key = entry.getKey();
			Integer[] matchingArray = getMatchingArray(key, annotations, regroupedPolarities);
			for (int i = 0; i < matchingArray.length; i++) {
				confusionMatrix[index][i] = matchingArray[i];
			}
			index++;
		}
		
		for(Map.Entry<Float, ArrayList<Integer>> entry : annotations.entrySet()) {
			Float key = entry.getKey();
			DecimalFormat formatter = new DecimalFormat("0.00");
			System.out.printf("%s \t", formatter.format(key));
		}
		System.out.println();
		return confusionMatrix;
	}

	public static void main(String[] args) {
		NaiveMethodResultsMapper nmrm = new NaiveMethodResultsMapper();
		ArrayList<ArrayList<String>> data = nmrm.readFromCsv(VOTES_FILE);
		int[][] confusionMatrix = nmrm.createConfusionMatrix(data);
		int sum = 0;
		int diagSum = 0;

		for (int i = 0; i < confusionMatrix.length; i++) {
			for (int j = 0; j < confusionMatrix[0].length; j++) {
				System.out.print(confusionMatrix[i][j] + "\t");
				sum += confusionMatrix[i][j];
				if (i == j)
					diagSum += confusionMatrix[i][j];
			}
			System.out.println();
			System.out.println();
		}
		System.out.println(1.0 * diagSum/sum);
		System.out.println(sum);
	}
}
