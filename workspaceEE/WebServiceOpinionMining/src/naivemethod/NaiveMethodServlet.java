package naivemethod;


import general.GeneralConstants;
import general.GeneralConstants.Parsers;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 
 * @author andreea.badoiu
 * Servlet implementation class NaiveMethodServlet
 */
public class NaiveMethodServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor. 
	 */
	public NaiveMethodServlet() {
		// TODO Auto-generated constructor stub
		System.out.println("Aici");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String articleText = null;
		Double polarity = null;

		String entity = new String();
		String url = new String();

		System.out.println("get");
		if (request.getParameterMap().containsKey("entityName")) {
			entity = request.getParameter("entityName");
			System.out.println(entity);
		}

		if (request.getParameterMap().containsKey("url")) {
			url = request.getParameter("url");
			System.out.println(url);
		}

		if (url.isEmpty() || entity.isEmpty()) {
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			out.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			out.append("<response>");
			out.append("Nu s-au specificat un url sau o entitate valida!");
			out.append("</response>");
			out.close();
		} else {
			articleText = NaiveMethodTextExtractor.extractArticleFromSolr(url);
			NaiveMethodOpinionExtractor naiveMethodOpinionExtractor = new NaiveMethodOpinionExtractor(articleText);
			try {
				System.out.println("Entity: " + entity);
				polarity = naiveMethodOpinionExtractor.
						computePolarityForEntity(entity, 
												 GeneralConstants.PARSER, -1);
			} catch (Exception e) {
				e.printStackTrace();
				System.err.println("Nu s-a putut calcula polaritatea entitatii! Este posibil ca serviciul web de la uaic sa nu functioneze!");
			}
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			out.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			out.append("<response>");
			out.append(polarity.toString());
			out.append("</response>");
			out.close();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Ruleaza");
	}

}
