package naivemethod;

import general.ArticleAndEntities;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;

import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;

import solraccess.SolrDocs;
import dataaccess.DataAccessConstants;
import dataaccess.DataBaseConnection;

public class NaiveMethodTextExtractor {
	/**
	 * 
	 * @param lastId the id you want to start to read from adnotari database - articles table
	 * @param text return the resulted text in this variable
	 * @param entities return the entities contained by this paragraph 
	 */
	public void extractParagraph(int lastId, String text, HashMap<String, String> entities) {
		
	}
	
	/**
	 * 
	 * @param lastId the id you want to start reading from opinion_mining database - url_entitati_full table 
	 * @param text return the resulted text in this variable
	 * @param entities return the entities contained by the article 
	 * @return 
	 * @throws SQLException 
	 */
	public ArticleAndEntities extractArticle(String url) throws SQLException {
		String articleText;
		HashSet<String> entitiesVersions;
		HashMap<String, String> completeArticleEntities = new HashMap<>();
		
		articleText = extractArticleFromSolr(url);
		ArrayList<String> articleEntities = ExtractEntitiesForLink(url);
		entitiesVersions = extractVersionsForEntities(articleEntities, completeArticleEntities);
		
		return new ArticleAndEntities(articleText, entitiesVersions);
		
	}
	
	public String getArticleURL() throws SQLException {
		int lastID = 0;
		//TODO selecteaza linkul din url_entitati_full - in functie de lastid
		String query = "SELECT id, url FROM "
				+ "(SELECT id, url "
				+ "FROM url_entitati_full "
				+ "WHERE id > " + lastID + " "  
				+ "ORDER BY id DESC )"
//				+ "LIMIT " + numberOfRows + ") "
				+ "AS new_select "
				+ "GROUP BY url "
				+ "ORDER BY id ASC";
		
		System.out.println("QUERY: " + query);
		int numberOfColumns = 2;
		
		int numberOfArticles = 1;
		ArrayList<ArrayList<Object>> result = DataBaseConnection.executeQuery(DataAccessConstants.ENTITATI_ARTICLES_DB, query, numberOfColumns);
		if (result.size() < 1) {
			numberOfArticles = 0;
		}
		String articleURL = new String();
		for (int i = 0; i < numberOfArticles; i++) {
			System.out.println("Articol " + i );
			ArrayList<Object> value = result.get(i);
			
			articleURL = value.get(1).toString();
		}
		return articleURL;
	}
	
	/**
	 * Extrage din solr articolul corespunzator linkului primit ca parametru
	 * 
	 * @param articleLink linkul articolului
	 * @return Textul continut in articol
	 */
	public static String extractArticleFromSolr(String articleLink) {
		SolrDocs solrdocs = new SolrDocs();
		SolrDocumentList documents = solrdocs.Extract("Source:", articleLink, 0, 1);
		SolrDocument document = documents.get(0);
		String text = document.getFieldValue("Text").toString();
		System.out.println("Solr text: " + text);
		return text;
	}
	
	
	/**
	 * Pentru fiecare entitate detectata, adauga in vectorul de entitati toate variantele pentru fiecare entitate
	 * Aceste variante se iau din baza de date opinionm_minig, tabela clasificator3
	 * @param entities entitatile extrase din articol
	 * @return entitatile la care s-au adaugat toate variantele identificate pentru fiecare entitate din multimea initiala
	 * @throws SQLException 
	 */
	private static HashSet<String> extractVersionsForEntities(ArrayList<String> entities, HashMap<String, String> commaSeparatedEntityVersions) throws SQLException {
		HashSet<String> entitiesVersions = new HashSet<>();
		entitiesVersions.addAll(entities);
		for (String entity : entities) {
//			System.out.println("Entity: " + entity);
			entity = entity.replace("ş", "ș");
			String query = "SELECT versiuni FROM clasificator3 "
					+ "WHERE nume = _utf8\"" + entity + "\"";

			int numberOfColumns = 1;
			ArrayList<ArrayList<Object>> result = DataBaseConnection.executeQuery(DataAccessConstants.ENTITATI_ARTICLES_DB, query, numberOfColumns);
//			System.out.println("Size: " + result.size());
			for (int i = 0; i < result.size(); i++) {
				ArrayList<Object> value = result.get(i);
				commaSeparatedEntityVersions.put(entity, value.get(0).toString().trim().replaceAll("\\xa0+$", ""));
				String[] entityVersions = value.get(0).toString().split(", "); 
				for (int j = 0; j < entityVersions.length; j++) {
//					System.out.println("Entitate:" + entityVersions[j].replaceAll("[\\t\\s\\xa0]+$", "") + "===");
					entitiesVersions.add(entityVersions[j].trim().replaceAll("\\xa0+$", ""));
				}
			}
		}
		return entitiesVersions;
	}
	
	
	/**
	 * 
	 * @param link Linkul articolului
	 * @return	Un ArrayList ce contine entitatile identificate in acest articol
	 * @throws SQLException
	 */
	private static ArrayList<String> ExtractEntitiesForLink(String link) throws SQLException {
		ArrayList<String> entities = new ArrayList<>();
		String query = "SELECT  nume "
				+ "FROM `url_entitati_full` "
				+ "WHERE url = \""
				+ link + "\"";
		int numberOfColumns = 1;
		ArrayList<ArrayList<Object>> result = DataBaseConnection.executeQuery(DataAccessConstants.ENTITATI_ARTICLES_DB, query, numberOfColumns);
		for (int i = 0; i < result.size(); i++) {
			ArrayList<Object> value = result.get(i);
			entities.add(value.get(0).toString());
		}
		return entities;
	}
	
	
	public void getNextText(int lastId, String text, HashMap<String, String> entities) throws SQLException {
//		String text = new String();
		int articleid = lastId + 1;
		
		ArrayList<ArrayList<Object>> resultForText, resultForEntities;
		String textQuery = "SELECT article, articleid FROM articles WHERE id = " + articleid;
		resultForText = DataBaseConnection.executeQuery(DataAccessConstants.WORDS_DB, textQuery, 2);
		if (!resultForText.isEmpty()) {
			ArrayList<Object> value = resultForText.get(0);
			text = value.get(0).toString();
			articleid = Integer.parseInt(value.get(1).toString());
		}
		String entitiesQuery = "SELECT entity_name, entity_versions from article_entities WHERE articleid = " + articleid;
		resultForEntities = DataBaseConnection.executeQuery(DataAccessConstants.WORDS_DB, entitiesQuery, 2);
		if (!resultForEntities.isEmpty()) {
//			HashMap<String, String> entities = new HashMap<>();
			ArrayList<String> entitiesList = new ArrayList<>();
			Iterator<Entry<String, String>> it = entities.entrySet().iterator();
			while (it.hasNext()) {
				Entry<String, String> entry = it.next();
				entitiesList.add(entry.getKey() + "(" + entry.getValue() + ")");
			}
		}
	}
	
	public static void main(String[] args) {
		NaiveMethodTextExtractor nmte = new NaiveMethodTextExtractor();
		int lastId = -1;
		String text = new String();
		HashMap<String, String> entities = new HashMap<>();
		try {
			nmte.getNextText(lastId, text, entities);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
