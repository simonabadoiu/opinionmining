package naivemethod;

import general.GeneralConstants;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import naivemethoddependency.DependencyTreePolarity;
import dataaccess.DataAccessConstants;
import dataaccess.DataBaseConnection;
import databaseobjects.AsocUserVoteArticleTableRow;
import dependencyutils.DependencyTree;
import dependencyutils.DependencyTreeFactory;
import utils.General;

/**
 * 
 * @author andreea.badoiu
 * 
 * Takes annotations from database and compares the manual annotations with the results
 * given by the naive method opinion calculator
 */
public class NaiveMethodVerifyAccuracy {

	/**
	 * 
	 * @param articleIndex indexul articolului pentru care trebuie sa extraga
	 * 					   din baza de date
	 * @return lista de linii din baza de date corespunzatoare articolului cu
	 * 		   indexul articleIndex
	 */
	public ArrayList<AsocUserVoteArticleTableRow> extractVoteFromDB(int articleIndex) {
		String query = "SELECT * FROM asocUserVoteArticleEntity WHERE articleid = " + articleIndex;
		ArrayList<ArrayList<Object>> result = new ArrayList<>();
		try {
			result = DataBaseConnection.executeQuery(DataAccessConstants.ADNOTARI_DB, query, 6);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		ArrayList<AsocUserVoteArticleTableRow> tableRows = convertToDbObject(result);
		return tableRows;
	}

	public ArrayList<AsocUserVoteArticleTableRow> convertToDbObject(ArrayList<ArrayList<Object>> queryResult) {
		ArrayList<AsocUserVoteArticleTableRow> tableRows = new ArrayList<>();
		for (ArrayList<Object> values : queryResult) {
			tableRows.add(new AsocUserVoteArticleTableRow(values));
		}
		return tableRows;
	}

	public String extractEntity(String entityVersions) {
		int bracketIndex = entityVersions.indexOf("(");
		return entityVersions.substring(0, bracketIndex).trim();
	}

	public String extractArticleFromDB(int articleIndex) {
		String article = new String();
		String query = "SELECT article FROM articles WHERE id = " + articleIndex;
		ArrayList<ArrayList<Object>> result = new ArrayList<>();

		try {
			result = DataBaseConnection.executeQuery(DataAccessConstants.ADNOTARI_DB, query, 1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (!result.isEmpty()) {
			article = result.get(0).get(0).toString();
		}

		return article;
	}

	/**
	 * 
	 * @return a HashMap that has an average polarity associated to each entity 
	 * 		   that was annotated at least once
	 */
	/*public HashMap<String, Float> entitiesPolarity(ArrayList<AsocUserVoteArticleTableRow> tableRows) {
		HashMap<String, Float>		entPolarity = new HashMap<>();
		HashMap<String, Integer>	votesCount = new HashMap<>();

		for (AsocUserVoteArticleTableRow row : tableRows) {
			String entity = extractEntity(row.getEntityname());
			float polarity = row.getPolarity();

			if (entPolarity.containsKey(entity)) {
				float newPolarity = entPolarity.get(entity) + polarity;
				entPolarity.put(entity, newPolarity);
				int count = votesCount.get(entity) + 1;
				votesCount.put(entity, count);
			} else {
				entPolarity.put(entity, polarity);
				votesCount.put(entity, 1);
			}
		}

		Iterator it = entPolarity.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry pairs = (Map.Entry)it.next();
			String entity = (String) pairs.getKey();
			float votesSum = (float) pairs.getValue();
			int votesNumber = votesCount.get(entity);
			entPolarity.put(entity, votesSum/votesNumber);
		}
		System.out.println("Polarity: " + entPolarity);
		return entPolarity;
	}*/
	public HashMap<String, Float> entitiesPolarity(ArrayList<AsocUserVoteArticleTableRow> tableRows) {
		HashMap<String, Float>		entPolarity = new HashMap<>();
		HashMap<String, Integer>	votesCount = new HashMap<>();

		for (AsocUserVoteArticleTableRow row : tableRows) {
			String entity = extractEntity(row.getEntityname());
			float polarity = row.getPolarity();
			entPolarity.put(entity, polarity);
		}

		return entPolarity;
	}

	public float computePolarityForEntity(String entity, String text,
			GeneralConstants.Parsers parser, int articleIndex) {
		NaiveMethodOpinionExtractor naiveMethodOpinionExtractor = new NaiveMethodOpinionExtractor(text);
		float polarity = Float.NaN;
		try {
			polarity = (float) naiveMethodOpinionExtractor.computePolarityForEntity(entity, parser, articleIndex);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return polarity;
	}
	
	public float computePolarityForReview(String filePath,
			GeneralConstants.Parsers parser, int articleIndex) {
		NaiveMethodOpinionExtractor naiveMethodOpinionExtractor = new NaiveMethodOpinionExtractor("");
		float polarity = Float.NaN;
		try {
			polarity = (float) naiveMethodOpinionExtractor.computePolarityForReview(filePath, parser, articleIndex);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return polarity;
	}
	
	public static void writeDepTreeReviewResultsToCsv() throws IOException, SQLException {
		GeneralConstants.Parsers parser = GeneralConstants.PARSER;
		String fileName = "carturesti_reviews_deptree_uaic.csv";
		Writer writer = null;
		NaiveMethodVerifyAccuracy nmva = new NaiveMethodVerifyAccuracy();
		
		//get the list of files for the reviews
		String folderPath = "webServicesOutputCarturesti";
		File folder = new File(folderPath);
		File[] files = folder.listFiles();
		
		int articleIndex = 0;
		int lastIndex = files.length;

		String articleText = new String();

		//open file for writing
		writer = new BufferedWriter(new OutputStreamWriter(
				new FileOutputStream(fileName), "UTF-8"));
		writer.write("ArticleID, Entity, Vote, ComputedPolarity\n");
		
		while (articleIndex < lastIndex) {
			//extract the score of the review from the file name
			String file = files[articleIndex].getName();
			Double vote = Double.parseDouble(file.substring(file.indexOf("_") + 1, file.indexOf(".")).toString());
			//compute polarity for each entity and append the results to file
			DependencyTree tree = DependencyTreeFactory.getReviewDepTree(files[articleIndex].getAbsolutePath());
//			ArrayList<DependencyTree> apparitions = searchEntity(tree, entityName);
//			ArrayList<DependencyTree> subtrees = getAllRelevantSubtrees(tree, entityName);
			ArrayList<DependencyTree> subtrees = new ArrayList<>();
			subtrees.add(tree);
			float computedPolarity = DependencyTreePolarity.computeParagraphPolarity(subtrees);
			writer.write(articleIndex + ", " + articleIndex + ", " + vote + "," + computedPolarity + "\n");
			articleIndex++;
		}
		
		writer.close();
	}
	
	public static void writeDepTreeNewsResultsToCsv() throws Exception {
		GeneralConstants.Parsers parser = GeneralConstants.PARSER;
		String fileName = "news_deptree_uaic.csv";
		Writer writer = null;
		NaiveMethodVerifyAccuracy nmva = new NaiveMethodVerifyAccuracy();

		int lastIndex = General.getLastArticleIndex();
		//		int lastIndex = 100;
		int articleIndex = 1;

		String articleText = new String();
		HashMap<String, Float> entVotes = new HashMap<>();

		//open file for writing
		writer = new BufferedWriter(new OutputStreamWriter(
				new FileOutputStream(fileName), "UTF-8"));
		writer.write("ArticleID, Entity, Vote, ComputedPolarity\n");

		while (articleIndex < lastIndex) {
			articleText = nmva.extractArticleFromDB(articleIndex);
			ArrayList<AsocUserVoteArticleTableRow> tableRows = nmva.extractVoteFromDB(articleIndex);
			System.out.println(tableRows);
			//extract manually annotated scores for text
			entVotes = nmva.entitiesPolarity(tableRows);

			System.out.println("Text: " + articleText);

			//compute polarity for each entity and append the results to file
			Iterator it = entVotes.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pairs = (Map.Entry)it.next();
				String entity = (String) pairs.getKey();
				float vote = (float) pairs.getValue();
				
				//float computedPolarity = nmva.computePolarityForEntity(entity, articleText, parser, articleIndex);
				DependencyTree tree = DependencyTreeFactory.getDepTree(articleIndex);
				ArrayList<DependencyTree> subtrees = DependencyTreePolarity.getAllRelevantSubtrees(tree, entity);
				try {
					Float computedPolarity = DependencyTreePolarity.computeParagraphPolarity(subtrees);
					writer.write(articleIndex + ", " + entity + ", " + vote + "," + computedPolarity + "\n");
				} catch (NullPointerException e) {
					System.err.println("Null pointer exception!");
				}
			}
			articleIndex++;
		}
		
		writer.close();
	}

	public static void writeReviewResultToCsv() throws IOException {
		GeneralConstants.Parsers parser = GeneralConstants.PARSER;
		String fileName = "reviews_emag_uaic.csv";
		Writer writer = null;
		NaiveMethodVerifyAccuracy nmva = new NaiveMethodVerifyAccuracy();

		//get the list of files for the reviews
		String folderPath = "webServicesOutputEmag";
		File folder = new File(folderPath);
		File[] files = folder.listFiles();

		int articleIndex = 0;
		int lastIndex = files.length;

		String articleText = new String();

		//open file for writing
		writer = new BufferedWriter(new OutputStreamWriter(
				new FileOutputStream(fileName), "UTF-8"));
		writer.write("ArticleID, Entity, Vote, ComputedPolarity\n");

		while (articleIndex < lastIndex) {
			//extract the score of the review from the file name
			String file = files[articleIndex].getName();
			Double vote = Double.parseDouble(file.substring(file.indexOf("_") + 1, file.indexOf(".")).toString());
			//compute polarity for each entity and append the results to file
			float computedPolarity = nmva.computePolarityForReview(files[articleIndex].getAbsolutePath(), parser, articleIndex);
			writer.write(articleIndex + ", " + articleIndex + ", " + vote + "," + computedPolarity + "\n");
			articleIndex++;
		}
		/*articleText = "vreme . ANM avea emite , în acest dimineață , un avertizare de cod galben"
					+ " de ceață , fenomen care afecta județ Teleorman , precum~și pe drum național"
					+ " și european aferent . ceață determina scădere vizibilitate local sub 200 m și"
					+ " izolat sub 50 m .";
		float computedPolarity = nmva.computePolarityForEntity("teleorman", articleText);*/
		writer.close();
	}

	public static void main(String[] args) {
		try {
//			writeReviewResultToCsv();
//			writeDepTreeReviewResultsToCsv();
			writeDepTreeNewsResultsToCsv();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*public static void main(String[] args) throws IOException {
		GeneralConstants.Parsers parser = GeneralConstants.PARSER;
		String fileName = "votes_uaic.csv";
		Writer writer = null;
		NaiveMethodVerifyAccuracy nmva = new NaiveMethodVerifyAccuracy();

		int lastIndex = General.getLastArticleIndex();
		//		int lastIndex = 100;
		int articleIndex = 1;

		String articleText = new String();
		HashMap<String, Float> entVotes = new HashMap<>();

		//open file for writing
		writer = new BufferedWriter(new OutputStreamWriter(
				new FileOutputStream(fileName), "UTF-8"));
		writer.write("ArticleID, Entity, Vote, ComputedPolarity\n");

		while (articleIndex < lastIndex) {
			articleText = nmva.extractArticleFromDB(articleIndex);
			ArrayList<AsocUserVoteArticleTableRow> tableRows = nmva.extractVoteFromDB(articleIndex);
			System.out.println(tableRows);
			//extract manually annotated scores for text
			entVotes = nmva.entitiesPolarity(tableRows);

			System.out.println("Text: " + articleText);

			//compute polarity for each entity and append the results to file
			Iterator it = entVotes.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pairs = (Map.Entry)it.next();
				String entity = (String) pairs.getKey();
				float vote = (float) pairs.getValue();
				float computedPolarity = nmva.computePolarityForEntity(entity, articleText, parser, articleIndex);
				writer.write(articleIndex + ", " + entity + ", " + vote + "," + computedPolarity + "\n");
			}
			articleIndex++;
		}
		articleText = "vreme . ANM avea emite , în acest dimineață , un avertizare de cod galben"
					+ " de ceață , fenomen care afecta județ Teleorman , precum~și pe drum național"
					+ " și european aferent . ceață determina scădere vizibilitate local sub 200 m și"
					+ " izolat sub 50 m .";
		float computedPolarity = nmva.computePolarityForEntity("teleorman", articleText);
		writer.close();
	}*/

}
