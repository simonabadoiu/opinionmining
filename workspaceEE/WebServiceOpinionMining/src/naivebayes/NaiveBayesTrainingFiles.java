package naivebayes;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeMap;

import dataaccess.DataAccessConstants;
import dataaccess.DataBaseConnection;
import utils.BaseWordsUtils;
import utils.EntitiesUtils;
import utils.FilesUtils;
import utils.General;
import utils.UtilsConstants.ManualAnnotation;
import utils.WordPos;
import utils.WordsPositions;
import weka.classifiers.Classifier;
import weka.core.Capabilities;
import weka.core.Capabilities.Capability;

public class NaiveBayesTrainingFiles {
	public static final int 	RIGHT_SIDE_LIMIT = 10;
	public static final int 	LEFT_SIDE_LIMIT = 10;
	public static final String 	ROOT_FOLDER = "wekaData_infinit_7c";

	public ArrayList<ArrayList<Object>> getEntitiesPolarities(int articleid) {
		ArrayList<ArrayList<Object>> result = null;

		String query = "SELECT entityName, polarity from asocUserVoteArticleEntity where articleid = " + articleid;
		try {
			result = DataBaseConnection.executeQuery(DataAccessConstants.ADNOTARI_DB, query, 2);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public String getParagraphText(int articleIndex) throws SQLException {
		String articleText = null;

		//Get article text
		String query = "SELECT article FROM articles WHERE id = " + articleIndex;
		ArrayList<ArrayList<Object>> result = DataBaseConnection.
				executeQuery(
						DataAccessConstants.ADNOTARI_DB, 
						query, 
						1);
		if (!result.isEmpty()) {
			articleText = result.get(0).get(0).toString();
		}

		return articleText;
	}

	public static ArrayList<String> getAdjacentWords(String entity, String baseWordsArticle) throws Exception {

		ArrayList<String> adjacentWords = new ArrayList<>();
		ArrayList<WordPos> entityWordPos = new ArrayList<>();
		ArrayList<WordPos> wordsPos = new ArrayList<>();

		TreeMap<Integer, Integer> entityOccs;
		entityOccs = EntitiesUtils.entityOccurences(entity,
				baseWordsArticle);

		wordsPos = WordsPositions.getWordsPosition(baseWordsArticle, entityOccs, entityWordPos);
		//		System.out.println(wordsPos);
		//		System.out.println(entityWordPos);

		//compute average polarity for each entity occurrence
		for (WordPos wp : wordsPos) {
			for (int i = 0; i < entityWordPos.size(); i++) {

				int wordIndex = wp.pos;
				int entityIndex = entityWordPos.get(i).pos;

				int dist = entityIndex - wordIndex;
				if (LEFT_SIDE_LIMIT == 0 && RIGHT_SIDE_LIMIT == 0) {
					adjacentWords.add(wp.word);
					continue;
				}
				if ( ((dist > 0) && (dist <= LEFT_SIDE_LIMIT)) || 
						((dist < 0) && (-dist <= RIGHT_SIDE_LIMIT)) &&
						((LEFT_SIDE_LIMIT != 0) && (RIGHT_SIDE_LIMIT != 0)) ) {
					//					System.out.println("Dist: " + dist);
					adjacentWords.add(wp.word);
				}
			}
		}

		return adjacentWords;
	}

	public void writeToTrainingFile(ArrayList<String> words, Float polarity, 
			String entity, int index) {
		ManualAnnotation annotation = General.processManualAnnotation(polarity);
		String pathToFile = ROOT_FOLDER + "/" + annotation.toString() + "/" + entity + index + ".txt";
		String content = "";
		for (String word : words) {
			content += word + " ";
		}
		FilesUtils.writeToFile(pathToFile, content);
	}

	public void createTrainingFiles(String rootFolder) throws Exception {
		int firstArticleIndex = 1;
		int lastArticleIndex = General.getLastArticleIndex();

		int fileIndex = 0;

		//for each manually annotated article from the database
		for (int articleIndex = firstArticleIndex;
				articleIndex < lastArticleIndex; 
				articleIndex++) {

			ArrayList<ArrayList<Object>> result = getEntitiesPolarities(articleIndex);
			String article = getParagraphText(articleIndex);
			String baseWordsArticle = null;
			if (article != null) {
				baseWordsArticle = BaseWordsUtils.getBaseWordsArticle(articleIndex);
				//				baseWordsArticle = article;

				/*
				 * for each annotated entity from the article, extract 
				 * the words from the left and from the right limited by the
				 * defined limits:  RIGHT_SIDE_LIMIT and  LEFT_SIDE_LIMIT
				 */
				for (int j = 0; j < result.size(); j++) {
					fileIndex++;
					String entity = result.get(j).get(0).toString();
					entity = entity.substring(0, entity.indexOf("("));
					Float polarity = Float.parseFloat(result.get(j).get(1).toString());

					ArrayList<String> adjacentWords = getAdjacentWords(entity, baseWordsArticle);
					System.out.println("============================================");
					System.out.println(entity + ": " + adjacentWords);
					System.out.println("============================================");
					writeToTrainingFile(adjacentWords, polarity, entity, fileIndex);
				}
			}
		}
	}

	public static void naiveBayesClassifier() throws Exception {
		ObjectInputStream ois = new ObjectInputStream(
				new FileInputStream("modele_automate/naive_bayes_modes_test.model"));
		Classifier cls = (Classifier) ois.readObject();
		//		 cls.classifyInstance(arg0);
		Iterator<Capability> it = cls.getCapabilities().capabilities();
		while (it.hasNext()) {
			System.out.println(it.next().values().length);
		}
	}

	public static void main(String[] args) throws Exception {
		NaiveBayesTrainingFiles nbtf = new NaiveBayesTrainingFiles();
		//		nbtf.createTrainingFiles("");
		naiveBayesClassifier();
	}
}
