package naivebayes;

/**
 * 
 * @author andreea.badoiu
 * Here are defined the classes for NaiveBayes algorithm implementation
 */
public class ConstantsNaiveBayes {
	
	/**
	 * 
	 * @author andreea.badoiu
	 * There are two classes a text can be part of - Positive and Negative classes
	 */
	public enum OpinionClass {
		POSITIVE {
			public String toString() {
				return "Positive";
			}
		},
		
		NEGATIVE {
			public String toString() {
				return "Negative";
			}
		}
	}
	
	public enum ManualAnnotation {
		VNEG {
			public String toString() {
				return "vneg";
			}
		},
		NEG {
			public String toString() {
				return "neg";
			}
		}, 
		SNEG {
			public String toString() {
				return "sneg";
			}
		},
		NEUTRAL {
			public String toString() {
				return "neutral";
			}
		}, 
		SPOS {
			public String toString() {
				return "spos";
			}
		},
		POS {
			public String toString() {
				return "pos";
			}
		},
		VPOS {
			public String toString() {
				return "vpos";
			}
		};
	}
}
