package dataaccess;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.TreeMap;

/**
 * This class resolves a connection to the opinion mining database.
 *
 * @author alexandru.dinca
 */
public class OpinionMiningDBAdapter {

    /**
     * Static class used to retain affective scores.
     */
    public static class AffectiveScore {

        public Double senticnet;
        public Double sentiwn;
        public Double anew;

        private AffectiveScore(Double senticnet, Double sentiwn, Double anew) {
            this.anew = anew;
            this.senticnet = senticnet;
            this.sentiwn = sentiwn;
        }

        public Double getValue(DataAccessConstants.AffectiveDB affectiveDB) {

            if (affectiveDB == DataAccessConstants.AffectiveDB.ANEW) {
                return anew;
            }
            if (affectiveDB == DataAccessConstants.AffectiveDB.SENTICNET) {
                return senticnet;
            }
            if (affectiveDB == DataAccessConstants.AffectiveDB.SENTIWN) {
                return sentiwn;
            }

            return Double.NaN;
        }
    }

    /**
     * Static class used for DB dump.
     */
    public static class OpinionMiningDB {

        public Map<String, AffectiveScore> en_scores;
        public Map<String, AffectiveScore> ro_scores;

        private OpinionMiningDB(Map<String, AffectiveScore> enscores, Map<String, AffectiveScore> roscores) {
            this.en_scores = enscores;
            this.ro_scores = roscores;
        }
    }

    private Connection connection;
    private PreparedStatement preparedStmt;
    private ResultSet rs;

    /**
     * Constructor which establish a connection to the opinion mining database.
     * @throws SQLException
     */
    public OpinionMiningDBAdapter() throws SQLException {

        this.connection = (new MySQLConnectionResolver(DataAccessConstants.MYSQL_OPINIONMINING_PROPERTIES_FILEPATH).getConnection());
    }

    /**
     * Get the affective value of a phrase in the specified language from local database.
     *
     * @param word token to be searched
     * @param lang whether ro for ro_scores table or en for en_scores
     * @param affectiveDB_columnName affective database to be used @see diploma.Constants
     * @return null if an error occurred, Double.NaN if there's no record in DB
     */
    public Double getAffectiveValueFromDB(String word, DataAccessConstants.Language lang, String affectiveDB_columnName) throws SQLException {

        /* TODO investigate this kind of tokens - possessives */
        if (word.contains("'")) {
            return Double.NaN;
        }

        /* throw an exception if the language it's not supported */
        if (lang == null || (lang != DataAccessConstants.Language.RO && lang != DataAccessConstants.Language.EN)) {
            throw new UnsupportedOperationException("Language not allowed.");
        }

        String tableName = "adjectives_" + lang + "_scores";

        String query = "SELECT * FROM " + tableName + " WHERE " + lang + "phrase='" + word + "'";
        preparedStmt = connection.prepareStatement(query);
        rs = preparedStmt.executeQuery();

        if (!rs.next()) {
            return Double.NaN;
        }
        return Double.parseDouble(rs.getString(affectiveDB_columnName));
    }

    /**
     * Dump the opinion mining database.
     * @return an object containing the dump of the current database.
     * @throws SQLException
     */
    public OpinionMiningDB dump() throws SQLException {

        return new OpinionMiningDB(dumpTable("enphrase", "en_scores"), dumpTable("rophrase", "adjectives_ro_scores"));
    }

    /**
     * Dump a table from the current database.
     * @param keyColumnName the name of the key column
     * @param tableName table name to be dumped
     * @return a map containing the dump of the specified table
     * @throws SQLException
     */
    private Map<String, AffectiveScore> dumpTable(String keyColumnName, String tableName) throws SQLException {

        String query = "SELECT * FROM " + tableName;
        preparedStmt = connection.prepareStatement(query);
        rs = preparedStmt.executeQuery();

        Map<String, AffectiveScore> scores = new TreeMap<>();

        while (rs.next()) {
            String phrase = rs.getString(keyColumnName);
            Double senticnet = rs.getDouble("senticnet");
            Double sentiwn = rs.getDouble("sentiwordnet");
            Double anew = rs.getDouble("anew");
            scores.put(phrase, new AffectiveScore(senticnet, sentiwn, anew));
        }

        return scores;
    }
}
