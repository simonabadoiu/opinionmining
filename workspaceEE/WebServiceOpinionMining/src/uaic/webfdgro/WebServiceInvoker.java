package uaic.webfdgro;

import uaic.webfdgro.FdgParserRoWSStub.ParseText;

public class WebServiceInvoker {

	public String queryServer(String input) throws Exception {
		System.out.println("input: " + input);
		FdgParserRoWSStub serviceParserStub = new FdgParserRoWSStub();
		
		FdgParserRoWSStub.ParseTextE textParser = new FdgParserRoWSStub.ParseTextE();
		
		ParseText parseText = new ParseText();
		parseText.setTxt(input);
		
		textParser.setParseText(parseText);
		FdgParserRoWSStub.ParseTextResponseE textResponse = serviceParserStub.parseText(textParser);
		
		String output = textResponse.getParseTextResponse().get_return();
		
		return output;
	}
	
	public static void main(String[] args) throws Exception {
		WebServiceInvoker wbs = new WebServiceInvoker();
		System.out.println(wbs.queryServer("Domnul Traian Băsescu a fost ales președintele României chiar dacă locuitorii țării nu își doreau sa aibă aceeași problemă și în acest nou mandat."));
	}
}
