package dbdump;

import general.WordPolarity;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import dataaccess.DataBaseConnection;

public class DbDump {
	int database;
	String tableName;
	
	public DbDump(Integer database, String tableName) {
		this.database = database;
		this.tableName = tableName;
	}
	
	public static HashMap<String, WordPolarity> dump(Integer database, String tableName, ArrayList<String> columns) throws SQLException {
		HashMap<String, WordPolarity> dbDump = new HashMap<>();
		String cols = new String();
		
		for (int i = 0; i < columns.size(); i++) {
			cols += columns.get(i) + ", ";
		}
		cols = cols.substring(0, cols.length() - 2);
		System.out.println(cols);
		String query = "SELECT " + cols + " FROM " + tableName;
		
		ArrayList<ArrayList<Object>> result = DataBaseConnection.executeQuery(database, query, columns.size());
		
		for (ArrayList<Object> row : result) {
			double senticnet = Double.parseDouble(row.get(1).toString());
			double sentiwordnet = Double.parseDouble(row.get(2).toString());
			double anew = Double.parseDouble(row.get(3).toString());
			
			dbDump.put(row.get(0).toString(), new WordPolarity(senticnet, sentiwordnet, anew));
		}
		
		return dbDump;
	}
}
