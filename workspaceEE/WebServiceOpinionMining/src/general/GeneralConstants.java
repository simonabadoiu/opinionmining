package general;

public class GeneralConstants {
	/**
	 * There are two parsers available
	 * @author andreea.badoiu
	 *
	 */
	public enum Parsers {
		UAIC {
			public String toString() {
				return "uaic_parser";
			}
		},
		
		RACAI {
			public String toString() {
				return "racai_parser";
			}
		}
	}
	
//	public static final Parsers PARSER = Parsers.RACAI;
	public static final Parsers PARSER = Parsers.UAIC;
}
