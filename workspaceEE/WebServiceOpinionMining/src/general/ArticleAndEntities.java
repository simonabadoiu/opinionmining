package general;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

public class ArticleAndEntities {
	public String				text;
	public ArrayList<String>	entitati;
	
	public ArticleAndEntities(String text, HashSet<String> entitiesVersions) {
		HashSet<String> x = new HashSet<>();
		this.text = text;
		this.entitati = new ArrayList<>();
		Iterator<String> it = entitiesVersions.iterator();
		while (it.hasNext()) {
			entitati.add(it.next());
		}
	}
	
	/*
	 * TODO verifica in tabela in care se retin voturile de la un utilizator, pana la ce
	 * id articol/paragraf a adnotat. Se va alege primul articol pe care nu l-a adnotat inca
	 */
	public void findNextTextId() {
		/*
		 * TODO se ia primul id lipsa din lista de texte adnotate asociate acestui utilizator
		 */
	}
	
	public String extractTextAtId() {
		return null;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public String getText() {
		return text;
	}
	
	public ArrayList<String> getEntitati() {
		return entitati;
	}
	
	public void setEntitati(ArrayList<String> entitati) {
		this.entitati = entitati;
	}
	
}
