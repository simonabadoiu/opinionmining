package naivemethoddependency;

import general.WordPolarity;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import utils.BaseWordsUtils;
import dataaccess.DataAccessConstants;
import dbdump.DbDump;
import dependencyutils.DependencyTree;
import dependencyutils.DependencyTreeFactory;

/**
 * Generates dependencies for the text received as parameter.
 * If an index is received, read the dependencies from file, otherwise sent the text to UAIC web service
 * in order to get this
 * @author andreea.badoiu
 *
 */
public class DependencyTreePolarity {

	/**
	 * Computes polarity for the specified entity
	 * @param tree
	 * @param entity
	 * @return
	 * @throws Exception 
	 */
	public static ArrayList<DependencyTree> getAllRelevantSubtrees(DependencyTree tree, String entity) throws Exception {
		// search the entity in the tree, by making a BFS traversal
		String entityName = entity;
		ArrayList<DependencyTree> apparitions = searchEntity(tree, entityName);
		System.out.println("Number of apparitions for: " + entity + ": " + apparitions);
		ArrayList<DependencyTree> subtrees = new ArrayList<>();
		
		// find the subtree root
		for (int i = 0; i < apparitions.size(); i++) {
			DependencyTree subtreeRoot = null;
			subtreeRoot = findThePhraseRoot(apparitions.get(i));
			System.out.println("Subtree root: \n" + subtreeRoot);
			subtrees.add(subtreeRoot);
		}
		
		return subtrees;
	}
	
	/**
	 * It receives the subtree in which the entity was found
	 * @param tree
	 * 			the subtree corresponding to the entity
	 * @return
	 * 		the computed polarity
	 */
	public static Float computePolarity(DependencyTree tree, HashMap<String, WordPolarity> dbdump) {
		Float polarity = 0.0f;
		Integer sign = 1;										//it can be 1 or -1, depending on the number of negations encountered - odd or even
		LinkedList<DependencyTree> queue = new LinkedList<>();
		queue.addLast(tree);
		
		while (!queue.isEmpty()) {
			DependencyTree currentNode = queue.pollFirst();
			if (dbdump.containsKey(currentNode.getLemma())) {
				polarity += (float)dbdump.get(currentNode.getLemma()).senticnet;
				System.out.println("Word: " + currentNode.getLemma() + ", Polarity: " + (float)dbdump.get(currentNode.getLemma()).senticnet);
			}
			if (currentNode.getLemma().equals("nu")) {
//				System.out.println("Schimba semnul");
				sign *= -1;
				polarity *= sign;
			}
			
			ArrayList<DependencyTree> children = currentNode.getChildren();
			for (int i = 0; i < children.size(); i++) {
				queue.addLast(children.get(i));
			}
		}
	
		return polarity;
	}
	
	public static Float computeParagraphPolarity(ArrayList<DependencyTree> relevantSubtrees) throws SQLException {
		//create dbDump
		ArrayList<String> columns = new ArrayList<>();
		columns.add("rophrase");
		columns.add("senticnet");
		columns.add("sentiwordnet");
		columns.add("anew");
		HashMap<String, WordPolarity> dump = DbDump.dump(DataAccessConstants.WORDS_DB, DataAccessConstants.ADJECTIVES_RO_SCORES, columns);
		
		ArrayList<Float> polarities = new ArrayList<>();
		Float sum = 0.0f;
		for (int i = 0; i < relevantSubtrees.size(); i++) {
			Float polarity = computePolarity(relevantSubtrees.get(i), dump);
			polarities.add(polarity);
			sum += polarity;
		}
		if (relevantSubtrees.size() > 0)
			return sum/relevantSubtrees.size();
		else 
			return 0.0f;
	}
	
	public static ArrayList<DependencyTree> searchEntity(DependencyTree tree, String entity) throws Exception {
		// get all the versions of the entity
		ArrayList<String> entityVersions = BaseWordsUtils.getEntityBaseVersions(entity);
		System.out.println("Entity versions: " + entityVersions);
		ArrayList<DependencyTree> apparitions = new ArrayList<>();
		
		//find all the apparitions of the entity in the tree
		apparitions = bfs(tree, entityVersions);
		
		return apparitions;
	}
	
	/**
	 * Find the root of the subtree that contains the entity. This
	 * method is looking for the first verb starting from the entity and
	 * going up in the tree.
	 * @param entitySubTree
	 * @return
	 */
	public static DependencyTree findThePhraseRoot(DependencyTree entitySubTree) {

		DependencyTree parent = entitySubTree.getParent();
		
		if (parent == null)
			return parent;
		
		if (parent.getParent() == null)
			return parent;
		
		if (parent.getParent().getLemma().equals("ROOT-NODE")) {
			return parent;
		}
		
		if (parent.getParent().getLemma().equals("SENTENCE-ROOT")) {
			return parent;
		}
		
		if (parent.getPos() != null && parent.getPos().equals("VERB")) {
			return parent;
		}
		
		return findThePhraseRoot(parent.getParent());
		
	}
	
	/**
	 * Search all the aparitions of all the entities versions in the tree
	 * @param tree
	 * @param entityVersions
	 * @return
	 * 		All the subtrees where one of the versions of the entity was found
	 * 		as root
	 */
	public static ArrayList<DependencyTree> bfs(DependencyTree tree,
										 ArrayList<String> entityVersions) {
		ArrayList<DependencyTree> result = new ArrayList<>();
		LinkedList<DependencyTree> queue = new LinkedList<>();
		
		queue.add(tree);
		while (!queue.isEmpty()) {
//			System.out.println(queue.size());
			DependencyTree currentNode = queue.pollFirst();
//			System.out.println(currentNode.getLemma());
			
			// if we found what we are looking for, add the current node to the list of
			// nodes containing the entity and continue searching other apparitions
			for (int i = 0; i < entityVersions.size(); i++) {
				String[] versionWords = entityVersions.get(i).split(" ");
				for (int j = 0; j < versionWords.length; j++) {
					if (currentNode.getLemma().toLowerCase().contains(versionWords[j])) {
						result.add(currentNode);
						break;
					}
				}
			}
			
			//add the children of the current node to the queue
			ArrayList<DependencyTree> children = currentNode.getChildren();
			for (int i = 0; i < children.size(); i++) {
				queue.addLast(children.get(i));
			}
		}
		System.out.println(result);
		return result;
	}
	
	public static void main(String[] args) throws Exception {
		String entityName = "Traian Băsescu";
		int articleIndex = 0;
//		DependencyTree tree = DependencyTreeFactory.getDepTree(articleIndex);
		DependencyTree tree = DependencyTreeFactory.getDepTree("Traian Băsescu nu a greșit când a spus că iubește să plece din țară");
//		ArrayList<DependencyTree> apparitions = searchEntity(tree, entityName);
		ArrayList<DependencyTree> subtrees = getAllRelevantSubtrees(tree, entityName);
		Float polarity = computeParagraphPolarity(subtrees);
		System.out.println(polarity);
	}
	
}