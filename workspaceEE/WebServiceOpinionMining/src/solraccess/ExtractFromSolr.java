package solraccess;

import org.apache.solr.common.SolrDocumentList;

import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: andreea.badoiu
 * Date: 8/16/13
 * Time: 12:25 PM
 * To change this template use File | Settings | File Templates.
 */
public class ExtractFromSolr {

    static public final String  category = "\"news\"";
    static public final Integer count = 15;
    static SolrDocumentList     solrDocs;
    static long                 totalDocsNo;
    static SolrDocs             solrd;

    public ExtractFromSolr() {
        solrd = new SolrDocs();
    }

    /**
     *
     * @param category the category in which are counted the documents
     */
    public static void getTotalNumberOfDocs(String category) {
        solrDocs = solrd.Extract("Category:", category, 0, 0);
        totalDocsNo = solrDocs.getNumFound();
    }
    
    /**
     * 
     * @param url the url of the article we are searching
     * @return the extracted SolrDocumentList
     */
    public static String extractTextFromURL(String url) {
    	solrDocs = solrd.Extract("Source:", url, 0, 1);
    	return solrDocs.get(0).getFieldValue("Text").toString();
    }

    public static void main(String[] args) throws SQLException {
    	solrd = new SolrDocs();
    	String text = extractTextFromURL("http://www.evenimentul.ro/articol/-20-de-puncte-la-finalul-turului.html");
    	System.out.println(text);
    }
}
