package solraccess;


import java.net.MalformedURLException;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.CommonsHttpSolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;

/**
 * Created with IntelliJ IDEA.
 * User: andreea.badoiu
 * Date: 8/16/13
 * Time: 11:08 AM
 * To change this template use File | Settings | File Templates.
 */
@SuppressWarnings("deprecation")
public class SolrDocs {
    final String IP = "http://10.6.35.103";
    final String PORT = "8983";

    CommonsHttpSolrServer server;

    public SolrDocs() {
        try {
            server = new CommonsHttpSolrServer(IP + ":" + PORT + "/solr");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

	/**
	 *
	 * @param fieldName a string representing the field -> "Category:" or "Source:"
	 * @param fieldComponent one component of this fiel -> "News" or the link to one article
	 * @param from where to start the search from
	 * @param count the maximum number of documents to be found
	 * @return the list of documents extracted from solr
	 */
	public SolrDocumentList Extract(String fieldName, String fieldComponent, int from, int count) {
		SolrDocumentList    docs = null;
		SolrQuery           query = new SolrQuery();

		query.setStart(from);                           				// numarul documentului de la care incepe query-ul
		query.setRows(count);                          					// numarul maxim de documente returnate la interogarea curenta

		query.setQuery(fieldName + " \"" + fieldComponent + "\"");		// gaseste doar articolele care au in campul filedName, continut corespunzator fieldComponent
		query.setSortField("Date", SolrQuery.ORDER.desc);				// sortare descrescatoare dupa data

		QueryResponse rsp = null;
		try {
			rsp = server.query(query);
		} catch (SolrServerException e) {
			e.printStackTrace();
		}

		docs = rsp.getResults();
		return docs;
	}
}
