package statistics;

import general.GeneralConstants;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import utils.General;
import dataaccess.DataAccessConstants;
import dataaccess.DataBaseConnection;

/**
 * Source: http://en.wikibooks.org/wiki/Algorithm_Implementation/Statistics/Cohen%27s_kappa
 * @author andreea.badoiu
 *
 */
public class CohensKappa {
 
	public static double computeKappa(List<List<Integer>> scores) {
		if (scores.size() != 2) {
			throw new IllegalArgumentException("Only scores from two annotators are valid for Cohen's Kappa");
		}
 
		if (scores.get(0).size() != scores.get(1).size()) {
			throw new IllegalArgumentException("The scores from both annotators must have the same length");
		}
 
		Map<Integer, Map<Integer, Integer>> table = new HashMap<Integer, Map<Integer, Integer>>();
 
		int tableSize = 0;
 
		for (int i = 0; i < scores.get(0).size(); i++) {
			int first = scores.get(0).get(i);
			int second = scores.get(1).get(i);
 
			if (!table.containsKey(first)) {
				table.put(first, new HashMap<Integer, Integer>());
			}
 
			if (!table.get(first).containsKey(second)) {
				table.get(first).put(second, 0);
			}
 
			table.get(first).put(second, table.get(first).get(second) + 1);
 
			if (first > tableSize) {
				tableSize = first + 1;
			}
 
			if (second > tableSize) {
				tableSize = second + 1;
			}
		}
 
		boolean DEBUG = true;
		if (DEBUG) {
			System.out.println(table);
		}
 
		int[][] matrix = new int[7][7];
		// sum the rows and cols
		Map<Integer, Integer> sumRows = new HashMap<Integer, Integer>();
		Map<Integer, Integer> sumCols = new HashMap<Integer, Integer>();
 
		for (Map.Entry<Integer, Map<Integer, Integer>> entry : table.entrySet()) {
			int rowNumber = entry.getKey();
			int sumRow = 0;
 
			for (Map.Entry<Integer, Integer> rowEntry : entry.getValue().entrySet()) {
				
				int colNumber = rowEntry.getKey();
				int value = rowEntry.getValue();
 
				matrix[rowNumber][colNumber] = value;
				
				sumRow += value;
 
				if (!sumCols.containsKey(colNumber)) {
					sumCols.put(colNumber, 0);
				}
 
				sumCols.put(colNumber, sumCols.get(colNumber) + value);
			}
 
			sumRows.put(rowNumber, sumRow);
		}
 
		// sum the total
		int sumTotal = 0;
		for (Integer rowSum : sumRows.values()) {
			sumTotal += rowSum;
		}
 
		if (DEBUG) {
			System.out.println("Row sums: " + sumRows);
			System.out.println("Col sums: " + sumCols);
			System.out.println("Total: " + sumTotal);
			System.out.println("table size: " + tableSize);
		}
 
		// sum diagonal
		int sumDiagonal = 0;
		for (int i = 1; i <= tableSize; i++) {
			int value = 0;
			if (table.containsKey(i) && table.get(i).containsKey(i)) {
				value = table.get(i).get(i);
			}
 
			sumDiagonal += value;
		}
 
		if (DEBUG) {
			System.out.println("Sum diagonal: " + sumDiagonal);
		}
 
 
		double p = (double) sumDiagonal / (double) sumTotal;
 
		if (DEBUG) {
			System.out.println("p: " + p);
		}
 
 
		double peSum = 0;
		for (int i = 1; i <= tableSize; i++) {
			if (sumRows.containsKey(i) && sumCols.containsKey(i)) {
				peSum += (double) sumRows.get(i) * (double) sumCols.get(i);
			}
		}
 
		if (DEBUG) {
			System.out.println("pesum: " + peSum);
		}
 
		double pe = peSum / (sumTotal * sumTotal);
 
		if (DEBUG) {
			System.out.println("pe: " + pe);
		}
 
		for (int i = 0; i < 7; i++) {
			for (int j = 0; j < 7; j++) {
				System.out.print(matrix[i][j] + ", ");
			}
		}
		
		return (p - pe) / (1.0d - pe);
	}
	
 
	public static void main(String[] args) throws SQLException {
//		exampleWikipediaOne();
//		exampleWikipediaTwo();
//		examplePaper1();
//		examplePaper2();
		annotationsAgreement();
	}
	
	public static void annotationsAgreement() throws SQLException {
		String query = "SELECT "
					 + "U1.userid, "
					 + "U2.userid, "
					 + "U1.polarity, "
					 + "U2.polarity, "
					 + "U2.entityname, "
					 + "U1.articleid "
					 + "FROM asocUserVoteArticleEntity U1, asocUserVoteArticleEntity U2 "
					 + "WHERE U1.articleid = U2.articleid "
					 + "AND U1.entityname = U2.entityname "
					 + "AND U1.userid < U2.userid";
		ArrayList<ArrayList<Object>> result;
		result = DataBaseConnection.executeQuery(DataAccessConstants.ADNOTARI_DB, query, 6);
		
		List<List<Integer>> scores = new ArrayList<>();
		scores.add(new ArrayList<Integer>());
		scores.add(new ArrayList<Integer>());
		
		System.out.println("Size: " + result.size());
		for (int i = 0; i < result.size(); i++) {
			float score1 = Float.parseFloat(result.get(i).get(2).toString());
			float score2 = Float.parseFloat(result.get(i).get(3).toString());
			
//			scores.get(0).add(General.processManualAnnotation(score1).ordinal());
//			scores.get(1).add(General.processManualAnnotation(score2).ordinal());
			int s1 = General.processManualAnnotation(score1).ordinal();
			int s2 = General.processManualAnnotation(score2).ordinal();
//			System.out.println(s1 + " : " + s2 + " ========== " + score1 + " : " + score2);
//			if (score1 == score2) totalEgaleScor++;
//			if (s1 == s2) totalEgaleOrder++;
//			n++;
			
			/*if (s1 < 3)
				s1 = 1;
			if (s1 > 3)
				s1 = 2;
			
			if (s2 < 3)
				s2 = 1;
			if (s2 > 3)
				s2 = 2;
			scores.get(0).add(s1);
			scores.get(1).add(s2);*/
			
			/*if (s1 < 2) {
				s1 = 1;
			} else if (s1 > 4) {
				s1 = 2;
			} else {
				s1 = 3;
			}
			
			if (s2 < 2) {
				s2 = 1;
			} else if (s2 > 4) {
				s2 = 2;
			} else {
				s2 = 3;
			}*/
			
			/*if (s1 < 2) {
				s1 = 1;
			} else {
				if (s1 > 4) {
					s1 = 5;
				}
			}
			
			if (s2 < 2) {
				s2 = 1;
			} else {
				if (s2 > 4) {
					s2 = 5;
				}
			}*/
			
			/*if (s1 == 1 || s1 == 2) {
				s1 = 2;
			} else if (s1 == 4 || s1 == 5) {
				s1 = 4;
			}
			
			if (s2 == 1 || s2 == 2) {
				s2 = 2;
			} else if (s2 == 4 || s2 == 5) {
				s2 = 4;
			}*/
			
			if (s1 > 0 || s1 < 6)
				s1 = 2;
			if (s2 > 0 || s2 < 6)
				s2 = 2;
			
			scores.get(0).add(s1);
			scores.get(1).add(s2);
		}
		
		System.out.println(scores.get(0));
		System.out.println(scores.get(1));
		
		System.out.println("Cohen's Kappa: " + computeKappa(scores));
//		System.out.println("Total egale: " + totalEgaleScor + ", Total order: " + totalEgaleOrder + ", Total: " + n);
	}
 
 
	public static void exampleWikipediaOne() {
		List<Integer> one = new ArrayList<Integer>();
		List<Integer> two = new ArrayList<Integer>();
		for (int i = 0; i < 45; i++) {
			one.add(1);
			two.add(1);
		}
 
		for (int i = 0; i < 15; i++) {
			one.add(1);
			two.add(2);
		}
 
		for (int i = 0; i < 25; i++) {
			one.add(2);
			two.add(1);
		}
 
		for (int i = 0; i < 15; i++) {
			one.add(2);
			two.add(2);
		}
 
		List<List<Integer>> list = new ArrayList<List<Integer>>();
		list.add(one);
		list.add(two);
 
		System.out.println("Kappa: " + computeKappa(list));
	}


}
