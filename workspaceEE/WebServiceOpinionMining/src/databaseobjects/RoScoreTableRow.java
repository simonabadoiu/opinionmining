package databaseobjects;

import java.util.ArrayList;

public class RoScoreTableRow {
	private String rophrase;
	private float senticnet;
	private float sentiwordnet;
	private float anew;
	
	public RoScoreTableRow(String rophrase, float senticnet, float sentiwordnet, float anew) {
		this.rophrase = rophrase;
		this.senticnet = senticnet;
		this.sentiwordnet = sentiwordnet;
		this.anew = anew;
	}
	
	public RoScoreTableRow(ArrayList<Object> row) {
		rophrase = row.get(0).toString();
		senticnet = Float.parseFloat(row.get(1).toString());
		sentiwordnet = Float.parseFloat(row.get(2).toString());
		anew = Float.parseFloat(row.get(3).toString());
	}

	public String getRophrase() {
		return rophrase;
	}

	public void setRophrase(String rophrase) {
		this.rophrase = rophrase;
	}

	public float getSenticnet() {
		return senticnet;
	}

	public void setSenticnet(float senticnet) {
		this.senticnet = senticnet;
	}

	public float getSentiwordnet() {
		return sentiwordnet;
	}

	public void setSentiwordnet(float sentiwordnet) {
		this.sentiwordnet = sentiwordnet;
	}

	public float getAnew() {
		return anew;
	}

	public void setAnew(float anew) {
		this.anew = anew;
	}
	
	
}
