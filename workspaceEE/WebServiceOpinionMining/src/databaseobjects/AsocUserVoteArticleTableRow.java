package databaseobjects;

import java.util.ArrayList;

/**
 * 
 * @author andreea.badoiu
 *
 * Object associated with one AsocUserVoteArticleTable row
 */
public class AsocUserVoteArticleTableRow {
	private int		asocid;
	private int		userid;
	private int		articleid;
	private String	entityname;
	private float	polarity;
	private String	date;
	
	public AsocUserVoteArticleTableRow(int asocid, int userid, int articleid,
									   String entityname, float polarity, String date) {
		this.asocid = asocid;
		this.userid = userid;
		this.articleid = articleid;
		this.entityname = entityname;
		this.polarity = polarity;
		this.date = date;
	}
	
	public AsocUserVoteArticleTableRow(ArrayList<Object> row) {
		asocid = Integer.parseInt(row.get(0).toString());
		userid = Integer.parseInt(row.get(1).toString());
		articleid = Integer.parseInt(row.get(2).toString());
		entityname = row.get(3).toString();
		polarity = Float.parseFloat(row.get(4).toString());
		date = row.get(5).toString();
	}

	public int getAsocid() {
		return asocid;
	}

	public void setAsocid(int asocid) {
		this.asocid = asocid;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public int getArticleid() {
		return articleid;
	}

	public void setArticleid(int articleid) {
		this.articleid = articleid;
	}

	public String getEntityname() {
		return entityname;
	}

	public void setEntityname(String entityname) {
		this.entityname = entityname;
	}

	public float getPolarity() {
		return polarity;
	}

	public void setPolarity(float polarity) {
		this.polarity = polarity;
	}
	
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	
	@Override
	public String toString() {
		return "[" + asocid + ", " + userid + ", " + articleid + ", " + entityname + ", " + polarity + ", " + date + "]";
	} 
}
