package wekaClassifier;

import java.util.ArrayList;

import utils.General;
import utils.UtilsConstants.ManualAnnotation;
import dependencyutils.DependencyPattern;

/**
 * Keep a strucured format of the data that si going to
 * be written into the arff file for weka
 * @author andreea.badoiu
 *
 */
public class WekaData {
	private ArrayList<DependencyPattern> 	patterns;
	private String 							entity;
	private ManualAnnotation 				polarity;

	public WekaData(ArrayList<DependencyPattern> patterns,
			String entity,
			Float polarity) {
		this.patterns = patterns;
		this.entity = entity;
		this.polarity = General.processManualAnnotation(polarity);
	}

	public ArrayList<DependencyPattern> getPatterns() {
		return patterns;
	}
	public void setPatterns(ArrayList<DependencyPattern> patterns) {
		this.patterns = patterns;
	}
	public String getEntity() {
		return entity;
	}
	public void setEntity(String entity) {
		this.entity = entity;
	}
	public ManualAnnotation getPolarity() {
		return polarity;
	}
	public void setPolarity(Float polarity) {
		this.polarity = General.processManualAnnotation(polarity);
	}
}
