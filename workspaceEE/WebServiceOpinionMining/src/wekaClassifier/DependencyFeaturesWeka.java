package wekaClassifier;


import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

import utils.General;
import utils.UtilsConstants.ManualAnnotation;
import weka.core.Attribute;
import weka.core.FastVector;
import dependencyutils.DependencyPattern;

/**
 * Create the training data for the dependency features 
 * identified in the training files
 * @author andreea.badoiu
 *
 */
public class DependencyFeaturesWeka {
	// The number of attributes is equal with the number of 
	// uniquely identified patterns in all the training texts

	FastVector fvWekaAttributes;
	int instancesNumber;
	ArrayList<DependencyPattern> uniquePatterns;

	public DependencyFeaturesWeka(HashSet<DependencyPattern> uniquePatterns,
			int instancesNumber,
			ArrayList<WekaData> wekaData) {
		this.instancesNumber = instancesNumber;
		this.uniquePatterns = new ArrayList<>(uniquePatterns);

		int attributesNumber = uniquePatterns.size();

		/*for (int i = 0; i < uniquePatterns.size(); i++) {
			Attribute attribute = new Attribute("numeric" + i);
			fvWekaAttributes.addElement(attribute);
		}

		attributesNumber += 7;
		// declare the class attribute along with its values
		FastVector fvClassVal = new FastVector(7);
		fvClassVal.addElement("v_positive");
		fvClassVal.addElement("positive");
		fvClassVal.addElement("s_positive");
		fvClassVal.addElement("neutral");
		fvClassVal.addElement("s_negative");
		fvClassVal.addElement("negative");
		fvClassVal.addElement("v_negative");

		fvWekaAttributes = new FastVector(attributesNumber);*/
	}

	private void writeRelation(Writer writer) throws IOException {
		writer.write("@relation 'Relation'\n\n");
	}

	private void writeClass(Writer writer) throws IOException {
		String classes = new String("{");
		for (ManualAnnotation ann : ManualAnnotation.values()) {
			classes += ann.toString() + ",";
		}
		classes = classes.substring(0, classes.length() - 2);
		classes += "}";

		writer.write("@attribute @@class@@ " + classes + "\n");
	}

	private void writeAttributes(HashSet<DependencyPattern> uniquePatterns, Writer writer) throws IOException {

		Iterator<DependencyPattern> it = uniquePatterns.iterator();
		
		int index = 0;
		while (it.hasNext()) {
			DependencyPattern pattern = it.next();
//			String attribute = "@attribute '" + pattern.toString() + "' numeric\n";
			String attribute = "@attribute '" + index + "' numeric\n";
			writer.write(attribute);
			index++;
		}
	}

	private void writeData(ArrayList<WekaData> wekaData, Writer writer) throws IOException {
		writer.write("\n@data\n");
		for (int i = 0; i < wekaData.size(); i++) {
			TreeMap<Integer, Integer> foundPatterns = new TreeMap<>();
			String arffDataLine = new String("{");
			WekaData data = wekaData.get(i);

			ArrayList<DependencyPattern> p = data.getPatterns();
			Iterator<DependencyPattern> it = p.iterator();

			ManualAnnotation polarity = data.getPolarity();
			arffDataLine += "0 " + polarity.toString();

			while (it.hasNext()) {
				DependencyPattern depPatt = it.next();

				int index = uniquePatterns.indexOf(depPatt);
				if (index >= 0) {
					 
					int count = 0;
					if (foundPatterns.containsKey(index)) {
						count = foundPatterns.get(index);
					}
					count++;
					foundPatterns.put(index, count);
				}
			}

			for(Map.Entry<Integer,Integer> entry : foundPatterns.entrySet()) {
				Integer attIndex = entry.getKey();
				Integer attApparitions = entry.getValue();
				arffDataLine += "," + (attIndex + 1) + " " + attApparitions;
			}

			arffDataLine += "}\n";
			writer.write(arffDataLine);
		}
	}

	public void writeWekaArff(String fileName, 
			HashSet<DependencyPattern> uniquePatterns, 
			ArrayList<WekaData> wekaData) {
		Writer writer = null;

		try {
			writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(fileName), "utf-8"));

			writeRelation(writer);
			writeClass(writer);
			writeAttributes(uniquePatterns, writer);
			writeData(wekaData, writer);
		} catch (IOException ex) {
			// report
		} finally {
			try {writer.close();} catch (Exception ex) {}
		}
	}

}
