package wekaClassifier;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Random;

import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.bayes.NaiveBayesMultinomial;
import weka.classifiers.functions.SMO;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.StringToWordVector;

public class SentimentAnalysisMain {

	public final static int NUMBER_ATTRIBUTES = 2;
	public final static String PHRASE = "_phrase";
	public final static int PHRASE_INDEX = 2;
	public final static String SENTIMENT = "_sentiment";
	public final static int SENTIMENT_INDEX = 3;
	public final static String TRAIN_DATASET = "E:/Traian/Lucrari&Carti&Prezentari/Softbinator/train.tsv";
	public final static int NUMBER_EXAMPLES = 156060; 

	public static Instances instances;
	public static Instances wordVectorInstances;

	public static void loadDatasetFromTsv() throws IOException {
		final Attribute phraseAttribute = new Attribute(PHRASE, (FastVector) null);

		FastVector sentimentValues = new FastVector(5);
		sentimentValues.addElement("1");
		sentimentValues.addElement("2");
		sentimentValues.addElement("3");
		sentimentValues.addElement("4");
		sentimentValues.addElement("0");
		final Attribute sentimentAttribute = new Attribute(SENTIMENT, sentimentValues);

		FastVector attributes = new FastVector(NUMBER_ATTRIBUTES);
		attributes.addElement(phraseAttribute);
		attributes.addElement(sentimentAttribute);

		instances = new Instances("sentiment_analysis", attributes, NUMBER_EXAMPLES);
		instances.setClassIndex(0);

		ArffSaver saver = new ArffSaver();
		saver.setInstances(instances);
		//	    saver.setFile(new File("train.arff"));
		//	    saver.writeBatch();
	}

	public static void loadDatasetFromArff(String filename) throws Exception {
		DataSource source = new DataSource(filename);
		instances = source.getDataSet();

		if (instances.classIndex() == -1)
			instances.setClassIndex(0);
	}

	public static void applyFilter() throws Exception {
		StringToWordVector stringToWordVectorFilter = new StringToWordVector();
		stringToWordVectorFilter.setInputFormat(instances);
		stringToWordVectorFilter.setOutputWordCounts(true);
		stringToWordVectorFilter.setLowerCaseTokens(true);
		//stringToWordVectorFilter.setUseStoplist(true);

		wordVectorInstances = Filter.useFilter(instances, stringToWordVectorFilter);

		ArffSaver saver = new ArffSaver();
		saver.setInstances(wordVectorInstances);
		saver.setFile(new File("train_wordVector.arff"));
		saver.writeBatch();
	}

	public static void main(String[] args) throws Exception {
		System.out.println("Loading..");
//		loadDatasetFromArff("ttt.arff");
		loadDatasetFromArff("bayes_3classes_news_bow_10_10_stringtoword.arff");
		System.out.println("Applying filter..");
		//		applyFilter();

		//System.out.println("Building Naive Bayes..");
//		Classifier naiveBayes = new NaiveBayesMultinomial();
//		Classifier naiveBayes = new NaiveBayes();
		Classifier naiveBayes = new SMO();
		//		naiveBayes.buildClassifier(wordVectorInstances);
		naiveBayes.buildClassifier(instances);
		//save model
		ObjectOutputStream oos = new ObjectOutputStream(
				new FileOutputStream("modele_automate/1smo_news_10_10.model"));
		oos.writeObject(naiveBayes);
		oos.flush();
		oos.close();

//		System.out.println("Evaluating..");
//		Evaluation eval = new Evaluation(instances);
//		eval.crossValidateModel(naiveBayes, instances, 10, new Random(100));
//
//		System.out.println(eval.toSummaryString("\nResults\n======\n", false));
//		System.out.println(eval.toMatrixString());
	}
}
