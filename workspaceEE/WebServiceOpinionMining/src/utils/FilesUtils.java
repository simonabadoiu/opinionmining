package utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

/**
 * 
 * @author andreea.badoiu
 *
 */
public class FilesUtils {
	
	public static void writeToFile(String pathToFile, String content) {
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(pathToFile, "UTF-8");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		writer.println(content);
		writer.close();
	}
}
