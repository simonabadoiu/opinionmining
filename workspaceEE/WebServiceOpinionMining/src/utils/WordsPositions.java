package utils;

import general.WordPolarity;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

public class WordsPositions {
	/**
	 * 
	 * @param article - textul articolului	
	 * @param entityPos - pozitiile si lungimea entitatilor din text
	 * @return un ArrayList in care se gaseste: cuvantul, numarul de ordine in propozitie, polaritatea
	 * @throws SQLException 
	 */
	public static ArrayList<WordPos> getWordsPosition(String article,
			TreeMap<Integer, Integer> entityPos, 
			ArrayList<WordPos> entityWordPos) throws SQLException {

		String articleSubstr = new String();
		ArrayList<WordPos> articleWords = new ArrayList<>();

		int word_pos = 0;
		int beginPos = 0;

		String art = new String(article);
		System.out.println(entityPos);
		for(Map.Entry<Integer,Integer> entry : entityPos.entrySet()) {
			Integer index = entry.getKey();
			Integer length = entry.getValue();

			try {
				articleSubstr = art.substring(beginPos, index);
			} catch (StringIndexOutOfBoundsException e) {
				e.printStackTrace();
			}
			String[] words = articleSubstr.split("[\\p{IsPunctuation}\\p{IsWhite_Space}]+");
			for (int i = 0; i < words.length; i++) {
				WordPolarity polarity = null;
				articleWords.add(new WordPos(words[i], word_pos, polarity));
				word_pos++;
			}

			//numara si cuvantul care reprezinta entitatea
			entityWordPos.add(new WordPos(art.substring(index, index+length), word_pos, null));
			word_pos++;

			//articleSubstr = art.substring(index + length);
			beginPos = index+length;
		}

		articleSubstr = art.substring(beginPos);
		String[] words = articleSubstr.split("[\\p{IsPunctuation}\\p{IsWhite_Space}]+");
		for (int i = 0; i < words.length; i++) {
			WordPolarity polarity = null;
			articleWords.add(new WordPos(words[i], word_pos, polarity));
			word_pos++;
		}
		return articleWords;
	}
}
