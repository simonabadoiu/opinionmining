package utils;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.ArrayList;

import dataaccess.DataAccessConstants;
import dataaccess.DataBaseConnection;
import utils.UtilsConstants.ManualAnnotation;

public class General {
	
	public static ArrayList<String> readFromFile(String fileName) {
		ArrayList<String> lines = new ArrayList<>();

		try{
			FileInputStream fstream = new FileInputStream(fileName);
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;

			while ((strLine = br.readLine()) != null)   {
				lines.add(strLine);
			}

			in.close();
		}catch (Exception e){
			System.err.println("Error: " + e.getMessage());
		}

		return lines;
	}
	
	public static ManualAnnotation processManualAnnotation(float value) {
		if (value > -1.1 && value < -0.9) {
			return ManualAnnotation.V_NEGATIVE;
		}
		if (value >= -0.7 && value < -0.6) {
			return ManualAnnotation.NEGATIVE;
		}
		if (value >= -0.4 && value < -0.3) {
			return ManualAnnotation.S_NEGATIVE;
		}
		if (value >= -0.1 && value < 0.1) {
			return ManualAnnotation.NEUTRAL;
		}
		if (value >= 0.3 && value < 0.4) {
			return ManualAnnotation.S_POSITIVE;
		}
		if (value >= 0.6 && value < 0.7) {
			return ManualAnnotation.POSITIVE;
		}
		if (value >= 0.9 && value < 1.1) {
			return ManualAnnotation.V_POSITIVE;
		}
		return null;
	}
	
	public static boolean isStopWord(String word, ArrayList<String> stopWords) {
		return stopWords.contains(word);
	}
	
	/**
	 * @return the last article index from table asocUserVoteArticleEntity
	 */
	public static int getLastArticleIndex() {
		String query = "SELECT articleid from asocUserVoteArticleEntity ORDER BY articleid DESC LIMIT 1";
		ArrayList<ArrayList<Object>> result = new ArrayList<>();

		try {
			result = DataBaseConnection.executeQuery(DataAccessConstants.ADNOTARI_DB, query, 1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		int lastIndex = Integer.parseInt(result.get(0).get(0).toString());
		return lastIndex;
	}
}
