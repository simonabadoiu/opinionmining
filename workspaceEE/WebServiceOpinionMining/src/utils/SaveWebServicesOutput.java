package utils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Random;


//import webservices.org.bermuda.ws.WebServiceInvoker;
import uaic.webfdgro.WebServiceInvoker;
import dataaccess.DataAccessConstants;
import dataaccess.DataBaseConnection;

public class SaveWebServicesOutput {
	public static String ROOT_FOLDER = "WebServicesOutput";
	public static String RACAI_FOLDER = "Racai";
	public static String UAIC_FOLDER = "Uaic";

	public static String extractArticleFromDB(int articleIndex) {
		String article = new String();
		String query = "SELECT article FROM articles WHERE id = " + articleIndex;
		ArrayList<ArrayList<Object>> result = new ArrayList<>();

		try {
			result = DataBaseConnection.executeQuery(DataAccessConstants.ADNOTARI_DB, query, 1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (!result.isEmpty()) {
			article = result.get(0).get(0).toString();
		}

		return article;
	}
	
	public static ArrayList<String> extractAllEntities() {
		ArrayList<String> entities = new ArrayList<>();
		
		String query = "SELECT entityName "
					 + "FROM asocUserVoteArticleEntity "
					 + "GROUP BY entityname";
		ArrayList<ArrayList<Object>> result = null;
		try {
			result = DataBaseConnection.
						executeQuery(DataAccessConstants.ADNOTARI_DB, 
									 query, 
									 1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if (result != null && !result.isEmpty()) {
			for (int i = 0; i < result.size(); i++) {
				String entity = result.get(i).get(0).toString();
				int index = entity.indexOf("(");
				if (index != -1) {
					entity = entity.substring(0, index);
				}
				entities.add(entity);
			}
		}
		return entities;
	}
	
	public static void addBaseEntitiesToDB() throws Exception {
		ArrayList<String> entities = extractAllEntities();
		for (int i = 0; i < entities.size(); i++) {
			//ArrayList<String> versions = BaseWordsUtils.extractEntityVersions(entities.get(i));
			ArrayList<String> versions = BaseWordsUtils.getEntityBaseVersions(entities.get(i));
			String mergedVersions = new String();
			for (int j = 0; j < versions.size(); j++) {
				mergedVersions += versions.get(j) + ", ";
			}
			System.out.println(mergedVersions + " | " + versions);
			String query = "INSERT INTO uaic_entities_base_words "
						 + "VALUES(" + 
						 	(i + 1) + ", \"" + 
						 	entities.get(i) + "\", \"" +
						 	mergedVersions.substring(0, mergedVersions.length() - 2).toLowerCase() + "\")";
			System.out.println(query);
			DataBaseConnection.insertQuery(DataAccessConstants.WORDS_DB, query);
		}
	}
	
	public static void main(String[] args) throws Exception {
//		int lastIndex = General.getLastArticleIndex();
		int lastIndex = 9912;
		int articleIndex = 9911;
		String pathToFile = ROOT_FOLDER + "/" + UAIC_FOLDER + "/";
		
		WebServiceInvoker wsi = new WebServiceInvoker();
		Random r = new Random();
		while (articleIndex < lastIndex) {
			System.out.println(articleIndex);
			String article = extractArticleFromDB(articleIndex);
			if (!article.isEmpty()) {
				String xml = wsi.queryServer(article);
				String path = pathToFile + articleIndex + ".xml";
				System.out.println(path);
				FilesUtils.writeToFile(path, xml);
			}
			articleIndex++;
			java.lang.Thread.sleep(5000 + r.nextInt(10000));
		}
		/*addBaseEntitiesToDB();
		System.out.println(extractAllEntities().size());*/
	}
}
