package utils;

import general.GeneralConstants;
import general.GeneralConstants.Parsers;

import java.util.ArrayList;
import java.util.TreeMap;

public class EntitiesUtils {
	/**
	 * 
	 * @param entity entitatea pentru care vrem sa calculam polaritatea
	 * @param article textul articolului - contine formele de baza ale cuvintelor din text
	 * @return un treeMap ce contine pozitia entitatii si lungimea acesteia
	 * 		   (se iau in considerare toate versiunile unei entitati)
	 * @throws Exception
	 */
	public static TreeMap<Integer, Integer> entityOccurences(String entity,
			String article) 
					throws Exception {
		Parsers parser = GeneralConstants.PARSER;
		String lowerCaseArticle = article.toLowerCase();
		int lastIndex = 0;
//		ArrayList<String> entitieVersions = BaseWordsUtils.extractEntityVersions(entity);
		ArrayList<String> entitieVersions = BaseWordsUtils.getEntityBaseVersions(entity);
//		ArrayList<String> entitieVersions = BaseWordsUtils.extractLocalEntityVersions(entity);

		//retine pozitia entitatii in text si lungimea acesteia, iar restul textului este impartit in cuvinte
		TreeMap<Integer, Integer> entityPos = new TreeMap<>();
		System.out.println("Versions: " + entitieVersions);
		for (String ent : entitieVersions) {
			lastIndex = 0;
			while(lastIndex != -1){

				lastIndex = lowerCaseArticle.indexOf(ent.toLowerCase(), lastIndex);

				//imparte in cuvinte textul care se gaseste inainte de last index
//				String[] words = text.split("[\\p{IsPunctuation}\\p{IsWhite_Space}]+");

				if (lastIndex != -1) {
					entityPos.put(lastIndex, ent.length());
					lastIndex += entity.length();
				}
			}
		}

		System.out.println("tree: " + entityPos);
		return entityPos;
	}
}
