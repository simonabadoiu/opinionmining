package utils;

import general.WordPolarity;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import dataaccess.DataAccessConstants;
import dbdump.DbDump;

public class PolarityCalculator {
	private static HashMap<String, Integer> stopWords = StopWordsUtils.getStopWords();
	/**
	 * 
	 * @param article - textul articolului	
	 * @param entityPos - pozitiile si lungimea entitatilor din text
	 * @return un ArrayList in care se gaseste: cuvantul, numarul de ordine in propozitie, polaritatea
	 * @throws SQLException 
	 */
	public static ArrayList<WordPos> computeWordsPolarity(String article,
			TreeMap<Integer, Integer> entityPos, 
			ArrayList<WordPos> entityWordPos) throws SQLException {
		//create dbDump
		ArrayList<String> columns = new ArrayList<>();
		columns.add("rophrase");
		columns.add("senticnet");
		columns.add("sentiwordnet");
		columns.add("anew");
		HashMap<String, WordPolarity> dbDump = DbDump.dump(DataAccessConstants.WORDS_DB, DataAccessConstants.ADJECTIVES_RO_SCORES, columns);

		String articleSubstr = new String();
		ArrayList<WordPos> articleWords = new ArrayList<>();

		int word_pos = 0;
		int beginPos = 0;

		String art = new String(article);
		System.out.println(entityPos);
		for(Map.Entry<Integer,Integer> entry : entityPos.entrySet()) {
			Integer index = entry.getKey();
			Integer length = entry.getValue();

			articleSubstr = art.substring(beginPos, index);
			String[] words = articleSubstr.split("[\\p{IsPunctuation}\\p{IsWhite_Space}]+");
			for (int i = 0; i < words.length; i++) {
				WordPolarity polarity = null;
				if (!stopWords.containsKey(words[i]))
					polarity = dbDump.containsKey(words[i]) ? dbDump.get(words[i]) : null;
					if (polarity != null)
						articleWords.add(new WordPos(words[i], word_pos, polarity));
					word_pos++;
			}

			//numara si cuvantul care reprezinta entitatea
			entityWordPos.add(new WordPos(art.substring(index, index+length), word_pos, null));
			word_pos++;

			//articleSubstr = art.substring(index + length);
			beginPos = index+length;
		}
		articleSubstr = art.substring(beginPos);
		String[] words = articleSubstr.split("[\\p{IsPunctuation}\\p{IsWhite_Space}]+");
		for (int i = 0; i < words.length; i++) {
			WordPolarity polarity = null;
			if (!stopWords.containsKey(words[i]))
				polarity = dbDump.containsKey(words[i]) ? dbDump.get(words[i]) : null;
				if (polarity != null)
					articleWords.add(new WordPos(words[i], word_pos, polarity));
				word_pos++;
		}
		return articleWords;
	}

	public static ArrayList<WordPos> computeReviewWordsPolarity(String article) throws SQLException {
		//create dbDump
		ArrayList<String> columns = new ArrayList<>();
		columns.add("rophrase");
		columns.add("senticnet");
		columns.add("sentiwordnet");
		columns.add("anew");
		HashMap<String, WordPolarity> dbDump = DbDump.dump(DataAccessConstants.WORDS_DB, DataAccessConstants.ADJECTIVES_RO_SCORES, columns);

		ArrayList<WordPos> articleWords = new ArrayList<>();

		int word_pos = 0;

		String art = new String(article);
		String[] words = article.split("[\\p{IsPunctuation}\\p{IsWhite_Space}]+");
		for (int i = 0; i < words.length; i++) {
			WordPolarity polarity = null;
			if (!stopWords.containsKey(words[i]))
				polarity = dbDump.containsKey(words[i]) ? dbDump.get(words[i]) : null;
				if (polarity != null)
					articleWords.add(new WordPos(words[i], word_pos, polarity));
				word_pos++;
		}
		return articleWords;
	}
}
