package utils;

import general.WordPolarity;

public class WordPos {
	public String word;
	public int pos;
	public WordPolarity polarity;

	public WordPos(String word, int pos, WordPolarity polarity) {
		this.word = word;
		this.pos = pos;
		this.polarity = polarity;
	}

	@Override
	public String toString() {
		return "[" + word + ": " + pos + ", " + polarity + "]";
	}
}
