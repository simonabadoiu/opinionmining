package utils;

import general.GeneralConstants;
import general.GeneralConstants.Parsers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import dataaccess.DataAccessConstants;
import dataaccess.DataBaseConnection;
import utils.XMLUtils;

/**
 * 
 * @author andreea.badoiu
 *
 */
public class BaseWordsUtils {

	public static ArrayList<String> removeDuplicateVersions(ArrayList<String> versions) {
		ArrayList<String> filteredVersions = new ArrayList<>();
		Iterator<String> it = versions.iterator();
		int i = 0;
		while (it.hasNext()) {
			String entity = it.next();
			boolean isDuplicate = false;
			for (int j = 0; j < versions.size(); j++) {
				if (i != j && entity.contains(versions.get(j).toLowerCase())) {
					isDuplicate = true;
					break;
				}
			}
			if (!isDuplicate) {
				filteredVersions.add(versions.get(i));
				i++;
			} else {
				it.remove();
			}
		}
		System.out.println("Versions: " + versions);
		System.out.println("Filtered versions: " + filteredVersions);
		return filteredVersions;
	}

	public static ArrayList<String> getBaseEntityNames(ArrayList<String> entityVersions,
			GeneralConstants.Parsers parser)
					throws Exception {
		uaic.webfdgro.WebServiceInvoker wsiUaic = null;
		webservices.org.bermuda.ws.WebServiceInvoker wsiRacai = null;
		String tagName, namedItem;

		ArrayList<String> returnEntityVersions = new ArrayList<>();
		if (entityVersions.size() == 0)
			return new ArrayList<>();
			if (parser.toString().equals(GeneralConstants.Parsers.UAIC.toString())) {
				wsiUaic = new uaic.webfdgro.WebServiceInvoker();
				tagName = "W";
				namedItem = "LEMMA";
			}
			else {
				wsiRacai = new webservices.org.bermuda.ws.WebServiceInvoker();
				tagName = "t";
				namedItem = "lemma";
			}

			for (String entity : entityVersions) {

				String xml = wsiUaic != null ? wsiUaic.queryServer(entity) : wsiRacai.queryServer(entity);
				Document doc = XMLUtils.loadXMLFromString(xml);
				doc.getDocumentElement().normalize();

				NodeList nodes = doc.getElementsByTagName(tagName);
				String ent = new String();
				for (int i = 0; i < nodes.getLength(); i++) {
					Node node = nodes.item(i);
					NamedNodeMap nnm = node.getAttributes();
					String lemma = nnm.getNamedItem(namedItem).getNodeValue();
					ent += lemma + " ";
				}
				returnEntityVersions.add(ent.trim().toLowerCase());
			}
			return returnEntityVersions;
	}


	/* return the base words for all the entity versions */
	public static ArrayList<String> getEntityBaseVersions(String entity) 
			throws Exception {
		switch (UtilsConstants.BASE_WORDS_SRC) {
		case UtilsConstants.WEB:
			return extractEntityVersions(entity);
		case UtilsConstants.LOCAL:
			return extractLocalEntityVersions(entity);
		default:
			return null;
		}
	}

	public static String getArticleBaseWords(String text, int articleIndex) throws Exception {
		if (articleIndex == -1) {
			try {
				return getBaseWordsArticle(text);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			return getBaseWordsArticle(articleIndex);
		}

		return null;
	}

	/**
	 * 
	 * @param entity entitatea pentru care vrem sa calculam opinia
	 * @return toate versiunile pentru entitatea respectiva
	 * @throws Exception 
	 */
	public static ArrayList<String> extractEntityVersions(String entity) 
			throws Exception {
		Parsers parser = GeneralConstants.PARSER;
		HashSet<String> versions = new HashSet<>();

		entity = entity.replace("ş", "ș");
		String query = "SELECT versiuni FROM clasificator3 "
				+ "WHERE nume = _utf8\"" + entity + "\"";

		int numberOfColumns = 1;
		ArrayList<ArrayList<Object>> result = DataBaseConnection.
				executeQuery(DataAccessConstants.ENTITATI_ARTICLES_DB, 
						query, 
						numberOfColumns);

		versions.add(entity);															//era .toLowerCase()
		for (int i = 0; i < result.size(); i++) {
			ArrayList<Object> value = result.get(i);
			String[] entityVersions = value.get(0).toString().split(", ");
			for (int j = 0; j < entityVersions.length; j++) {
				versions.add(entityVersions[j].trim().replaceAll("\\xa0+$", ""));		//era .toLowerCase()
			}
		}

		ArrayList<String> listOfVersions = new ArrayList<>(versions);
		System.out.println("List of versions" + listOfVersions);

		// get base words for entities
		ArrayList<String> v = getBaseEntityNames(listOfVersions, parser);
		System.out.println("base names: " + v);

		// delete entitie versions that contain another entity version
		ArrayList<String> filteredVersions = removeDuplicateVersions(v);

		return filteredVersions;
	}

	/**
	 * Extract entity version from database
	 * @param entity
	 * @return
	 * @throws Exception
	 */
	public static ArrayList<String> extractLocalEntityVersions(String entity) 
			throws Exception {
		String localDatabase;
		if (GeneralConstants.PARSER.equals(GeneralConstants.Parsers.UAIC)) {
			localDatabase = UtilsConstants.UAIC_ENTITIES;
		} else {
			localDatabase = UtilsConstants.RACAI_ENTITIES;
		}

		String query = "SELECT basename FROM " + 
				localDatabase + 
				" WHERE entityname = \"" + entity + "\"";

		ArrayList<ArrayList<Object>> result = DataBaseConnection.executeQuery(DataAccessConstants.WORDS_DB, query, 1);
		String versions = result.get(0).get(0).toString();
		String[] listOfVersions = versions.split(", ");

		return new ArrayList<String>(Arrays.asList(listOfVersions));
	}


	public static String getBaseWordsArticle(String text) 
			throws Exception {
		Parsers parser = GeneralConstants.PARSER;
		text = text.replace("ş", "ș");

		uaic.webfdgro.WebServiceInvoker wsiUaic = null;
		webservices.org.bermuda.ws.WebServiceInvoker wsiRacai = null;
		String tagName, namedItem;

		if (parser.toString().equals(Parsers.UAIC.toString())) {
			wsiUaic = new uaic.webfdgro.WebServiceInvoker();
			tagName = "W";
			namedItem = "LEMMA";
		}
		else {
			wsiRacai = new webservices.org.bermuda.ws.WebServiceInvoker();
			tagName = "t";
			namedItem = "lemma";
		}

		text = text.replaceAll("\\s+", " ");
		//		text = text.substring(0, text.length() - 10);
		//		text = text.replace('”', '\"');
		//		text = text.replace('„', '\"');

		String xml = wsiUaic != null ? wsiUaic.queryServer(text) : wsiRacai.queryServer(text);
		Document doc = XMLUtils.loadXMLFromString(xml);
		doc.getDocumentElement().normalize();

		NodeList nodes = doc.getElementsByTagName(tagName);
		String baseWordsarticle = new String();
		for (int i = 0; i < nodes.getLength(); i++) {
			Node node = nodes.item(i);
			NamedNodeMap nnm = node.getAttributes();
			String lemma = nnm.getNamedItem(namedItem).getNodeValue();
			baseWordsarticle += lemma + " ";
		}
		return baseWordsarticle;
	}

	public static String getBaseWordsArticle(int articleIndex) 
			throws Exception {
		Parsers parser = GeneralConstants.PARSER;

		String fileName = UtilsConstants.ROOT_FOLDER;
		String tagName, namedItem;

		if (parser.toString().equals(Parsers.UAIC.toString())) {
			fileName += UtilsConstants.UAIC_FOLDER;
			tagName = "W";
			namedItem = "LEMMA";
		}
		else {
			fileName += UtilsConstants.RACAI_FOLDER;
			tagName = "t";
			namedItem = "lemma";
		}

		fileName += articleIndex + ".xml";
		Document doc = XMLUtils.loadXMLFromFile(fileName);
		doc.getDocumentElement().normalize();

		NodeList nodes = doc.getElementsByTagName(tagName);
		String baseWordsarticle = new String();
		for (int i = 0; i < nodes.getLength(); i++) {
			Node node = nodes.item(i);
			NamedNodeMap nnm = node.getAttributes();
			String lemma = nnm.getNamedItem(namedItem).getNodeValue();
			baseWordsarticle += lemma + " ";
		}
		return baseWordsarticle;
	}
	
	public static String getReviewBaseWordsArticle(String fileName) 
			throws Exception {
		Parsers parser = GeneralConstants.PARSER;

		String tagName, namedItem;

		if (parser.toString().equals(Parsers.UAIC.toString())) {
//			fileName += UtilsConstants.UAIC_FOLDER;
			tagName = "W";
			namedItem = "LEMMA";
		}
		else {
//			fileName += UtilsConstants.RACAI_FOLDER;
			tagName = "t";
			namedItem = "lemma";
		}

		Document doc = XMLUtils.loadXMLFromFile(fileName);
		doc.getDocumentElement().normalize();

		NodeList nodes = doc.getElementsByTagName(tagName);
		String baseWordsarticle = new String();
		for (int i = 0; i < nodes.getLength(); i++) {
			Node node = nodes.item(i);
			NamedNodeMap nnm = node.getAttributes();
			String lemma = nnm.getNamedItem(namedItem).getNodeValue();
			baseWordsarticle += lemma + " ";
		}
		return baseWordsarticle;
	}

	public static void main(String[] args) throws Exception {
		System.out.println(extractLocalEntityVersions("Administrația Națională de Meteorologie"));
	}
}
