package utils;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

public class StopWordsUtils {
	private static final String STOP_WORDS_FILE = "stopwords_ro.txt";

	public static HashMap<String, Integer> getStopWords() {
		HashMap<String, Integer> stopWords = new HashMap<>();
	
		try{
			FileInputStream fstream = new FileInputStream(STOP_WORDS_FILE);
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;

			while ((strLine = br.readLine()) != null)   {
				stopWords.put(strLine, 1);
			}

			in.close();
		}catch (Exception e){
			System.err.println("Error: " + e.getMessage());
		}
		
		return stopWords;
	}
}
