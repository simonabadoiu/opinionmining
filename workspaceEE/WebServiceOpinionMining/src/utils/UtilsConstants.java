package utils;

public class UtilsConstants {
	public static final String	STOP_WORDS_FILE = "stopwords_ro.txt";
	public static final String	RACAI_ENTITIES = "entities_base_words";
	public static final String	UAIC_ENTITIES = "uaic_entities_base_words";
	
	/* indicates if the basewords are taken from the web or from local sources */
	public static final int		WEB = 1;
	public static final int		LOCAL = 2;
//	public static final int 	BASE_WORDS_SRC = LOCAL;
	public static final int 	BASE_WORDS_SRC = WEB;
	
	//the folders where is saved the output from the web services
	public static String	ROOT_FOLDER = "WebServicesOutput/";
	public static String	RACAI_FOLDER = "Racai/";
	public static String	UAIC_FOLDER = "Uaic/";

	/**
	 * All the possible values for a manual annotation
	 * @author andreea.badoiu
	 *
	 */
	public enum ManualAnnotation {
		V_NEGATIVE {
			public String toString() {
				return "very_negative";
			}
		},
		NEGATIVE {
			public String toString() {
				return "negative";
			}
		}, 
		S_NEGATIVE {
			public String toString() {
				return "somewhat_negative";
			}
		},
		NEUTRAL {
			public String toString() {
				return "neutral";
			}
		}, 
		S_POSITIVE {
			public String toString() {
				return "somewhat_positive";
			}
		},
		POSITIVE {
			public String toString() {
				return "positive";
			}
		},
		V_POSITIVE {
			public String toString() {
				return "very_positive";
			}
		};
		
		/*V_NEGATIVE {
			public String toString() {
				return "negative";
			}
		},
		NEGATIVE {
			public String toString() {
				return "negative";
			}
		}, 
		S_NEGATIVE {
			public String toString() {
				return "negative";
			}
		},
		NEUTRAL {
			public String toString() {
				return "neutral";
			}
		}, 
		S_POSITIVE {
			public String toString() {
				return "positive";
			}
		},
		POSITIVE {
			public String toString() {
				return "positive";
			}
		},
		V_POSITIVE {
			public String toString() {
				return "positive";
			}
		};*/

		public static float getVNegValue() {
			return -1.0f;
		}
		public static float getSNegValue() {
			return 0.67f;
		}
		public static float getNegValue() {
			return -0.33f;
		}
		public static float getNeutralValue() {
			return 0.0f;
		}
		public static float getSPosValue() {
			return 0.33f;
		}
		public static float getPosValue() {
			return -0.67f;
		}
		public static float getVPosValue() {
			return 1.0f;
		}
	}
}
