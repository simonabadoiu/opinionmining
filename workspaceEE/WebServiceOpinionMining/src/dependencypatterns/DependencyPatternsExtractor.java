package dependencypatterns;

import general.WordPolarity;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Random;

import org.apache.solr.update.processor.UniqFieldsUpdateProcessorFactory.UniqFieldsUpdateProcessor;

import com.sun.mail.handlers.text_html;

import naivemethoddependency.DependencyTreePolarity;
import utils.BaseWordsUtils;
import utils.General;
import utils.UtilsConstants;
import utils.UtilsConstants.ManualAnnotation;
import wekaClassifier.DependencyFeaturesWeka;
import wekaClassifier.WekaData;
import dataaccess.DataAccessConstants;
import dataaccess.DataBaseConnection;
import dbdump.DbDump;
import dependencyutils.DependencyConstants.Features;
import dependencyutils.DependencyPattern;
import dependencyutils.DependencyTree;
import dependencyutils.DependencyTreeFactory;
import dependencyutils.FeatureConnection;
import dependencyutils.DependencyConstants;
import edu.stanford.nlp.trees.DependencyTreeTransformer;

/**
 * 
 * @author andreea.badoiu
 * 
 * Creates a training file for Weka using the dependency patterns detected
 * in the dependency trees generated from the manually annotated paragraphs
 */
public class DependencyPatternsExtractor {

	public ArrayList<DependencyPattern> allPatterns = new ArrayList<>();
	ArrayList<WekaData> trainingData;
	/**
	 * Extract all the patterns starting from the entity in the parse tree. 
	 * The pattern is not going to contain more than five features
	 * @param tree
	 * @param entity
	 * @param index
	 * 		The index of the pattern that 
	 * @return
	 * 		A list of representative patterns for the entity given as argument
	 * @throws Exception 
	 */
	public ArrayList<DependencyPattern> extractPatterns(DependencyTree tree, String entity,
			HashMap<String, WordPolarity> dbdump) {
		ArrayList<DependencyPattern> patterns = new ArrayList<>();
		ArrayList<DependencyTree> relevantSubtrees = new ArrayList<>();
		try {
			relevantSubtrees = DependencyTreePolarity.getAllRelevantSubtrees(tree, entity);
		} catch (Exception e) {
			System.err.println("Can't get the relevant subtrees from the tree!");
			e.printStackTrace();
		}

		ArrayList<String> entityVersions = new ArrayList<>();
		try {
			entityVersions = BaseWordsUtils.getEntityBaseVersions(entity);
		} catch (Exception e) {
			String error = "Can not extract the entity base versions for: " + entity;
			System.err.println(error);
			e.printStackTrace();
		}
		System.out.println("Entity versions: " + entityVersions);
		// for each subtree, compute all the possible patterns
		for (int i = 0; i < relevantSubtrees.size(); i++) {
			ArrayList<DependencyPattern> subtreePatterns = new ArrayList<>();
			ArrayList<FeatureConnection> currentPattern = new ArrayList<>();
			subtreePatterns = extractPatternsForSubTree(relevantSubtrees.get(i), 
					entityVersions,  
					currentPattern,
					dbdump);
			patterns.addAll(subtreePatterns);
		}
		return patterns;
	}
	
	public ArrayList<DependencyPattern> extractPatternsFromEntity(DependencyTree tree, String entity,
			HashMap<String, WordPolarity> dbdump) {
		ArrayList<DependencyPattern> patterns = new ArrayList<>();
		ArrayList<DependencyTree> apparitions = new ArrayList<>();
		try {
			apparitions = DependencyTreePolarity.getAllRelevantSubtrees(tree, entity);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		ArrayList<String> entityVersions = new ArrayList<>();
		try {
			entityVersions = BaseWordsUtils.getEntityBaseVersions(entity);
		} catch (Exception e) {
			String error = "Can not extract the entity base versions for: " + entity;
			System.err.println(error);
			e.printStackTrace();
		}
		System.out.println("Entity versions: " + entityVersions);

		System.out.println("Apparitions: " + apparitions.size());
		for (int i = 0; i < apparitions.size(); i++) {
			ArrayList<DependencyPattern> subtreePatterns = new ArrayList<>();
			ArrayList<FeatureConnection> currentPattern = new ArrayList<>();
			subtreePatterns = extractPatternsStartingFromEntity(apparitions.get(i), 
					entityVersions,  
					currentPattern,
					dbdump);
//			System.out.println("=========================================\n" + subtreePatterns + "\n==================================================");
			patterns.addAll(subtreePatterns);
		}
		
		return patterns;
	}
	
	/**
	 * Extract all the patterns starting from the entity. The tree is seen as a graph - we are not
	 * exploring only the children but also the parents of a node
	 * @param tree
	 * @param entityVersions
	 * @param currentPattern
	 * @param dbdump
	 * @return
	 */
	public ArrayList<DependencyPattern> extractPatternsStartingFromEntity(DependencyTree tree, 
			ArrayList<String> entityVersions,
			ArrayList<FeatureConnection> currentPattern,
			HashMap<String, WordPolarity> dbdump) {

		if (tree == null) {
			return new ArrayList<>();
		}

		ArrayList<FeatureConnection> localPattern = new ArrayList<>(currentPattern);
		ArrayList<DependencyPattern> patterns = new ArrayList<>();

		Features featureType = computeFeatureType(tree, entityVersions, dbdump);
		String depRel = tree.getDeprel();
		FeatureConnection feature = new FeatureConnection(featureType, depRel); 
		localPattern.add(feature);

		if (tree.hasChildren() && localPattern.size() < 5) {
			ArrayList<DependencyTree> children = tree.getChildren();
			for (int i = 0; i < children.size(); i++) {
				if (!children.get(i).isVisited()) {
					children.get(i).setVisited(true);
					patterns.addAll(extractPatternsStartingFromEntity(children.get(i), 
							entityVersions,
							localPattern,
							dbdump));
				}
			}
			if (!(tree.getParent() == null) && !tree.getParent().getUniqueId().equals("ROOT-NODE") && !tree.getParent().isVisited()) {
				tree.getParent().setVisited(true);
				patterns.addAll(extractPatternsStartingFromEntity(tree.getParent(), 
						entityVersions, 
						localPattern, 
						dbdump));
			}
		} else if (localPattern.size() <= 5) {
			DependencyPattern pattern = new DependencyPattern();
			pattern.setPattern(localPattern);
			patterns.add(pattern);
		}

		return patterns;
	}

	public Features computeFeatureType(DependencyTree tree, ArrayList<String> entityVersions,
			HashMap<String, WordPolarity> dbdump) {
		if (tree == null)
			return Features.NOT_RELEVANT;
		
		String lemma = tree.getLemma();

		if (entityVersions.contains(lemma))
			return Features.ENTITY;

		Float polarity = null;
		//search lemma in affective scores database

		//depending on the score, the type will be:
		// NEUTRAL, NEGATIVE, POSITIVE, ENTITY or NOT_RELEVANT
		// NOT_RELEVANT are all the words that do not have an affective score
		// ENTITY is a word that is found in the entityVersions
		if (dbdump.containsKey(lemma)) {
			polarity = (float) dbdump.get(lemma).senticnet;
		}
		if (polarity == null) {
			return Features.NOT_RELEVANT;
		}
		if (polarity < /*-80*/ -10) {
			return Features.NEGATIVE;
		}
		if (polarity > /*120*/20) {
			return Features.POSITIVE;
		}

		return Features.NEUTRAL;
	}

	public ArrayList<DependencyPattern> extractPatternsForSubTree(DependencyTree tree, 
			ArrayList<String> entityVersions,
			ArrayList<FeatureConnection> currentPattern,
			HashMap<String, WordPolarity> dbdump) {
		
		if (tree == null) {
			return new ArrayList<>();
		}

		ArrayList<FeatureConnection> localPattern = new ArrayList<>(currentPattern);
		ArrayList<DependencyPattern> patterns = new ArrayList<>();

		Features featureType = computeFeatureType(tree, entityVersions, dbdump);
		String depRel = tree.getDeprel();
		FeatureConnection feature = new FeatureConnection(featureType, depRel); 
		localPattern.add(feature);

		if (tree.hasChildren() && localPattern.size() < 5) {
			ArrayList<DependencyTree> children = tree.getChildren();
			for (int i = 0; i < children.size(); i++) {
				patterns.addAll(extractPatternsForSubTree(children.get(i), 
						entityVersions,
						localPattern,
						dbdump));
			}
		} else if (localPattern.size() <= 5) {
			DependencyPattern pattern = new DependencyPattern();
			pattern.setPattern(localPattern);
			patterns.add(pattern);
		}

		return patterns;
	}

	public ArrayList<ArrayList<Object>> getEntitiesPolarities(int articleid) {
		ArrayList<ArrayList<Object>> result = null;

		String query = "SELECT entityName, polarity from asocUserVoteArticleEntity where articleid = " + articleid;
		try {
			result = DataBaseConnection.executeQuery(DataAccessConstants.ADNOTARI_DB, query, 2);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * Creates weka training file using the manually annotated articles
	 * @param patternType
	 */
	public void makeWekaArff(int patternType) {
		Random rand = new Random();
		HashSet<DependencyPattern> uniquePatterns = new HashSet<>();
		HashMap<String, WordPolarity> 	dbdump;
		String 							folder;
		final File 						uaicFolder;
		ArrayList<String> 				columns;

		dbdump = null;
		folder = UtilsConstants.ROOT_FOLDER + UtilsConstants.UAIC_FOLDER;
		uaicFolder = new File(folder);

		//create dbDump
		columns = new ArrayList<>();
		columns.add("rophrase");
		columns.add("senticnet");
		columns.add("sentiwordnet");
		columns.add("anew");
		
		try {
			dbdump = DbDump.dump(DataAccessConstants.WORDS_DB, DataAccessConstants.ADJECTIVES_RO_SCORES, columns);
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("The affective scores database can not be accessed!");
		}

		File[] allFiles = uaicFolder.listFiles();
		int n = allFiles.length;
//		n = 50;
		trainingData = new ArrayList<>();
		for (int i = 0; i < n; i++) {
			String fileName = allFiles[i].getName();
			Integer articleIndex = Integer.parseInt(fileName.substring(0, fileName.indexOf(".")));
			//get the tree for this article
			DependencyTree tree = DependencyTreeFactory.getDepTree(articleIndex);

			//Select all the entities annotated for the current articleIndex
			//TODO select only the entities annotated by one annotator
			ArrayList<ArrayList<Object>> result = getEntitiesPolarities(articleIndex);
			for (int j = 0; j < result.size(); j++) {
				String entity = result.get(j).get(0).toString();
				entity = entity.substring(0, entity.indexOf("("));
				Float polarity = Float.parseFloat(result.get(j).get(1).toString());
				/*if (polarity.equals(0.0f)) {
					float r = rand.nextFloat();
					if (r > 0.1) {
						continue;
					}
				}*/
				
				//Get all the patterns identified in the tree for the entity
				ArrayList<DependencyPattern> patterns = new ArrayList<>();
//				patterns = extractPatterns(tree, entity, dbdump);	//switch on pattern type
				patterns = extractPatternsFromEntity(tree, entity, dbdump);
				if (patterns != null) {
					preprocessPatterns(patterns);
					
					try {
	//					System.out.println("text: " + BaseWordsUtils.getBaseWordsArticle(articleIndex));
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					WekaData wekaData = new WekaData(patterns, entity, polarity);
					trainingData.add(wekaData);
					
	//				System.out.println("+++ === --- For entity: " + entity + " --- ==== +++");
	//				System.out.println(patterns);
					for (int k = 0; k < patterns.size(); k++) {
						DependencyPattern pattern = patterns.get(k);
						
						if (!pattern.getPattern().isEmpty())
							uniquePatterns.add(pattern);
					}
				}
			} 
		}
//		System.out.println("Unique patterns: " + uniquePatterns);
		System.out.println("Size: " + uniquePatterns.size());
		DependencyFeaturesWeka dfw = new DependencyFeaturesWeka(uniquePatterns, trainingData.size(), trainingData);
		dfw.writeWekaArff("wekaDepFile_from_entity.arff", uniquePatterns, trainingData);
	}
	
	public void preprocessPatterns(ArrayList<DependencyPattern> patterns) {	
//		System.out.println("=============== PAtterns before ============");
//		System.out.println(patterns);
		for (int k = 0; k < patterns.size(); k++) {
			if (!patterns.get(k).isAllNeutral())
				patterns.get(k).trimLastNeutral();
		}
//		System.out.println("=============== PAtterns after ============");
//		System.out.println(patterns);
	}

	public static void main(String[] args) {
		DependencyPatternsExtractor 	dpe;
		/*HashMap<String, WordPolarity> 	dbdump;
		ArrayList<String> 				columns;
		String 							entity;
		DependencyTree 					tree;
		ArrayList<DependencyPattern>	patterns;*/

		dpe = new DependencyPatternsExtractor();
		/*tree = DependencyTreeFactory.getDepTree(0);
		entity = "Traian Băsescu";
		dbdump =  null;
		patterns = null;

		//create dbDump
		columns = new ArrayList<>();
		columns.add("rophrase");
		columns.add("senticnet");
		columns.add("sentiwordnet");
		columns.add("anew");
		try {
			dbdump = DbDump.dump(DataAccessConstants.WORDS_DB, DataAccessConstants.ADJECTIVES_RO_SCORES, columns);
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("The affective scores database can not be accessed!");
		}

		try {
			patterns = dpe.extractPatterns(tree, entity, dbdump);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println(patterns);*/
		dpe.makeWekaArff(0);
	}

}
