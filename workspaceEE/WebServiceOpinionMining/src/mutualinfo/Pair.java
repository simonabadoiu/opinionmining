package mutualinfo;

@SuppressWarnings("rawtypes")
public class Pair implements Comparable {
	public int attr;
	public Double val;

	public Pair(int attr, double cls) {
		this.attr = attr;
		this.val = cls;
	}

	@Override
	public int compareTo(Object o) {
		Pair a = (Pair) o;
		return val.compareTo(a.val);
	}
	
	public String toString() {
		return val + "";
	}
}
