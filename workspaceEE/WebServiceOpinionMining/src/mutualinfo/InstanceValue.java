package mutualinfo;

@SuppressWarnings("rawtypes")
public class InstanceValue implements Comparable {
	public Integer attr;
	public String val;

	public InstanceValue(int attr, String value) {
		this.attr = attr;
		this.val = value;
	}

	@Override
	public int compareTo(Object o) {
		InstanceValue a = (InstanceValue) o;
		return attr.compareTo(a.attr);
	}

	public String toString() {
		return val + "";
	}
}
