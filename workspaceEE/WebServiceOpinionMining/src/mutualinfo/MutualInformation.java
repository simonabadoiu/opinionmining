package mutualinfo;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map.Entry;

import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

public class MutualInformation {

	public Instances instances;
	public double[][] matrixInClass;
	public double[][] matrixNotArgInClass;
	public double[][] matrixNotInClass;
	public double[][] matrixNotArgNotInClass;
	public int classIndex;
	public int numberOfAttr;
	public HashMap<Integer, ArrayList<Pair>> vals;
	public ArrayList<String> attrs;
	public ArrayList<String> insts;
	public HashMap<Integer, Integer> coresp;
	int		N = 0;

	public MutualInformation(String filename, int numberOfAttributes,
			int classIndex) {
		numberOfAttr = numberOfAttributes;
		this.classIndex = classIndex;
		loadDatasetFromArff(filename);
		matrixInClass = new double[instances.attribute(classIndex).numValues()][instances
				.numAttributes()];
		matrixNotArgInClass = new double[instances.attribute(classIndex)
				.numValues()][instances.numAttributes()];
		matrixNotInClass = new double[instances.attribute(classIndex)
				.numValues()][instances.numAttributes()];
		matrixNotArgNotInClass = new double[instances.attribute(classIndex)
				.numValues()][instances.numAttributes()];
		vals = new HashMap<>();
		for (int i = 0; i < instances.attribute(classIndex).numValues(); i++) {
			vals.put(i, new ArrayList<Pair>());
		}
		attrs = new ArrayList<>();
		insts = new ArrayList<>();
		coresp = new HashMap<>();
	}

	public void loadDatasetFromArff(String filename) {
		try {
			DataSource source;
			source = new DataSource(filename);
			instances = source.getDataSet();
			if (classIndex == -1)
				classIndex = instances.numAttributes();
			if (instances.classIndex() == -1)
				instances.setClassIndex(classIndex);
		} catch (Exception e) {
			System.err.println("loadDatasetFromArff: " + e.getMessage());
		}
	}

	private void computeMI() {
		for (int clsInd = 0; clsInd < instances.attribute(classIndex)
				.numValues(); clsInd++) {
			Enumeration<Instance> enu = instances.enumerateInstances();
			
			while (enu.hasMoreElements()) {
				N++;
				Instance inst = enu.nextElement();
				int clsIndex = (int) inst.classValue();
				for (int i = 0; i < instances.numAttributes(); i++) {
					if (clsInd == clsIndex) {
						/* in class */
						if (inst.value(i) != 0) {
							matrixInClass[clsIndex][i]++;
						} else {
							matrixNotArgInClass[clsIndex][i]++;
						}
					} else {
						/* not in class */
						if (inst.value(i) != 0) {
							matrixNotInClass[clsInd][i]++;
						} else {
							matrixNotArgNotInClass[clsInd][i]++;
						}
					}
				}
			}
		}
	}

	private double log2(double nr) {
		return Math.log(nr) / Math.log(2);
	}

	private double computeI(int attrInd, int clsInd) {
		double N11 = matrixInClass[clsInd][attrInd];
		double N01 = matrixNotArgInClass[clsInd][attrInd];
		double N10 = matrixNotInClass[clsInd][attrInd];
		double N00 = matrixNotArgNotInClass[clsInd][attrInd];
		double N1_ = N11 + N10;
		double N_1 = N11 + N01;
		double N0_ = N00 + N01;
		double N_0 = N00 + N10;

		return (N11 / N) * log2((N * N11) / (N1_ * N_1)) + (N01 / N)
				* log2((N * N01) / (N0_ * N_1)) + (N10 / N)
				* log2((N * N10) / (N1_ * N_0)) + (N00 / N)
				* log2((N * N00) / (N0_ * N_0));
	}

	private void computeIAll() {
		for (int i = 0; i < instances.numAttributes(); i++) {
			if (i != classIndex) {
				for (int cl = 0; cl < instances.attribute(classIndex)
						.numValues(); cl++) {
					ArrayList<Pair> v = vals.get(cl);
					Double val = computeI(i, 1);
					if (!(val.equals(Double.NaN))) {
						v.add(new Pair(i, val));
					}
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	public void createNewArffFile(String filename) throws FileNotFoundException {
		computeMI();
		computeIAll();

		for (Entry<Integer, ArrayList<Pair>> e : vals.entrySet()) {
			Collections.sort(e.getValue(), Collections.reverseOrder());
		}
		attrs.add(instances.attribute(classIndex).toString());
		coresp.put(classIndex, 0);
		for (int cl = 0; cl < instances.attribute(classIndex).numValues(); cl++) {
			ArrayList<Pair> v = vals.get(cl);
			if (v.size() < numberOfAttr)
				numberOfAttr = v.size();
			for (int i = 0; i < numberOfAttr; i++) {
				if (i!=classIndex && !attrs.contains(instances.attribute(v.get(i).attr)
						.toString())) {
					attrs.add(instances.attribute(v.get(i).attr).toString());
					coresp.put(v.get(i).attr, i);
				}
			}
		}

		Enumeration<Instance> enu = instances.enumerateInstances();
		while (enu.hasMoreElements()) {
			Instance inst = enu.nextElement();
			boolean ok = true;
			for (int i = 0; i < instances.numAttributes(); i++) {
				if (i != classIndex && inst.value(i) != 0) {
					if (!attrs.contains(instances.attribute(i).toString())) {
						ok = false;
					}
				}
			}
			if (ok) {
				insts.add(inst.toString());
			}
		}
		PrintWriter p = new PrintWriter(filename);
		p.println("@relation rel");
		for (String s : attrs) {
			p.println(s);
		}
		p.println("@data");
		for (String s : insts) {
			ArrayList<InstanceValue> instance = new ArrayList<>();
			if (!s.equals("{}")) {
				s = s.replace("{", "");
				s = s.replace("}", "");
				String[] tokens = s.split(",");
				for (int i = 0; i < tokens.length; i++) {
					String[] parts = tokens[i].split(" ");
					int val = coresp.get(Integer.parseInt(parts[0]));
					instance.add(new InstanceValue(val, parts[1]));
				}
				Collections.sort(instance);
				p.print("{");
				for (int i = 0; i < instance.size() - 1; i++) {
					p.print(instance.get(i).attr + " " + instance.get(i).val
							+ ",");
				}
				p.print(instance.get(instance.size() - 1).attr + " "
						+ instance.get(instance.size() - 1).val);
				p.println("}");
			}
		}
		p.close();
	}

	public static void main(String[] args) throws Exception {
		MutualInformation mi = new MutualInformation("wekaDepFile.arff",
				8000, 0);
		mi.createNewArffFile("test.arff");
	}
}
