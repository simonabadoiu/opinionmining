import general.Constants;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.ResourceBundle;

import polarityDatabase.AddArticlesToDB;
import dataaccess.DataBaseConnection;
import sun.launcher.resources.launcher;
import javafx.application.Application;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;


public class AdnotariGUI implements EventHandler, Initializable {
	private Stage				applicationStage;
    private Scene				applicationScene;
    
    private static int 			idUserLogat;
    private int					idTextDeAdnotat;
    private String 				textDeAdnotat;
    private ArrayList<String>	listaEntitatiAdnotate;
    private ArrayList<Double>	noteEntitatiAdnotate;
    private ArrayList<String>	listaEntitatiDeAdnotat;
	
	@FXML private TextArea	campText;
	@FXML private TextArea	entitatiAdnotate;
	@FXML private TextArea	entitatiDeAdnotat;
	
	@FXML private ComboBox	entitiesCombo;
	@FXML private Slider	slider1;
	@FXML private Button	startButton;
	@FXML private Button	saveButton;
	@FXML private Button	removeButton;
	@FXML private Button	nextButton;
	@FXML private Label		errorLabel;
	
	public AdnotariGUI () {

	}
	
	public AdnotariGUI(int idUserLogat) throws SQLException {
		this.idUserLogat = idUserLogat;
	}
	
	@FXML
	public void getNextText() throws SQLException {
		
		errorLabel.setText("");
		entitatiAdnotate.setText("");
		entitatiDeAdnotat.setText("");
		listaEntitatiAdnotate = new ArrayList<>();
		noteEntitatiAdnotate = new ArrayList<Double>();

		idTextDeAdnotat++;
		int articleid = 0;
		ArrayList<ArrayList<Object>> resultForText, resultForEntities;
		String textQuery = "SELECT article, articleid FROM articles WHERE id = " + idTextDeAdnotat;
		resultForText = DataBaseConnection.executeQuery(Constants.ADNOTARI_DB, textQuery, 2);
		if (!resultForText.isEmpty()) {
			ArrayList<Object> value = resultForText.get(0);
			textDeAdnotat = value.get(0).toString();
			campText.setText(textDeAdnotat);
			articleid = Integer.parseInt(value.get(1).toString());
		}
//		String entitiesQuery = "SELECT entity_name from article_entities WHERE articleid = " + articleid;
		String entitiesQuery = "SELECT entity_name, entity_versions from article_entities WHERE articleid = " + articleid;
		resultForEntities = DataBaseConnection.executeQuery(Constants.ADNOTARI_DB, entitiesQuery, 2);
		if (!resultForEntities.isEmpty()) {
			entitiesCombo.getItems().clear();
//			HashSet<String> entities = new HashSet<>();
			HashMap<String, String> entities = new HashMap<>();
			for (ArrayList<Object> value : resultForEntities) {
				//entities.add(value.get(0).toString());
				entities.put(value.get(0).toString(), value.get(1).toString());
			}
			ArrayList<String> entitiesList = new ArrayList<>();
			Iterator<Entry<String, String>> it = entities.entrySet().iterator();
			while (it.hasNext()) {
				Entry<String, String> entry = it.next();
				entitiesList.add(entry.getKey() + "(" + entry.getValue() + ")");
			}
//			ArrayList<String> entitiesList = new ArrayList<>(entities);
//			Collections.sort(entitiesList);
			entitiesCombo.getItems().addAll(entitiesList);
			
			//populeaza spatiul cu lista entitatilor neadnotate
			//lista entitatilor neadnotate coincide cu lista tuturor entitatilor la inceput
			listaEntitatiDeAdnotat = new ArrayList<>(entitiesList);
			for (String entity : listaEntitatiDeAdnotat) {
				entitatiDeAdnotat.setText(entitatiDeAdnotat.getText() + entity + "\n");
			}
		}
	}
	
	public void start() throws IOException {
    	applicationStage = new Stage();
    	applicationScene = new Scene((Parent)FXMLLoader.load(getClass().getResource("GUI.fxml")));
    	applicationScene.addEventHandler(EventType.ROOT, (EventHandler<? super Event>)this);
    	applicationStage.setTitle("Despre");
    	//applicationStage.getIcons().add(new Image(Constants.ICON_FILE_NAME));
        Dimension screenDimension = Toolkit.getDefaultToolkit().getScreenSize();
        applicationStage.setScene(applicationScene);
        applicationStage.show();
	        
	}
	
	@FXML
	public void removeAnnotation() throws SQLException {
		double sliderValue;
		String entityName = null;
		
		errorLabel.setText("");
		
		if (entitiesCombo.getValue() == null) {
			errorLabel.setText("!!! Trebuie selectata o entitate !!!");
			return;
		}
		
		entityName = entitiesCombo.getValue().toString();
		sliderValue = slider1.getValue();
		
		if (!annotationExists(entityName, sliderValue)) {
			errorLabel.setText("!!! Adnotare inexistenta !!!");
		} else {
			//sterge adnotarea
			String expression = "DELETE FROM asocUserVoteArticleEntity"
					+ " WHERE "
					+ "userid = " + idUserLogat + " AND "
					+ "articleid = " + idTextDeAdnotat + " AND "
					+ "entityName = \"" + entityName + "\"";
			
			DataBaseConnection.insertQuery(Constants.ADNOTARI_DB, expression);
			
			//sterge entitatea din lista de entitati adnotate
			listaEntitatiAdnotate.remove(entityName);
			entitatiAdnotate.setText("");
			int i = 0;
			for (String entity : listaEntitatiAdnotate) {
				entitatiAdnotate.setText(entitatiAdnotate.getText() + entity + " -> " + getNameForAnnotationValue(noteEntitatiAdnotate.get(i))  + "\n");
				i++;
			}
			//adauga entitatea in lista de entitati neadnotate
			listaEntitatiDeAdnotat.add(entityName);
			Collections.sort(listaEntitatiDeAdnotat);
			entitatiDeAdnotat.setText("");
			for (String entity : listaEntitatiDeAdnotat) {
				entitatiDeAdnotat.setText(entitatiDeAdnotat.getText() + entity + "\n");
			}
		}
	}
	
	@FXML
	public void saveAnnotation() {
		errorLabel.setText("");
		double sliderValue;
		String entityName = null;
		//TODO salveaza valoarea aleasa in slider si o asociaza cu entitatea selectata in comboBox
		if (entitiesCombo.getValue() == null) {
			errorLabel.setText("!!! Trebuie selectata o entitate !!!");
			return;
		}
		entityName = entitiesCombo.getValue().toString();
		sliderValue = slider1.getValue();
		
		try {
			if (!annotationExists(entityName, sliderValue)) {
				String expression = "INSERT INTO asocUserVoteArticleEntity VALUES("
						+ "DEFAULT, "
						+ idUserLogat + ", "
						+ idTextDeAdnotat + ", "
						+ "\"" + entityName + "\", "
						+ sliderValue + ", "
						+ "(SELECT NOW()))";
				//adauga adnotare noua in tabela
				DataBaseConnection.insertQuery(Constants.ADNOTARI_DB, expression);
				
				listaEntitatiAdnotate.add(entityName);
				noteEntitatiAdnotate.add(sliderValue);
				
				listaEntitatiDeAdnotat.remove(entityName);
				
				entitatiAdnotate.setText("");
				int i = 0;
				for (String entity : listaEntitatiAdnotate) {
					entitatiAdnotate.setText(entitatiAdnotate.getText() + entity + " -> " + getNameForAnnotationValue(noteEntitatiAdnotate.get(i))  + "\n");
					i++;
				}
				
				entitatiDeAdnotat.setText("");
				for (String entity : listaEntitatiDeAdnotat) {
					entitatiDeAdnotat.setText(entitatiDeAdnotat.getText() + entity + "\n");
				}
			} else {
				String expression = "UPDATE asocUserVoteArticleEntity SET "
						+ "polarity = " + sliderValue + ", "
						+ "vote_date = (SELECT NOW()) "
						+ "WHERE "
						+ "userid = " + idUserLogat + " AND "
						+ "articleid = " + idTextDeAdnotat + " AND "
						+ "entityName = \"" + entityName + "\"";
				//TODO updateaza intrarea deja exisenta in tabela de adnotari
				DataBaseConnection.insertQuery(Constants.ADNOTARI_DB, expression);
				
				int indexEntitate = listaEntitatiAdnotate.indexOf(entityName);
				noteEntitatiAdnotate.set(indexEntitate, sliderValue);
				
				entitatiAdnotate.setText("");
				int i = 0;
				for (String entity : listaEntitatiAdnotate) {
					entitatiAdnotate.setText(entitatiAdnotate.getText() + entity + " -> " + getNameForAnnotationValue(noteEntitatiAdnotate.get(i))  + "\n");
					i++;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("Operatia de insert in tabela asocUserVoteArticleEntity a esuat");
		}
	}
	
	@FXML
	public void onStartButton() {
		listaEntitatiAdnotate = new ArrayList<>();
		noteEntitatiAdnotate = new ArrayList<>();
		listaEntitatiDeAdnotat = new ArrayList<>();
		
		nextButton.setDisable(false);
		removeButton.setDisable(false);
		saveButton.setDisable(false);
		entitiesCombo.setDisable(false);
		slider1.setDisable(false);
		startButton.setVisible(false);
		//TODO gaseste articolul la care a ramas acest utilizator
		String query = "SELECT articleid FROM asocUserVoteArticleEntity WHERE userid = " + idUserLogat + " ORDER BY articleid DESC LIMIT 1";
		try {
			ArrayList<ArrayList<Object>> result = DataBaseConnection.executeQuery(Constants.ADNOTARI_DB, query, 1);
			if (result.isEmpty()) {
				// TODO ia primul text din textele de adnotat
				idTextDeAdnotat = 1;
			} else {
				idTextDeAdnotat = Integer.parseInt(result.get(0).get(0).toString()) + 1;
			}
				int articleid = 0;
				ArrayList<ArrayList<Object>> resultForText, resultForEntities;
				String textQuery = "SELECT article, articleid FROM articles WHERE id = " + idTextDeAdnotat;
				resultForText = DataBaseConnection.executeQuery(Constants.ADNOTARI_DB, textQuery, 2);
				if (!resultForText.isEmpty()) {
					ArrayList<Object> value = resultForText.get(0);
					textDeAdnotat = value.get(0).toString();
					campText.setText(textDeAdnotat);
					articleid = Integer.parseInt(value.get(1).toString());
				}
				String entitiesQuery = "SELECT entity_name, entity_versions from article_entities WHERE articleid = " + articleid;
				resultForEntities = DataBaseConnection.executeQuery(Constants.ADNOTARI_DB, entitiesQuery, 2);
				/*if (!resultForEntities.isEmpty()) {
					entitiesCombo.getItems().clear();
					HashSet<String> entities = new HashSet<>();
					for (ArrayList<Object> value : resultForEntities) {
						entities.add(value.get(0).toString());
					}
					ArrayList<String> entitiesList = new ArrayList<>(entities);
					Collections.sort(entitiesList);
					entitiesCombo.getItems().addAll(entitiesList);
					
					//populeaza spatiul cu lista entitatilor neadnotate
					//lista entitatilor neadnotate coincide cu lista tuturor entitatilor la inceput
					listaEntitatiDeAdnotat = new ArrayList<>(entitiesList);
					for (String entity : listaEntitatiDeAdnotat) {
						entitatiDeAdnotat.setText(entitatiDeAdnotat.getText() + entity + "\n");
					}
				}*/
				if (!resultForEntities.isEmpty()) {
					entitiesCombo.getItems().clear();
//					HashSet<String> entities = new HashSet<>();
					HashMap<String, String> entities = new HashMap<>();
					for (ArrayList<Object> value : resultForEntities) {
						//entities.add(value.get(0).toString());
						entities.put(value.get(0).toString(), value.get(1).toString());
					}
					ArrayList<String> entitiesList = new ArrayList<>();
					Iterator<Entry<String, String>> it = entities.entrySet().iterator();
					while (it.hasNext()) {
						Entry<String, String> entry = it.next();
						entitiesList.add(entry.getKey() + "(" + entry.getValue() + ")");
					}
//					ArrayList<String> entitiesList = new ArrayList<>(entities);
//					Collections.sort(entitiesList);
					Collections.sort(entitiesList);
					entitiesCombo.getItems().addAll(entitiesList);
					
					//populeaza spatiul cu lista entitatilor neadnotate
					//lista entitatilor neadnotate coincide cu lista tuturor entitatilor la inceput
					listaEntitatiDeAdnotat = new ArrayList<>(entitiesList);
					for (String entity : listaEntitatiDeAdnotat) {
						entitatiDeAdnotat.setText(entitatiDeAdnotat.getText() + entity + "\n");
					}
				}
				
		} catch (SQLException e) {
			System.out.println("Eroare: nu s-a putut trimite cererea la baza de date");
		}
	}

	@Override
	public void handle(Event arg0) {
		// TODO Auto-generated method stub
		
	}
	
//	public static void main(String[] args) {
//		launch(args);
//	}
	

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		campText.setEditable(false);
		nextButton.setDisable(true);
		removeButton.setDisable(true);
		saveButton.setDisable(true);
		entitiesCombo.setDisable(true);
		slider1.setDisable(true);
		campText.setText("Apasa butonul Start pentru a incepe sa adnotezi");
	}
	
	public boolean annotationExists(String entityName, double sliderValue) {
		String query = "SELECT articleid FROM asocUserVoteArticleEntity WHERE "
				+ "userid = " + idUserLogat + " AND "
				+ "articleid = " + idTextDeAdnotat + " AND "
				+ "BINARY entityName = \"" + entityName + "\"";
		try {
			ArrayList<ArrayList<Object>> result = DataBaseConnection.executeQuery(Constants.ADNOTARI_DB, query, 1);
			if (result.isEmpty()) {
				System.out.println("Nu exista");
				return false;
			}
			else {
				System.out.println("Exista " + entityName);
				return true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	public String getNameForAnnotationValue(Double value) {
		System.out.println("Value: " + value);
		if (value > -1.1 && value < -0.99) {
			return "Very Negative";
		}
		if (value > -0.67 && value < -0.66) {
			return "Negative";
		}
		if (value > -0.34 && value < -0.33) {
			return "Somewhat Negative";
		}
		if (value > -0.1 && value < 0.1) {
			return "Neutral";
		}
		if (value > 0.33 && value < 0.34) {
			return "Somewhat Positive";
		}
		if (value > 0.66 && value < 0.67) {
			return "Positive";
		}
		if (value > 0.99 && value < 1.0) {
			return "Very Positive";
		}
		return null;
	}

}
