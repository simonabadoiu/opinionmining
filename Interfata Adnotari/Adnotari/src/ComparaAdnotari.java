import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;



public class ComparaAdnotari implements EventHandler, Initializable {
	
	private Stage				applicationStage;
    private Scene				applicationScene;
    
    private static int 			idUserLogat;
    private int					idTextDeAdnotat;

	public void start() throws IOException {
    	applicationStage = new Stage();
    	applicationScene = new Scene((Parent)FXMLLoader.load(getClass().getResource("comparaArticole.fxml")));
    	applicationScene.addEventHandler(EventType.ROOT, (EventHandler<? super Event>)this);
    	applicationStage.setTitle("Despre");
    	//applicationStage.getIcons().add(new Image(Constants.ICON_FILE_NAME));
        Dimension screenDimension = Toolkit.getDefaultToolkit().getScreenSize();
        applicationStage.setScene(applicationScene);
        applicationStage.show();
	        
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void handle(Event arg0) {
		// TODO Auto-generated method stub
		
	}



}
