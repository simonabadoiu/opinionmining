package polarityDatabase;

import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;

public class TestClass {
	public static void main(String[] args) {
		SolrDocs solrdocs = new SolrDocs();
		SolrDocumentList documents = solrdocs.Extract("Source:", "http://www.gandul.info/stiri/inghetarea-pretului-benzinei-in-romania-oficial-guvernamental-toate-caile-sunt-posibile-8150462", 0, 1);
		SolrDocument document = documents.get(0);
		//System.out.println("======================== Text ========================");
		String text = document.getFieldValue("Text").toString();
		String[] rezultat = text.split("\n");
		for (int i = 0; i < rezultat.length; i++) {
			System.out.println("Paragraf:\n " + rezultat[i]);
		}
	}
}
