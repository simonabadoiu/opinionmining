package polarityDatabase;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.CommonsHttpSolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;

import java.net.MalformedURLException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: andreea.badoiu
 * Date: 8/9/13
 * Time: 3:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class Main {

    /**
     *
     * @param fields the fields taken from the old database
     * @param articleIndex the index of the document
     * @return
     */
    public static ArrayList<String> makeDbFields(HashMap<String, ArrayList<String>> fields, String articleIndex) {
        return null;
    }

    public static void main(String[] args) throws MalformedURLException, SQLException {

        ArticlePolarityDB                       apdb = new ArticlePolarityDB();
        String                                  text;
        HashMap<String, ArrayList<String>>      fields;
        SolrDocumentList                        solrDocs;

        final String                            dummyValue = "UNKNOWN";
        final String                            dummyWord = "ROOT-0";
        int                                     i = -1;
        HashMap<String, String>                 baseWords;
        ArrayList<String>                       links;
        ArrayList<String>                       PosPolarity;
        ArrayList<String>                       NegPolarity;
        ArrayList<String>                       UnkPolarity;
        Integer                                 negativeNo = 0;
        Integer                                 positiveNo = 0;
        Integer                                 unknownNo = 0;
        SolrDocs                                solrd;
        ArrayList<String>                       dbFields;

        PosPolarity = new ArrayList<>();
        NegPolarity = new ArrayList<>();
        UnkPolarity = new ArrayList<>();

        apdb.openConnection("votedb");
        //apdb.createTable("votedb", "articles_polarity2");

        fields = apdb.getFromTable("newsletter");
        fields.put("PosPolarity", new ArrayList<String>());
        fields.put("NegPolarity", new ArrayList<String>());
        fields.put("UnkPolarity", new ArrayList<String>());

        //ids = fields.get("id");
        //links = fields.get("Article");
        //articleDates = fields.get("ArticleDate");
        //domains = fields.get("Domain");
        //totalNos = fields.get("TotalNo");
        //addingDates = fields.get("AddingDate");
        //System.out.println(ids.size() + " " + links.size() + " " + articleDates.size() + " " + domains.size() + " " + totalNos.size() + " " + addingDates.size());
        links = new ArrayList<>();
        links.add("http://alexandrunegrea.ro/2008/02/12/un-blogger-adevarat-tata.html");
        solrd = new SolrDocs();
        //server = new CommonsHttpSolrServer(IP + ":" + PORT + "/solr");

        int articleIndex = -1;
        for (String articleLink : links) {
            articleIndex++;
            boolean exists = false;
            //boolean exists = apdb.exists(Integer.parseInt(fields.get("id").get(articleIndex)), "articles_polarity");
            //System.out.println("Exists: " + exists);
            if (!exists) {
                System.out.println(articleLink);
                solrDocs = solrd.Extract("Source:", "\"" + articleLink + "\"", 0, 1);
                System.out.println(solrDocs.get(0).getFieldNames());
                System.out.println("Text:\n" + solrDocs.get(0).getFieldValue("Text"));
                System.out.println("content:\n" + solrDocs.get(0).getFieldValue("content"));

                i = -1;
                negativeNo = 0;
                positiveNo = 0;
                unknownNo = 0;

                text = (String) solrDocs.get(0).getFieldValue("Text");
                text = text.replace("\"", "");
                System.out.println(text.length());
                /*rp = new RomanianPolarity(text);

                rp.makeDepsList();
                depsList = rp.getDepsList();

                for (List<TypedDependency> deps : depsList) {
                    i++;

                    /*//* ROOT-0 *//**//*
                    gov = new TreeGraphNode(CoreLabel.factory().newLabel(dummyWord));
                    gov.label().setWord(dummyWord);
                    gov.label().setTag(dummyValue);
                    gov.label().setValue(dummyValue);
                    dep = new TreeGraphNode(CoreLabel.factory().newLabel(dummyWord));
                    dep.label().setWord(dummyWord);
                    dep.label().setTag(dummyValue);
                    dep.label().setValue(dummyValue);
                    typedDependency = new TypedDependency(null, gov, dep);

                    OMTree tree = rUtils.getTree(deps, typedDependency, true);
    //            showTree(tree, 1);

                    baseWords = TypedDependencyUtils.getBaseWords(i);

                    opinionPolarity = rUtils.getSentencePolarity(tree, affectiveDB, Constants.Language.RO, baseWords);
                    if (opinionPolarity == Constants.OpinionPolarity.NEGATIVE) {
                        negativeNo++;
                    }
                    else {
                        if (opinionPolarity == Constants.OpinionPolarity.POSITIVE) {
                            positiveNo++;
                        }
                        else {
                            if (opinionPolarity == Constants.OpinionPolarity.UNKNOWN) {
                                unknownNo++;
                            }
                        }
                    }
                    System.out.println("Opinion Polarity: " + opinionPolarity + " pentru " + baseWords);
                }*/
               /* System.out.println("Polaritati: Negative " + negativeNo + " Positive " + positiveNo + " Unknown " + unknownNo);
                PosPolarity.add(positiveNo.toString());
                NegPolarity.add(negativeNo.toString());
                UnkPolarity.add(unknownNo.toString());

                fields.put("PosPolarity", PosPolarity);
                fields.put("NegPolarity", NegPolarity);
                fields.put("UnkPolarity", UnkPolarity);*/

                //apdb.dbInsert(fields, articleIndex, "articles_polarity");
               // apdb.dbInsert();
            }
            else {
                PosPolarity.add("0");
                NegPolarity.add("0");
                UnkPolarity.add("0");
            }

        }

        apdb.closeConnection();

        //SolrDocumentList sdl = Extract("\"http://www.gandul.info/politica/angela-merkel-presedintelui-romaniei-nu-i-a-fost-niciodata-frica-sa-se-gandeasca-la-viitor-chiar-daca-aceste-actiuni-nu-au-fost-totdeauna-cele-mai-populare-10236708\"", 0, 10);
        //System.out.println(sdl.get(0).getFieldNames());
        //text = sdl.get(0).getFieldValue("Text"));
        //System.out.println(sdl.get(0).getFieldValue("content"));
    }
}
