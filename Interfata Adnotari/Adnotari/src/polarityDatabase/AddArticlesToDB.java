package polarityDatabase;

import general.Constants;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;






import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;

import dataaccess.DataBaseConnection;
import dataaccess.OpinionMiningDBAdapter;

public class AddArticlesToDB {
	
	public static OpinionMiningDBAdapter.OpinionMiningDB dump;
	
	//extrage linkurile din tabela care pastreazas asocierea dintre adresa articolului si entitatile pe care le contine
	/**
	 * 
	 * @param numberOfRows numarul maxim de linii pe care il selecteaza din tabela care contine asocierile dintre link si entitati
	 * @throws SQLException
	 * @throws IOException 
	 */
	public static void extractArticlesWithEntities (int numberOfRows) throws SQLException, IOException {
		/*String lastIdQuery = "SELECT articleid from articles "
							+ "ORDER BY articleid DESC "
							+ "LIMIT 1";
		
		
		int lastID = Integer.parseInt(DataBaseConnection.executeQuery(Constants.ADNOTARI_DB, lastIdQuery, 1).get(0).get(0).toString());
		System.out.println("Last id: " + lastID);
		
		//ia ultimele numberOfRows linii din tabela url_entitati_full
		String query = "SELECT id, url FROM "
				+ "(SELECT id, url "
				+ "FROM url_entitati_full "
				+ "WHERE id > " + lastID + " "  
				+ "ORDER BY id DESC) "
				//+ "LIMIT " + numberOfRows + ") "
				+ "AS new_select "
				+ "GROUP BY url "
				+ "ORDER BY id ASC";*/

		String firstIdQuery = "SELECT articleid from articles "
				+ "ORDER BY articleid ASC "
				+ "LIMIT 1";
		
		int firstID = Integer.parseInt(DataBaseConnection.executeQuery(Constants.ADNOTARI_DB, firstIdQuery, 1).get(0).get(0).toString());
		System.out.println("First id: " + firstID);
		
		String query = "SELECT id, url FROM "
				+ "(SELECT id, url "
				+ "FROM url_entitati_full "
				+ "WHERE id < " + firstID + " "  
				+ "ORDER BY id DESC )"
//				+ "LIMIT " + numberOfRows + ") "
				+ "AS new_select "
				+ "GROUP BY url "
				+ "ORDER BY id ASC";
		
		System.out.println("QUERY: " + query);
		int numberOfColumns = 2;
		
		//ArrayList<ArrayList<Object>> result = DataBaseConnection.getTableContent(Constants.ENTITATI_DB, "articles", attributes, null, null, null);
		ArrayList<ArrayList<Object>> result = DataBaseConnection.executeQuery(Constants.ENTITATI_DB, query, numberOfColumns);
		for (int i = 0; i < result.size(); i++) {
			boolean containsEntities = false;
			System.out.println("Articol " + i );
			ArrayList<Object> value = result.get(i);
			
			int articleIndex = Integer.parseInt(value.get(0).toString());
			String articleURL = value.get(1).toString();
			
			String articleText = extractArticleFromSolr(articleURL);
			ArrayList<String> articleEntities = ExtractEntitiesForLink(articleURL);
			HashMap<String, String> completeArticleEntities = new HashMap<>();
			HashSet<String> entitiesVersions = extractVersionsForEntities(articleEntities, completeArticleEntities);
			
			containsEntities = selectParagraphsForDB(articleText, entitiesVersions, articleIndex);
//			insertEntitiesToDB(completeArticleEntities, articleIndex);
			if (containsEntities) {
				insertEntitiesToDB(articleEntities, completeArticleEntities, articleIndex);
//				System.out.println("Adauga in entitati_articol " + completeArticleEntities);
//				System.out.println("Link: " + articleURL + "id articol: " + articleIndex);
			}
		}
	}
	
	/**
	 * Extrage din solr articolul corespunzator linkului primit ca parametru
	 * 
	 * @param articleLink linkul articolului
	 * @return Textul continut in articol
	 */
	private static String extractArticleFromSolr(String articleLink) {
		SolrDocs solrdocs = new SolrDocs();
		SolrDocumentList documents = solrdocs.Extract("Source:", articleLink, 0, 1);
		SolrDocument document = documents.get(0);
		//System.out.println("======================== Text ========================");
		String text = document.getFieldValue("Text").toString();
		return text;
	}
	
	/**
	 * Updateaza tabela care contine textele pentru adnotat
	 * 
	 * @param text Textul extras dintr-un articol
	 * @param entities entitatile identificate in acel articol
	 * @throws SQLException 
	 * @throws IOException 
	 */
	private static boolean selectParagraphsForDB(String text, HashSet<String> entities, int articleIndex) throws SQLException, IOException {
		boolean containsEntities = false;
		String[] paragraphs = text.split("\n");
		for (int i = 0; i < paragraphs.length; i++) {
			String paragraph = paragraphs[i].trim();
			if (paragraph.length() > 10 && paragraphContainsEntities(paragraph, entities)) {
				// add paragraph to database in order to be annotated
				PolarWords pw = new PolarWords(paragraph, dump);
				ArrayList<Double> posNegPolarity = pw.computePolarWords();
//				System.out.println("polaritati: " + posNegPolarity);
				if (posNegPolarity.get(0) > Constants.POSITIVE_VALUE || posNegPolarity.get(1) < Constants.NEGATIVE_VALUE) {
					containsEntities = true;
					System.out.println("Paragraph: " + paragraph);
					if (!textExists(paragraph)) {
						System.out.println("----------------- Insereaza paragraful ----------");
						paragraph = paragraph.replace("\"", "\\\"");
						String expression = "INSERT INTO articles VALUES(DEFAULT, "
										+ "\"" + paragraph + "\", " +
										 "(SELECT NOW()), " +
										 "\'" + articleIndex +"\'" +
										 ")";
						// add entities to database in order to help the annotation proccess;
						DataBaseConnection.insertQuery(Constants.ADNOTARI_DB, expression);
		//				System.out.println("------------------- Paragraf -----------------");
		//				System.out.println(paragraph);
					}
				}
			}
		}
		return containsEntities;
	}
	
	/**
	 * 
	 * @param link Linkul articolului
	 * @return	Un ArrayList ce contine entitatile identificate in acest articol
	 * @throws SQLException
	 */
	private static ArrayList<String> ExtractEntitiesForLink(String link) throws SQLException {
		ArrayList<String> entities = new ArrayList<>();
		String query = "SELECT  nume "
				+ "FROM `url_entitati_full` "
				+ "WHERE url = \""
				+ link + "\"";
		int numberOfColumns = 1;
		ArrayList<ArrayList<Object>> result = DataBaseConnection.executeQuery(Constants.ENTITATI_DB, query, numberOfColumns);
		for (int i = 0; i < result.size(); i++) {
			ArrayList<Object> value = result.get(i);
//			System.out.println(value.get(0));
			entities.add(value.get(0).toString());
		}
		return entities;
	}
	
	/**
	 * Pentru fiecare entitate detectata, adauga in vectorul de entitati toate variantele pentru fiecare entitate
	 * Aceste variante se iau din baza de date opinionm_minig, tabela clasificator3
	 * @param entities entitatile extrase din articol
	 * @return entitatile la care s-au adaugat toate variantele identificate pentru fiecare entitate din multimea initiala
	 * @throws SQLException 
	 */
	private static HashSet<String> extractVersionsForEntities(ArrayList<String> entities, HashMap<String, String> commaSeparatedEntityVersions) throws SQLException {
		HashSet<String> entitiesVersions = new HashSet<>();
		entitiesVersions.addAll(entities);
		for (String entity : entities) {
//			System.out.println("Entity: " + entity);
			entity = entity.replace("ş", "ș");
			String query = "SELECT versiuni FROM clasificator3 "
					+ "WHERE nume = _utf8\"" + entity + "\"";

			int numberOfColumns = 1;
			ArrayList<ArrayList<Object>> result = DataBaseConnection.executeQuery(Constants.ENTITATI_DB, query, numberOfColumns);
//			System.out.println("Size: " + result.size());
			for (int i = 0; i < result.size(); i++) {
				ArrayList<Object> value = result.get(i);
				commaSeparatedEntityVersions.put(entity, value.get(0).toString().trim().replaceAll("\\xa0+$", ""));
				String[] entityVersions = value.get(0).toString().split(", "); 
				for (int j = 0; j < entityVersions.length; j++) {
//					System.out.println("Entitate:" + entityVersions[j].replaceAll("[\\t\\s\\xa0]+$", "") + "===");
					entitiesVersions.add(entityVersions[j].trim().replaceAll("\\xa0+$", ""));
				}
			}
		}
		return entitiesVersions;
	}
	
	private static boolean paragraphContainsEntities(String paragraph, HashSet<String> entities) {
		String text = paragraph.toLowerCase();
		for (String entity : entities) {
			if (text.contains(entity.toLowerCase())) {
//				System.out.println("\nTEXTUL " + text + " contine entitatea " + entity);
				return true;
			}
		}
		return false;
	}
	
	/*public static void insertEntitiesToDB(HashSet<String> entities, int articleIndex) throws SQLException {
		for (String entity : entities) {
			String query = "INSERT INTO article_entities VALUES "
					+ "(DEFAULT, \"" + entity + "\", " +
					articleIndex + ")";
			DataBaseConnection.insertQuery(Constants.ADNOTARI_DB, query);
		}
	}*/
	
	public static void insertEntitiesToDB(ArrayList<String> entities, HashMap<String, String> entitiesVersions, int articleIndex) throws SQLException {
		for (String entity : entities) {
			String query = "INSERT INTO article_entities VALUES "
					+ "(DEFAULT, \"" + entity + "\", \"" + 
					entitiesVersions.get(entity) + "\", " +
					articleIndex + ")";
			DataBaseConnection.insertQuery(Constants.ADNOTARI_DB, query);
		}
	}
	
	public static boolean textExists(String text) {
		String query = null;
		query = "SELECT article FROM articles "
				+ "WHERE md5(article) = md5('" + text + "')";
		//System.out.println(query));
		
		try {
			ArrayList<ArrayList<Object>> result = DataBaseConnection.executeQuery(Constants.ADNOTARI_DB, query, 1);
			
			if (result.size() >= 1 && result.get(0).size() >= 1) {
				System.out.println("============= Articolul exista ============");
//				System.out.println(result.get(0).get(0));
				return true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("============= Articolul nu exista ============");
		return false;
	}
	
	public static void main(String[] args) throws IOException {
		try {
			dump = new OpinionMiningDBAdapter().dump();
			System.out.println("Executa");
			extractArticlesWithEntities(10000);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}























