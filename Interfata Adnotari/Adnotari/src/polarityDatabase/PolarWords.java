package polarityDatabase;
import general.Constants;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import dataaccess.OpinionMiningDBAdapter;
import dataaccess.OpinionMiningDBAdapter.OpinionMiningDB;

/**
 * 
 * @author andreea.badoiu
 *
 * Numara cate cuvinte cu polaritati pozitive sau negative apar. Se face o medie pentru
 * polaritatea pozitiva si pentru polaritatea negativa.
 * 
 */
public class PolarWords {
	private String 									text;					/* text to be processed */
	private OpinionMiningDBAdapter.OpinionMiningDB	dump;                   /* dump database */
	private double									posScore;
	private double 									negScore;
	private int 									wordsNumber;
	private String[]			 					words;
	
	public PolarWords(String text, OpinionMiningDB dump) throws SQLException {
		this.text = text;
		posScore = 0;
		negScore = 0;
		this.dump = dump;
	}
	
	public double getNegScore() {
		return negScore;
	}
	
	public double getPosScore() {
		return posScore;
	}
	
	public int getWordsNumber() {
		return wordsNumber;
	}
	
	public void countPosAndNegWords() {
		words = text.split("[ .?!…]+");
		wordsNumber = words.length;
		
		double value;
		Constants.AffectiveDB affectiveDB = general.Constants.AffectiveDB.SENTICNET;
		
		for (String word : words) {
			OpinionMiningDBAdapter.AffectiveScore polarity = dump.ro_scores.get(word);
            if (polarity != null) {
            	
            	value = polarity.getValue(affectiveDB)/* * 100*/;
//              	System.out.println(word + " polarity " + value);
                if (value < 0) {
                	negScore += value;
                } 
                else if (value >= 0) {
                	posScore += value;
                }
            
            }
		}
	}
	
	public ArrayList<Double> computePolarWords() throws IOException {
		ArrayList<Double> 	result = new ArrayList<>();
		double 				posAvg;
		double 				negAvg;
		
		countPosAndNegWords();
		
		if (wordsNumber == 0) return null;
		
		result.add(posScore/wordsNumber);
		result.add(negScore/wordsNumber);
		
		// keep the result in a csv file
		BufferedWriter bw = new BufferedWriter(new FileWriter("out/polaritati.csv", true));
		bw.append(result.get(0) + ", " + result.get(1) + "\n");
		bw.close();
		
		return result;
	}
	
	public static void main(String[] args) throws SQLException, IOException {
		//PolarWords pw = new PolarWords("Ana are mere!.. ea merge la scoala. Copilul a ajuns acasa? ce bine! asta este... … ana are	 mere …");
		//pw.computePolarWords();
		//System.out.println("Negativ: " + pw.getNegScore() + "\nPozitiv: " + pw.getPosScore());
	}
}
