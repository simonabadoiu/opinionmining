package polarityDatabase;

/**
 *
 * @author alexandru.dinca
 */
public class Constants {
   
	/* path to auxiliary files */
    //public static final filePath = "C:/"

    public static final String PERIOD = ".";
    public static String PROJECT_NAME = "Analiza și identificarea polarității opiniilor în texte";

    public enum OpinionPolarity {
        UNKNOWN, NEGATIVE, POSITIVE
    }

    public enum AffectiveScoreModel {
        LINEAR, LOGARITHMIC
    }

    public enum AffectiveDB {
        SENTICNET, SENTIWN, ANEW
    }

    /**
     * The current supported languages.
     */
    public enum Language {
        RO {
            @Override
            public String toString() {
                return "ro";
            }
        },
        EN {
            @Override
            public String toString() {
                return "en";
            }
        }
    }

    public static final String dummyValue = "UNKNOWN";
    public static final String dummyWord = "ROOT-0";

    /**
     * Files used by RomanianParser
     */


    /**
     * Files used by FrenchParser
     */

    /**
     * The current supported parsers
     */
    public enum Parser {
        RO_PARSER {
            @Override
            public String toString() {
                return "ro_parser";
            }
        }, FR_PARSER {
            @Override
            public String toString() {
                return "fr_parser";
            }
        }
    }

    /* Opinion's delimiters.
     * An opinion (Opinion) is a list sentences (Sentence) delimited by SENTENCE_DELIMS.
     * A sentence is a list of tokens (OMToken) delimited by OMTOKEN_DELIMS.
     */
    public static final String SENTENCE_DELIMS = ".!?…;";
    public static final String OMTOKEN_DELIMS = " ,\r\n";
    public static final String DELIMS = " ,\r\n;.!…?\":&()[]{}/\\-+*";

    /* Types of opinions */
    public static final int UNKNOWN_TYPE = 0;
    public static final int CUSTOMER_REVIEW = 1;
    public static final int BLOGGER_REVIEW = 2;

    /* Negation words */
    public static final String[] NEGATION_WORDS_EN = {
            "not", "n't", "never", "none", "nobody", "nowhere", "neither", "cannot"};

    public static final String[] NEGATION_WORDS_RO = {
            "nu"};

    /* List of intensifiers */
    public static final String[] INTENSIFIERS_EN = {
            "deeply", "rather", "very", "extremely", "lack", "few", "most"};
    public static final String[] INTENSIFIERS_RO = {
            "degrabă", "foarte", /*"lipsă",*/ "deosebit", "exagerat", "extrem", "mai"};

    /* Modals - neutralize the base valence of the words */
    public static final String[] MODALS_EN = {
            "might", "should", "could", "ought", "may", "can", "must", "will", "would"
    };

    public static final String[] MODALS_RO = {
            //"posibil", "poate", "putea", "trebui",
            "ar", "aș", "ai", "am", "ați"
    };

    /* General polarity shifters */
    public static final String[] EN_GEN_POL_SHIFTERS = {
            "little"
    };

    public static final String[] RO_GEN_POL_SHIFTERS = {
            "puțin", "scădea", "diminua", "chiar_dacă", "reduce", "scădea", "atenua", "împuțina", "repara"
    };

    /* Negative polarity shifters */
    public static final String[] EN_NEG_POL_SHIFTERS = {
            "lack"
    };

    public static final String[] RO_NEG_POL_SHIFTERS = {
            "lipsă", "renunţa", "pierde"
    };

    /* Positive poalrity shifters */
    public static final String[] EN_POS_POL_SHIFTERS = {
            "abate", "reduce", "attenuate", "soften"
    };

    public static final String[] RO_POS_POL_SHIFTERS = {
            "reduce", "alina", "micșora", "anula", "atenua", "calma", "potoli", "repara"
    };

    /* Connectors */
    public static final String[] EN_CONNECTORS = {
            "although", "however", "but", "notwithstanding"
    };

    public static final String[] RO_CONNECTORS = {
            "deși", "totuși", "dar", "însă", "oricum", "orișicum", "orișicât", "oricât"
            /* oricum cred ca este mai mult intensifier */
    };

    /* Reported speech */
    public static final String[] EN_REP_SPEECH = {
            "said", "remembers", "think", "thinks", "believe", "believes",
    };

    public static final String[] RO_REP_SPEECH = {
            "spune", "crede", "zice", "amintește"
    };

    /* ANEW constants - MINs & MAXs for ANEW word */
    public static final double VAL_MN_MIN = 1.25;   // rape
    public static final double VAL_MN_MAX = 8.82;   // triumphant
    public static final double ARO_MN_MIN = 2.29;   // boring
    public static final double ARO_MN_MAX = 8.17;   // rage
    public static final double DOM_MN_MIN = 2.17;   // rejection
    public static final double DOM_MN_MAX = 7.88;   // leader

    /*
     * Statuses for a tree node
     */
    public static final int  NOT_VISITED = 0;
    public static final int  VISITING = 1;
    public static final int  VISITED = 2;

    /*
     * Affective databases names.
     * There is code using the constants for accesing tables columns.
     */
    public static final String AFFECT_DB_SENTICNET = "senticnet";
    public static final String AFFECT_DB_SENTIWN = "sentiwordnet";
    public static final String AFFECT_DB_ANEW = "anew";

    public static final String INDEX_28K_ADJECTIVES_FILE = "db/dict/28K_adjectives.txt";

    /* one index for for each affective database */
    public static final String DB_INDEX_ANEW = "db/dict/ANEW2010All.txt";
    public static final String DB_INDEX_SENTICNET = "db/dict/sentic_concepts.txt";
    public static final String DB_INDEX_SENTIWN = "db/dict/SentiWordNet_3.0.0.txt";
    public static final String DB_INDEX_SENTIWN_NEW = "db/dict/SentiWordNet_3.0.0_20130122.txt";

    /* path for DB storage */
    public static final String DB_DATA_ANEW = "db/dict/ANEW/";
    public static final String DB_DATA_SENTICNET = "db/dict/SenticNet/";
    public static final String DB_DATA_ALTADICT = "db/dict/AltaDict/";
    public static final String DB_DATA_GOOGLE_ADJ = "db/dict/google-adjectives/";
    public static final String DB_DATA_NETFLASH = "db/dict/NetFlash/";
    public static final String DB_DATA_NETFLASH_NOT_EXISTING = "db/dict/NetFlash_not_existing";

    /* properties constants */
    public static final String MYSQL_DEXONLINE_PROPERTIES_FILEPATH = "properties/mysql-dexonline-properties.xml";
    public static final String MYSQL_OPINIONMINING_PROPERTIES_FILEPATH = "properties/mysql-opinionmining-properties.xml";

    /* corpus constants */
    public static final String CORP_REVIEW_POLARITY_PATH = "db/corpus/review_polarity/txt_sentoken/all/";
    public static final String CORP_REVIEW_POLARITY_ADJS_PATH = "db/corpus/review_polarity/txt_sentoken/all_adjs/";

    /* POS taggers constants */
    public static final String TAGGER_STANFORD_WSJ018_BIDIR_DISTSIM = "lib/stanford-postagger-2012-11-11/models/wsj-0-18-bidirectional-distsim.tagger";

    public static final String STOP_WORDS_FILE = "input/stopwords_ro.txt";
}
