package polarityDatabase;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * User: andreea.badoiu
 * Date: 8/9/13
 * Time: 3:06 PM
 * To change this template use File | Settings | File Templates.
 */
public class ArticlePolarityDB {
    final String         url = "jdbc:mysql://localhost/";
    final String         driver = "com.mysql.jdbc.Driver";
    final String         username = "root";
    final String         passw = "root";
    public Connection    conn;
    public Statement     stmt;

    public void createTable(String dbName, String tableName){

        try {

            /*Class.forName(driver);
            conn = DriverManager.getConnection(url + dbName, username, passw);
            stmt = conn.createStatement();*/

            String sql = "CREATE TABLE " + tableName +
                         " (id INTEGER not NULL, " +
                         " Article VARCHAR(255), " +
                         " ArticleDate DATE, " +
                         " Domain VARCHAR(255), " +
                         " PosPolarity VARCHAR(255), " +
                         " NegPolarity VARCHAR(255), " +
                         " UnkPolarity VARCHAR(255), " +
                         " TotalNo INTEGER, " +
                         " AddingDate DATE, " +
                         " Length INTEGER, " +
                         " PRIMARY KEY ( id ))";
            stmt.executeUpdate(sql);

        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } /*finally {
            try {
                if (stmt != null) {
                    conn.close();
                }
            } catch (SQLException e) {
            }

            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }*/
    }

    /* get corresponding link to every article */
    public HashMap<String, ArrayList<String>> getFromTable(String tableName) {
        ResultSet           rs;
        HashMap<String, ArrayList<String>> dbData;
        ArrayList<String>   ids;
        ArrayList<String>   links;
        ArrayList<String>   articleDates;
        ArrayList<String>   domains;
        ArrayList<String>   totalNos;
        ArrayList<String>   addingDates;

        dbData = new HashMap<>();
        ids = new ArrayList<>();
        links = new ArrayList<>();
        articleDates = new ArrayList<>();
        domains = new ArrayList<>();
        totalNos = new ArrayList<>();
        addingDates = new ArrayList<>();

        try {

            String sql = "SELECT * from " + tableName + " LIMIT 15";
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Integer id = rs.getInt("id");
                String articleLink = rs.getString("Article");
                String articleDate = rs.getString("DataArticol");
                String domain = rs.getString("Domeniu");
                Integer totalNo = rs.getInt("TotalNr");
                String addingDate = rs.getString("DataAdaugare");
                //System.out.println(id + " " + articleLink + " " + articleDate + " " + domain + " " + totalNo + " " + addingDate);

                ids.add(id.toString());
                links.add(articleLink);
                articleDates.add(articleDate);
                domains.add(domain);
                totalNos.add(totalNo.toString());
                addingDates.add(addingDate);
            }

        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        dbData.put("id", ids);
        dbData.put("Article", links);
        dbData.put("ArticleDate", articleDates);
        dbData.put("Domain", domains);
        dbData.put("TotalNo", totalNos);
        dbData.put("AddingDate", addingDates);

        return dbData;
    }

    public void openConnection(String dbName) {

        try {
            Class.forName(driver);
            conn = DriverManager.getConnection(url + dbName, username, passw);
            stmt = conn.createStatement();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    public void closeConnection() {
        try {
            if (stmt != null) {
                conn.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

   public void dbInsert(ArrayList<String> fieldsVals, String tableName) {
        int i = 0;
        String sql = "INSERT INTO " + tableName + " VALUES (";
        Iterator<String> it = fieldsVals.iterator();
        while (it.hasNext()) {
            if (i == 0) {
                sql += it.next();
                i++;
            }
            else {
                sql += ", " + it.next();
            }
        }
        sql += ")";
        System.out.println("SQL query: " + sql);

        try {
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }


    /*public void dbInsert(HashMap<String, ArrayList<String>> fields, int articleIndex, String tableName) {
        String sql = "INSERT into " + tableName +
                     " VALUES (" + fields.get("id").get(articleIndex) +
                     ", \'" + fields.get("Article").get(articleIndex) +
                     "\', \'" + fields.get("ArticleDate").get(articleIndex) +
                     "\', \'" + fields.get("Domain").get(articleIndex) +
                     "\', " + fields.get("PosPolarity").get(articleIndex) +
                     ", " + fields.get("NegPolarity").get(articleIndex) +
                     ", " + fields.get("UnkPolarity").get(articleIndex) +
                     ", 0" +
                     ", \'" + fields.get("AddingDate").get(articleIndex) +
                     "\')";
        System.out.println("SQL query: " + sql);
        try {
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }*/

    public ResultSet sqlQuery(String query) {
        ResultSet result = null;

        try {
            result = stmt.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            return result;
        }
    }

    public boolean exists(int id, String tableName) {
        ResultSet result = null;
        String sql = "SELECT * from " + tableName + " where id=" + id;
        System.out.println("--------------------------------------------\n" +  sql + "\n-------------------------------------");
        try {
            result = stmt.executeQuery(sql);
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } finally {
            if (result == null) {
                return false;
            }
            else {
                try {
                    if (result.next())
                        return true;
                } catch (SQLException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        }
        return false;
    }

    public boolean exists(String link, String tableName) {
        ResultSet result = null;
        String sql = "SELECT * from " + tableName + " where Article=" + link;
        System.out.println("--------------------------------------------\n" +  sql + "\n-------------------------------------");
        try {
            result = stmt.executeQuery(sql);
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } finally {
            if (result == null) {
                return false;
            }
            else {
                try {
                    if (result.next())
                        return true;
                } catch (SQLException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        }
        return false;
    }
}
