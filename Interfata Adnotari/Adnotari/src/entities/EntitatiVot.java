package entities;

import java.util.ArrayList;
import javafx.beans.property.SimpleStringProperty;

public class EntitatiVot extends Entity {
    final private SimpleStringProperty      entitate;

    
    public EntitatiVot(String entitate) {
        this.entitate      = new SimpleStringProperty(entitate);
    }
    
    public EntitatiVot(ArrayList<Object> entities) {
        this.entitate	    	= new SimpleStringProperty(entities.get(0).toString());
    }
    
    public String getEntitate() {
        return entitate.get();
    }  
    
    public void setEntitate(String entitate) {
        this.entitate.set(entitate);
    }   
    
    @Override
    public ArrayList<String> getValues() {
        ArrayList<String> values = new ArrayList<>();
        values.add(entitate.get());
        return values;
    }
}