package entities;

import java.util.ArrayList;
import javafx.beans.property.SimpleStringProperty;

public class AdnotariEntitati extends Entity {
    final private SimpleStringProperty      entitate;
    final private SimpleStringProperty      polaritate;
    final private SimpleStringProperty      idarticol;

    
    public AdnotariEntitati(String entitate, String polaritate, String idarticol) {
        this.entitate      = new SimpleStringProperty(entitate);
        this.polaritate    = new SimpleStringProperty(polaritate);
        this.idarticol     = new SimpleStringProperty(idarticol);
    }
    
    public AdnotariEntitati(ArrayList<Object> entities) {
        this.entitate	    = new SimpleStringProperty(entities.get(0).toString());
        this.polaritate        = new SimpleStringProperty(entities.get(1).toString());
        this.idarticol     = new SimpleStringProperty(entities.get(2).toString());        
    }
    
    public String getEntitate() {
        return entitate.get();
    }  
    
    public void setEntitate(String entitate) {
        this.entitate.set(entitate);
    }  
    
    public String getPolaritate() {
        return polaritate.get();
    }
    
    public void setPolaritate(String polaritate) {
        this.polaritate.set(polaritate);
    }
    
    public String getIdarticol() {
        return idarticol.get();
    }
    
    public void setIdarticol(String idarticol) {
        this.idarticol.set(idarticol);
    }   
    
    @Override
    public ArrayList<String> getValues() {
        ArrayList<String> values = new ArrayList<>();
        values.add(entitate.get());
        values.add(polaritate.get());
        values.add(idarticol.get());      
        return values;
    }
}