package entities;

import java.util.ArrayList;

import javafx.beans.property.SimpleStringProperty;

public class EntitatiVotate extends Entity {
    final private SimpleStringProperty      entitate;
    final private SimpleStringProperty      polaritate;

    
    public EntitatiVotate(String entitate, String polaritate) {
        this.entitate	= new SimpleStringProperty(entitate);
        this.polaritate	= new SimpleStringProperty(polaritate);
    }
    
    public EntitatiVotate(ArrayList<Object> entities) {
        this.entitate	= new SimpleStringProperty(entities.get(0).toString());
        this.polaritate	= new SimpleStringProperty(entities.get(1).toString());
    }
    
    public String getEntitate() {
        return entitate.get();
    }  
    
    public void setEntitate(String entitate) {
        this.entitate.set(entitate);
    }  
    
    public String getPolaritate() {
        return polaritate.get();
    }
    
    public void setPolaritate(String polaritate) {
        this.polaritate.set(polaritate);
    }
    
    @Override
    public ArrayList<String> getValues() {
        ArrayList<String> values = new ArrayList<>();
        values.add(entitate.get());
        values.add(polaritate.get());
        return values;
    }
}