package solraccess;

import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;

import polarityDatabase.ArticlePolarityDB;
import polarityDatabase.SolrDocs;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: andreea.badoiu
 * Date: 8/16/13
 * Time: 12:25 PM
 * To change this template use File | Settings | File Templates.
 */
public class ExtractFromSolr {

//    static public final String  category = "\"blog_comment\"";
    static public final String  category = "\"news\"";
//    static public final String  category = "\"fb_socialplugin\"";
//    static public final String  category = "\"blog_post\"";
    static public final Integer count = 15;
    static SolrDocumentList     solrDocs;
    static long                 totalDocsNo;
    static SolrDocs             solrd;

    public ExtractFromSolr() {
        solrd = new SolrDocs();
    }

    /**
     *
     * @param category the category in which are counted the documents
     */
    public static void getTotalNumberOfDocs(String category) {
        solrDocs = solrd.Extract("Category:", category, 0, 0);
        totalDocsNo = solrDocs.getNumFound();
    }

    /**
     *
     * @param category the category where the article si found
     * @param start from what document number should start the search
     * @param count the maximum number of documents returned
     */
    public static SolrDocumentList extractDocs(String category, int start, int count) {
        return solrd.Extract("Category:", category, start, count);
    }

    public static String getMonthNo(String month) {
        if (month.startsWith("Jan"))
            return "01";
        if (month.startsWith("Feb"))
            return "02";
        if (month.startsWith("Mar"))
            return "03";
        if (month.startsWith("Apr"))
            return "04";
        if (month.startsWith("May"))
            return "05";
        if (month.startsWith("Jun"))
            return "06";
        if (month.startsWith("Jul"))
            return "07";
        if (month.startsWith("Aug"))
            return "08";
        if (month.startsWith("Sep"))
            return "09";
        if (month.startsWith("Oct"))
            return "10";
        if (month.startsWith("Nov"))
            return "11";
        if (month.startsWith("Dec"))
            return "12";
        return "";
    }

    /**
     *
     * @param date a date in this format: Wed May 05 11:34:00 EEST 2010
     * @return a date in this format 2010-05-05 11:34:00
     */
    public static String makeDateFormat(String date) {

        String  year;
        String  month;
        String  newDate;
        ArrayList<String> dateArray = new ArrayList<>();

        StringTokenizer tk = new StringTokenizer(date);
        while (tk.hasMoreTokens()) {
            dateArray.add(tk.nextToken());
        }

        newDate = dateArray.get(5) + "-" + getMonthNo(dateArray.get(1)) + "-" + dateArray.get(2) + " " + dateArray.get(3);
        return newDate;
    }

    /**
     *
     * @param date a date with this format: 2012-12-11T05:34:28.564Z
     * @return a date with this format 2012-12-11 05:34:28
     */
    public static String getDateFormat(String date) {
        String newDate;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'.234Z'");
        Date resultdate = new Date(Long.parseLong(date));
        newDate = sdf.format(resultdate);

        StringTokenizer tk = new StringTokenizer(newDate, "T.");
        newDate = (tk.nextToken() + " " + tk.nextToken());

        return newDate;
    }

    public static ArrayList<String> makeFieldsArray(SolrDocument sd, Integer id, Integer pos, Integer neg, Integer unk) {
        ArrayList<String> dbFields = new ArrayList<>();
        System.out.println(sd.getFieldNames());
        dbFields.add(id.toString());
        dbFields.add("\'" + sd.getFieldValue("Source").toString() + "\'");
        System.out.println(sd.getFieldValue("DateTime_dt").toString());
        dbFields.add("\'" + makeDateFormat(sd.getFieldValue("DateTime_dt").toString()) + "\'");
        dbFields.add("\'" + sd.getFieldValue("Category").toString() + "\'");
        dbFields.add(pos.toString());
        dbFields.add(neg.toString());
        dbFields.add(unk.toString());
        dbFields.add("0");
//        dbFields.add("\'" + getDateFormat(sd.getFieldValue("tstamp").toString()) + "\'");
        System.out.println(sd.getFieldValue("Date").toString());
        dbFields.add("\'" + getDateFormat(sd.getFieldValue("Date").toString()) + "\'");
        dbFields.add(String.valueOf(sd.getFieldValue("Text").toString().length()));

        return dbFields;
    }
    
    public static ArrayList<String> makeFieldsArray(SolrDocument sd) {
    	 ArrayList<String> dbFields = new ArrayList<>();
    	System.out.println(sd.getFieldNames());
        /*dbFields.add("DEFAULT");
        dbFields.add("\'" + sd.getFieldValue("Source").toString() + "\'");
        System.out.println(sd.getFieldValue("DateTime_dt").toString());
        dbFields.add("\'" + makeDateFormat(sd.getFieldValue("DateTime_dt").toString()) + "\'");
        dbFields.add("\'" + sd.getFieldValue("Category").toString() + "\'");
        dbFields.add(pos.toString());
        dbFields.add(neg.toString());
        dbFields.add(unk.toString());
        dbFields.add("0");
//        dbFields.add("\'" + getDateFormat(sd.getFieldValue("tstamp").toString()) + "\'");
        System.out.println(sd.getFieldValue("Date").toString());
        dbFields.add("\'" + getDateFormat(sd.getFieldValue("Date").toString()) + "\'");
        dbFields.add(String.valueOf(sd.getFieldValue("Text").toString().length()));
*/
        return dbFields;
    }

    public static boolean isValid(String text, String oldText) {
        int length = text.length();

        if (length > 1200 || length == 0) {
            return false;
        }
        if (text.equals(oldText)) {
            return false;
        }

        return true;
    }

    public static void main(String[] args) throws SQLException {

        ArticlePolarityDB   apdb = new ArticlePolarityDB();
        List<Integer>       polarity = Arrays.asList(0, 0, 0);
        ArrayList<String>   dbFields;
//        RomanianPolarity    rp;
        Boolean             exists;
        String              dbName = "votedb";
        String              tableName = "short_articles_news_3";
        String              oldText = "";

        /* create table "short_articles" */
        apdb.openConnection(dbName);
        apdb.createTable(dbName, tableName);

        new ExtractFromSolr();
        getTotalNumberOfDocs(category);
        //totalDocsNo = 10;
        int start = 0;
        int id = 0;

        ResultSet idResult = apdb.sqlQuery("select id from " + tableName);
        /*while (idResult.next()) {
            id = idResult.getInt(1);
            System.out.println("IDDDDDDDDDDDDDDDDDDDDDDDDD " + id);
        }*/

        while (start < totalDocsNo) {
            solrDocs = extractDocs(category, start, count);
            Iterator<SolrDocument> it = solrDocs.iterator();
            while (it.hasNext()) {
                SolrDocument sd = it.next();
                //System.out.println(sd.getFieldNames());
                /*for (String fieldName : sd.getFieldNames()) {
                    System.out.println(fieldName + " - " + sd.getFieldValue(fieldName));
                }*/
                String text = sd.getFieldValue("Text").toString();
                System.out.println("Text:\n" + text + "\nLink:\n" +  sd.getFieldValue("Source"));
                //String text = "Ana nu are mere urâte.";
                /*String link = (String)sd.get("Source");
                System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                System.out.println(text.length());
                System.out.println("Document id: " + id + "\n" + text);
                System.out.println(link);*/

                /* compute polarity for this text */
                exists = apdb.exists(id + 1, tableName);
                //exists = apdb.exists(sd.getFieldValue("Source").toString(), tableName);
                if (exists && isValid(text, oldText)) {
                    id++;
                    oldText = text;
                    continue;
                }
                if (isValid(text, oldText)) {
//                   RomanianPolarity rp = new RomanianPolarity(text);
                   /* tp = new TextPolarity(text);
//                    tp.computePolarity(Constants.Parser.RO_PARSER, Constants.AffectiveDB.SENTICNET);
                    try {
                        polarity = tp.computePolarity(Constants.Parser.RO_PARSER, Constants.AffectiveDB.SENTICNET);
                        System.out.println("Polaritatile aici: " + polarity.get(0) + " " + polarity.get(1) + " " + polarity.get(2));
                    }  catch (NullPointerException e) {
                        e.printStackTrace();
                        System.out.println("Pentru tot textul");
                        oldText = text;
                        continue;
                    } catch (IndexOutOfBoundsException e) {
                        e.printStackTrace();
                        System.out.println("Pentru tot textul");
                        oldText = text;
                        continue;
                    } catch (NoSuchElementException e) {
                        e.printStackTrace();
                        System.out.println("Pentru tot textul");
                        oldText = text;
                        continue;
                    }*/

                    /* prepare fields for short_articles database */
                    id++;
                    dbFields = makeFieldsArray(sd, id, polarity.get(0), polarity.get(1), polarity.get(2));
                    apdb.dbInsert(dbFields, "short_articles_news_3");
                }
                oldText = text;
            }
            start += count;
        }
        apdb.closeConnection();
    }
}
