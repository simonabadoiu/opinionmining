package general;

public class Constants {
	final public static String      DATABASE_CONNECTION         = "jdbc:mysql://10.6.35.99:3306/adnotari?useUnicode=yes&characterEncoding=utf8";
    final public static String      DATABASE_NAME               = "adnotari";
    final public static String      DATABASE_USER               = "root";
    final public static String      DATABASE_PASSWORD           = "";
    
    final public static String      DATABASE_CONNECTION1         = "jdbc:mysql://10.6.35.99:3306/opinion_mining?useUnicode=yes&characterEncoding=utf8";
    final public static String      DATABASE_NAME1               = "opinion_mining";
    final public static String      DATABASE_USER1               = "root";
    final public static String      DATABASE_PASSWORD1           = "";
    
    final public static int			ADNOTARI_DB					= 1;
    final public static int			ENTITATI_DB					= 2;
    
	public static final boolean 	DEBUG 						= false;
	
	public static final String MYSQL_OPINIONMINING_PROPERTIES_FILEPATH = "properties/mysql-opinionmining-properties.xml";
	
	public enum AffectiveDB {
        SENTICNET, SENTIWN, ANEW
    }
	
	public enum Language {
        RO {
            @Override
            public String toString() {
                return "ro";
            }
        },
        EN {
            @Override
            public String toString() {
                return "en";
            }
        }
    }
	
	final public static Double		POSITIVE_VALUE				= 39.3;
	final public static Double		NEGATIVE_VALUE				= -5.0;
}
