package dataaccess;

import java.util.ArrayList;

public class TextDeAdnotat {
	public static String			text;
	public static ArrayList<String>	entitati;
	public static int				userId;
	
	public TextDeAdnotat(int userId) {
		this.userId = userId;
	}
	
	/*
	 * TODO verifica in tabela in care se retin voturile de la un utilizator, pana la ce
	 * id articol/paragraf a adnotat. Se va alege primul articol pe care nu l-a adnotat inca
	 */
	public void findNextTextId() {
		/*
		 * TODO se ia primul id lipsa din lista de texte adnotate asociate acestui utilizator
		 */
	}
	
	public String extractTextAtId() {
		return null;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public String getText() {
		return text;
	}
	
	public ArrayList<String> getEntitati() {
		return entitati;
	}
	
	public void setEntitati(ArrayList<String> entitati) {
		this.entitati = entitati;
	}
	
	public int getUserId() {
		return userId;
	}
	
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
}
