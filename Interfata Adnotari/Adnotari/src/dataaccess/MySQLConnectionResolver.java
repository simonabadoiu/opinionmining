package dataaccess;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * @author alexandru.dinca
 */
public class MySQLConnectionResolver {

    private Properties prop;
    private String dbms;
    private String jarFile;
    private String dbName;
    private String userName;
    private String password;
    private String urlString;
    private String driver;
    private String serverName;
    private int portNumber;

    public MySQLConnectionResolver(String pathToPropertiesFile) {
        try {
            this.setProperties(pathToPropertiesFile);
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        }
    }

    private void setProperties(String path) throws FileNotFoundException, IOException {

        this.prop = new Properties();
//        URL url = getClass().getResource(path);
        FileInputStream fis = new FileInputStream(path);
//        File file = new File(url.toURI());
//        FileInputStream fis = new FileInputStream(file);
        prop.loadFromXML(fis);
       
        this.dbms = this.prop.getProperty("dbms");
        this.jarFile = this.prop.getProperty("jar_file");
        this.driver = this.prop.getProperty("driver");
        this.dbName = this.prop.getProperty("database_name");
        this.userName = this.prop.getProperty("user_name");
        this.password = this.prop.getProperty("password");
        this.serverName = this.prop.getProperty("server_name");
        this.portNumber = Integer.parseInt(this.prop.getProperty("port_number"));

    }

    public Connection getConnection() throws SQLException {

        Connection conn = null;
        Properties connectionProps = new Properties();

        connectionProps.put("user", this.userName);
        connectionProps.put("password", this.password);

        switch (this.dbms) {
            case "mysql":
                String currentUrlString = "jdbc:" + this.dbms + "://" + this.serverName + ":" + this.portNumber + "/";
                System.out.println("Asta: " +currentUrlString);
                //conn = DriverManager.getConnection(currentUrlString, connectionProps);
                conn = DriverManager.getConnection(currentUrlString + this.dbName + "?useUnicode=true&characterEncoding=UTF-8", connectionProps);
                    /* useUnicode and characterEncoding were added to ensure diacritics integration */
                this.urlString = currentUrlString + this.dbName + "?useUnicode=true&characterEncoding=UTF-8";
                conn.setCatalog(this.dbName);
                break;
            case "derby":
                this.urlString = "jdbc:" + this.dbms + ":" + this.dbName;
                conn = DriverManager.getConnection(this.urlString + ";create=true", connectionProps);
                break;
        }

        return conn;
    }
}