package dataaccess;

import general.Constants;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.sql.*;
import java.util.ArrayList;

public class DataBaseConnection {
    public static Connection        dbConnection;
    public static DatabaseMetaData  dbMetaData;
    public static Statement         stmt;

    public DataBaseConnection() { }

    public static void openConnection(int database) throws SQLException {
        if (database ==  Constants.ADNOTARI_DB) {
	    	dbConnection    = DriverManager.getConnection(Constants.DATABASE_CONNECTION, Constants.DATABASE_USER, Constants.DATABASE_PASSWORD);
	        dbMetaData      = dbConnection.getMetaData();
	        stmt            = dbConnection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
	        return;
        }
        
        if (database == Constants.ENTITATI_DB) {
	    	dbConnection    = DriverManager.getConnection(Constants.DATABASE_CONNECTION1, Constants.DATABASE_USER1, Constants.DATABASE_PASSWORD1);
	        dbMetaData      = dbConnection.getMetaData();
	        stmt            = dbConnection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
	        return;
        }
    }

    public static void closeConnection() throws SQLException {
        stmt.close();
        dbConnection.close();
    }
 
    public static ArrayList<String> getTableNames(int database) throws SQLException {
        openConnection(database);
        ArrayList<String> result = new ArrayList<>();
        ResultSet RS = dbMetaData.getTables(Constants.DATABASE_NAME, null, null, null);
        while (RS.next()) {
            result.add(RS.getString("TABLE_NAME"));
        }
        closeConnection();
        return result;
    }    
    
    public static int getTableNumberOfRows(String tableName, int database) throws SQLException {
        openConnection(database);
        String query = "SELECT COUNT(*) FROM "+tableName;
        ResultSet RS = stmt.executeQuery(query);
        RS.next();
        int numberOfRows = RS.getInt(1);
        closeConnection();
        return numberOfRows;
    }
    
    public static int getTablePrimaryKeyMaxValue(String tableName, int database) throws SQLException {
        String primaryKey = getTablePrimaryKey(tableName, database);
        String query = "SELECT MAX("+primaryKey+") FROM "+tableName;
        openConnection(database);
        ResultSet RS = stmt.executeQuery(query);
        RS.next();
        int result = RS.getInt(1);
        closeConnection();
        return result;
    }
    
    public static int getTableNumberOfColumns(String tableName, int database) throws SQLException {
        int result = 0;
        openConnection(database);
        ResultSet RS = dbMetaData.getColumns(Constants.DATABASE_NAME, null, tableName, null);
        while (RS.next()) 
            result++;
        closeConnection();
        return result;
    }

    public static String getTablePrimaryKey(String tableName, int database) throws SQLException {
        String result = new String();
        openConnection(database);
        ResultSet RS = dbMetaData.getPrimaryKeys(Constants.DATABASE_NAME, null, tableName);
        while (RS.next())
            result += RS.getString("COLUMN_NAME")+" ";
        closeConnection();
        return result != null ? result.trim() : result;
    }   
    
    public static int getAttributeIndexInTable(String tableName, String attributeName, int database) {
        int result = -1;
        try {
            for (String tableAttribute: getTableAttributes(tableName, database)) {
                result++;
                if (tableAttribute.equals(attributeName))
                    return result;
            }
        } catch (Exception exception) {
            System.out.println ("Exceptie: "+exception.getMessage());
        }
        return -1;
    }            
    
    public static ArrayList<String> getTableAttributes(String tableName, int database) throws SQLException {
        ArrayList<String> result = new ArrayList<>();
        openConnection(database);
        ResultSet RS = dbMetaData.getColumns(Constants.DATABASE_NAME, null, tableName, null);
        while (RS.next())
            result.add(RS.getString("COLUMN_NAME"));
        closeConnection();
        return result;
    }

    public static ArrayList<ArrayList<Object>> getTableContent(int database, String tableName, ArrayList<String> attributes, String whereClause, String orderByClause, String groupByClause, String limitClause) throws SQLException {
        String expression = "SELECT ";
        int numberOfColumns = -1;
        if (attributes == null) {
            numberOfColumns = getTableNumberOfColumns(tableName, database);
            expression += "*";            
        }
        else {
            numberOfColumns = attributes.size();
            for (String attribute:attributes) {
                expression += attribute+", ";
            }
            expression = expression.substring(0,expression.length()-2);            
        }
        expression += " FROM "+tableName;
        if (whereClause != null) {
            expression += " WHERE "+whereClause;
        }
        if (groupByClause != null) {
            expression += " GROUP BY "+groupByClause;
        }
        if (orderByClause != null) {
            expression += " ORDER BY "+orderByClause;
        }
        
        if (limitClause != null) {
        	expression += " LIMIT " + limitClause;
        }
        	
        if (general.Constants.DEBUG) {
            System.out.println("query: "+expression);
        }
        if (numberOfColumns == -1) {
            return null;
        }      
        openConnection(database);
        ArrayList<ArrayList<Object>> dataBaseContent = new ArrayList<>();
        ResultSet result = stmt.executeQuery(expression);  
        int currentRow = 0;
        while (result.next()) {
            dataBaseContent.add(new ArrayList<>());
            for (int currentColumn = 0; currentColumn < numberOfColumns; currentColumn++) {
                dataBaseContent.get(currentRow).add(result.getString(currentColumn+1));
            }
            currentRow++;
        }
        closeConnection();
        return dataBaseContent;
    }

    public static void insertValuesIntoTable(int database, String tableName, ArrayList<String> attributes, ArrayList<String> values, boolean skipPrimaryKey) throws Exception {
        String expression = "INSERT INTO "+tableName+" (";
        if (attributes == null) {
            attributes = getTableAttributes(tableName, database);
            if (skipPrimaryKey) {
                attributes.remove(0);
            }
        }
        if (attributes.size() != values.size()) {
            throw new Exception ("Attributes size does not match values size !"+attributes.size()+" "+values.size());
        }
        for (String attribute:attributes) {
            expression += attribute + ", ";
        }      
        expression = expression.substring(0,expression.length()-2);
        expression += ") VALUES (";
        for (String currentValue: values) {
            expression += "\'"+currentValue+"\',";
        }
        expression = expression.substring(0,expression.length()-1);
        expression += ")";
        if (general.Constants.DEBUG) {
            System.out.println("query: "+expression);
        }
        openConnection(database);
        stmt.execute(expression);
        closeConnection();
    }

    public static void updateRecordsIntoTable(int database, String tableName, ArrayList<String> attributes, ArrayList<String> values, String whereClause) throws Exception {
        String expression = "UPDATE "+tableName+" SET ";
        if (attributes == null) {
            attributes = getTableAttributes(tableName, database);
        }
        if (attributes.size() != values.size()) {
            throw new Exception ("Attributes size does not match values size.");
        }
        for (int currentIndex = 0; currentIndex < values.size(); currentIndex++) {
            expression += attributes.get(currentIndex)+"="+"\'" + values.get(currentIndex) + "\'"+", ";
        }
        expression = expression.substring(0,expression.length()-2);
        expression += " WHERE ";
        if (whereClause != null ) {
            expression += whereClause;
        } else {
            expression += getTablePrimaryKey(tableName, database)+"\'"+values.get(0)+"\'";
        }
        if (general.Constants.DEBUG) {
            System.out.println("query: "+expression);
        }
        openConnection(database);
        stmt.execute(expression);
        closeConnection();
    }

    public static void deleteRecordsFromTable(int database, String tableName, ArrayList<String> attributes, ArrayList<String> values, String whereClause) throws Exception {
        String expression = "DELETE FROM "+tableName+" WHERE ";
        if (whereClause != null) {
            expression += whereClause;
        } else {
            if (attributes.size() != values.size()) {
                throw new Exception ("Attributes size does not match values size !");
            }
            for (int currentIndex = 0; currentIndex < values.size(); currentIndex++) {
                expression += attributes.get(currentIndex)+"=\'"+values.get(currentIndex)+"\' AND";
            }
            expression = expression.substring(0,expression.length()-4);
        }
        if (general.Constants.DEBUG) {
            System.out.println("query: "+expression);
        }
        openConnection(database);        
        stmt.execute(expression);
        closeConnection();
    }
    
    public static String executeProcedure(int database, String procedureName, ArrayList<String> parameterTypes, ArrayList<String> parameterValues, ArrayList<Integer> parameterDataTypes) throws SQLException {
        String result = new String();
        int resultIndex = -1;
        openConnection(database);
        String query = "{CALL "+procedureName+"(";
        int parameterNumber = parameterTypes.size();
        for (int count = 1; count <= parameterNumber; count++)
            query += "?, ";
        if (parameterNumber != 0)
            query = query.substring(0,query.length()-2);
        query += ")}";
        CallableStatement cstmt = dbConnection.prepareCall(query);
        int i = 0, j = 0, k = 1;
        for (String parameterType:parameterTypes) {
            switch (parameterType) {
                case "IN":
                    cstmt.setString(k,parameterValues.get(i++));
                    break;
                case "OUT":
                    cstmt.registerOutParameter(k,parameterDataTypes.get(j++).intValue());
                    resultIndex = k;
                    break;
                case "INOUT":
                    cstmt.setString(k,parameterValues.get(i++));
                    cstmt.registerOutParameter(k, parameterDataTypes.get(j++).intValue());
                    resultIndex = k;
                    break;
            }
            k++;
        }
        cstmt.execute();
        result = cstmt.getString(resultIndex);
        closeConnection();
        return result;
    }
    
    public static ArrayList<ArrayList<Object>> executeQuery(int database, String query, int numberOfColumns) throws SQLException {
    	//System.out.println("SQLquery: " + query);
    	openConnection(database);
        ArrayList<ArrayList<Object>> dataBaseContent = new ArrayList<>();
        ResultSet result = stmt.executeQuery(query);  
        int currentRow = 0;
        while (result.next()) {
            dataBaseContent.add(new ArrayList<>());
            for (int currentColumn = 0; currentColumn < numberOfColumns; currentColumn++) {
                dataBaseContent.get(currentRow).add(result.getString(currentColumn+1));
            }
            currentRow++;
        }
        closeConnection();
        return dataBaseContent;
    }
    
    public static void insertQuery(int database, String expression) throws SQLException {
    	//System.out.println("SQLexpression: " + expression);
    	openConnection(database);
        stmt.execute(expression);
        closeConnection();
    }

} 