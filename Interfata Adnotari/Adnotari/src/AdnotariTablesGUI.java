import general.Constants;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.Map.Entry;

import com.sun.javafx.beans.IDProperty;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import dataaccess.DataBaseConnection;
import entities.EntitatiVot;
import entities.EntitatiVotate;
import entities.Entity;


public class AdnotariTablesGUI implements EventHandler, Initializable {
	private Stage								applicationStage;
	private Scene								applicationScene;

	private static int 							idUserLogat;
	private int									idTextDeAdnotat;
	private String 								textDeAdnotat;
	private int									lastIndex;
	private ArrayList<String>					listaEntitatiAdnotate;
	private ArrayList<Double>					noteEntitatiAdnotate;
	private ArrayList<String>					listaEntitatiDeAdnotat;
	
	private String								selectedEntity;

	@FXML private TextArea						campText;
	
	@FXML private TableView<Entity>				tabelEntitatiAdnotate;
	@FXML private TableView<Entity>				tabelEntitatiDeAdnotat;
	
	@FXML private TableColumn<Entity, String>	coloanaEntitatiAdnotate;
	@FXML private TableColumn<Entity, String>	coloanaPolaritati;
	
	@FXML private TableColumn<Entity, String>	coloanaEntitatiDeAdnotat;

	@FXML private Slider						slider1;
	@FXML private Button						startButton;
	@FXML private Button						saveButton;
	@FXML private Button						removeButton;
	@FXML private Button						nextButton;
	@FXML private Label							errorLabel;
	@FXML private TextField						textIndexParagraf;
	
	private boolean 							restoreParagraphId;

	public AdnotariTablesGUI () {

	}

	public AdnotariTablesGUI(int idUserLogat) throws SQLException {
		this.idUserLogat = idUserLogat;
	}
	
	public void getText() throws SQLException {
		errorLabel.setText("");
		listaEntitatiAdnotate = new ArrayList<>();
		noteEntitatiAdnotate = new ArrayList<Double>();
		
		int articleid = 0;
		ArrayList<ArrayList<Object>> resultForText, resultForEntities;
		String textQuery = "SELECT article, articleid FROM articles WHERE id = " + idTextDeAdnotat;
		resultForText = DataBaseConnection.executeQuery(Constants.ADNOTARI_DB, textQuery, 2);
		while (resultForText.isEmpty()) {
			idTextDeAdnotat++;
			textQuery = "SELECT article, articleid FROM articles WHERE id = " + idTextDeAdnotat;
			resultForText = DataBaseConnection.executeQuery(Constants.ADNOTARI_DB, textQuery, 2);
		}
		if (!resultForText.isEmpty()) {
			ArrayList<Object> value = resultForText.get(0);
			textDeAdnotat = value.get(0).toString();
			campText.setText(textDeAdnotat);
			articleid = Integer.parseInt(value.get(1).toString());
		}
		
		String entitiesQuery = "SELECT entity_name, entity_versions from article_entities WHERE articleid = " + articleid;
		resultForEntities = DataBaseConnection.executeQuery(Constants.ADNOTARI_DB, entitiesQuery, 2);
		if (!resultForEntities.isEmpty()) {
			HashMap<String, String> entities = new HashMap<>();
			for (ArrayList<Object> value : resultForEntities) {
				//entities.add(value.get(0).toString());
				entities.put(value.get(0).toString(), value.get(1).toString());
			}
			ArrayList<String> entitiesList = new ArrayList<>();
			Iterator<Entry<String, String>> it = entities.entrySet().iterator();
			while (it.hasNext()) {
				Entry<String, String> entry = it.next();
				entitiesList.add(entry.getKey() + "(" + entry.getValue() + ")");
			}

			//populeaza spatiul cu lista entitatilor neadnotate
			//lista entitatilor neadnotate coincide cu lista tuturor entitatilor la inceput
			listaEntitatiDeAdnotat = new ArrayList<>(entitiesList);
		}
	}
	
	public void getAnnotatedEntities() throws SQLException {
		listaEntitatiAdnotate.clear();
		noteEntitatiAdnotate.clear();
		// gaseste toate entitatile adnotate si polaritatile asociate
		// sterge toate entitatile adnotate din lista entitatilor neadnotate
		String query = "SELECT entityname, polarity FROM asocUserVoteArticleEntity where articleid = " + idTextDeAdnotat + " AND userid = " + idUserLogat;
		ArrayList<ArrayList<Object>> result = DataBaseConnection.executeQuery(Constants.ADNOTARI_DB, query, 2);
		for (ArrayList<Object> value : result) {
			listaEntitatiAdnotate.add(value.get(0).toString());
			listaEntitatiDeAdnotat.remove(value.get(0).toString());
			noteEntitatiAdnotate.add(Double.parseDouble(value.get(1).toString()));
			System.out.println(Double.parseDouble(value.get(1).toString()));
		}
		populateEntitatiAdnotate();
		populateEntitatiDeAdnotat();
	}

	@FXML
	public void getNextText() throws SQLException {
		if (restoreParagraphId) {
			idTextDeAdnotat = lastIndex;
			restoreParagraphId = false;
		}

		/*errorLabel.setText("");
		listaEntitatiAdnotate = new ArrayList<>();
		noteEntitatiAdnotate = new ArrayList<Double>();*/

		idTextDeAdnotat++;
		getText();
		populateEntitatiDeAdnotat();
		populateEntitatiAdnotate();
		/*int articleid = 0;
		ArrayList<ArrayList<Object>> resultForText, resultForEntities;
		String textQuery = "SELECT article, articleid FROM articles WHERE id = " + idTextDeAdnotat;
		resultForText = DataBaseConnection.executeQuery(Constants.ADNOTARI_DB, textQuery, 2);
		if (!resultForText.isEmpty()) {
			ArrayList<Object> value = resultForText.get(0);
			textDeAdnotat = value.get(0).toString();
			campText.setText(textDeAdnotat);
			articleid = Integer.parseInt(value.get(1).toString());
		}
		
		String entitiesQuery = "SELECT entity_name, entity_versions from article_entities WHERE articleid = " + articleid;
		resultForEntities = DataBaseConnection.executeQuery(Constants.ADNOTARI_DB, entitiesQuery, 2);
		if (!resultForEntities.isEmpty()) {
			HashMap<String, String> entities = new HashMap<>();
			for (ArrayList<Object> value : resultForEntities) {
				//entities.add(value.get(0).toString());
				entities.put(value.get(0).toString(), value.get(1).toString());
			}
			ArrayList<String> entitiesList = new ArrayList<>();
			Iterator<Entry<String, String>> it = entities.entrySet().iterator();
			while (it.hasNext()) {
				Entry<String, String> entry = it.next();
				entitiesList.add(entry.getKey() + "(" + entry.getValue() + ")");
			}

			//populeaza spatiul cu lista entitatilor neadnotate
			//lista entitatilor neadnotate coincide cu lista tuturor entitatilor la inceput
			listaEntitatiDeAdnotat = new ArrayList<>(entitiesList);
			populateEntitatiDeAdnotat();
			populateEntitatiAdnotate();
		}*/
	}

	public void start() throws IOException {
		applicationStage = new Stage();
		applicationScene = new Scene((Parent)FXMLLoader.load(getClass().getResource("GUI_Tables.fxml")));
		applicationScene.addEventHandler(EventType.ROOT, (EventHandler<? super Event>)this);
		applicationStage.setTitle("Despre");
		//applicationStage.getIcons().add(new Image(Constants.ICON_FILE_NAME));
		Dimension screenDimension = Toolkit.getDefaultToolkit().getScreenSize();
		applicationStage.setScene(applicationScene);
		applicationStage.show();

	}

	@FXML
	public void removeAnnotation() throws SQLException {
		double sliderValue;

		errorLabel.setText("");

		if (selectedEntity == null) {
			errorLabel.setText("!!! Trebuie selectata o entitate !!!");
			return;
		}

		sliderValue = slider1.getValue();

		if (!annotationExists(selectedEntity, sliderValue)) {
			errorLabel.setText("!!! Adnotare inexistenta !!!");
		} else {
			//sterge adnotarea
			String expression = "DELETE FROM asocUserVoteArticleEntity"
					+ " WHERE "
					+ "userid = " + idUserLogat + " AND "
					+ "articleid = " + idTextDeAdnotat + " AND "
					+ "entityName = \"" + selectedEntity + "\"";

			DataBaseConnection.insertQuery(Constants.ADNOTARI_DB, expression);

			//sterge entitatea din lista de entitati adnotate
			listaEntitatiAdnotate.remove(selectedEntity);
			populateEntitatiAdnotate();
			
			//adauga entitatea in lista de entitati neadnotate
			listaEntitatiDeAdnotat.add(selectedEntity);
			Collections.sort(listaEntitatiDeAdnotat);
			populateEntitatiDeAdnotat();
		}
	}

	@FXML
	public void saveAnnotation() {
		errorLabel.setText("");
		double sliderValue;
		
		if (selectedEntity == null) {
			errorLabel.setText("!!! Trebuie selectata o entitate !!!");
			return;
		}
	
		sliderValue = slider1.getValue();

		try {
			if (!annotationExists(selectedEntity, sliderValue)) {
				String expression = "INSERT INTO asocUserVoteArticleEntity VALUES("
						+ "DEFAULT, "
						+ idUserLogat + ", "
						+ idTextDeAdnotat + ", "
						+ "\"" + selectedEntity + "\", "
						+ sliderValue + ", "
						+ "(SELECT NOW()))";
				//adauga adnotare noua in tabela
				DataBaseConnection.insertQuery(Constants.ADNOTARI_DB, expression);

				listaEntitatiAdnotate.add(selectedEntity);
				noteEntitatiAdnotate.add(sliderValue);

				listaEntitatiDeAdnotat.remove(selectedEntity);

				populateEntitatiAdnotate();
				populateEntitatiDeAdnotat();
			} else {
				String expression = "UPDATE asocUserVoteArticleEntity SET "
						+ "polarity = " + sliderValue + ", "
						+ "vote_date = (SELECT NOW()) "
						+ "WHERE "
						+ "userid = " + idUserLogat + " AND "
						+ "articleid = " + idTextDeAdnotat + " AND "
						+ "entityName = \"" + selectedEntity + "\"";
				//TODO updateaza intrarea deja exisenta in tabela de adnotari
				DataBaseConnection.insertQuery(Constants.ADNOTARI_DB, expression);

				int indexEntitate = listaEntitatiAdnotate.indexOf(selectedEntity);
				noteEntitatiAdnotate.set(indexEntitate, sliderValue);

				populateEntitatiAdnotate();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("Operatia de insert in tabela asocUserVoteArticleEntity a esuat");
		}
	}

	@FXML
	public void onStartButton() {
		listaEntitatiAdnotate = new ArrayList<>();
		noteEntitatiAdnotate = new ArrayList<>();
		listaEntitatiDeAdnotat = new ArrayList<>();

		nextButton.setDisable(false);
		removeButton.setDisable(false);
		saveButton.setDisable(false);
		slider1.setDisable(false);
		startButton.setVisible(false);
		//TODO gaseste articolul la care a ramas acest utilizator
		String query = "SELECT articleid FROM asocUserVoteArticleEntity WHERE userid = " + idUserLogat + " ORDER BY articleid DESC LIMIT 1";
		try {
			ArrayList<ArrayList<Object>> result = DataBaseConnection.executeQuery(Constants.ADNOTARI_DB, query, 1);
			if (result.isEmpty()) {
				// TODO ia primul text din textele de adnotat
				idTextDeAdnotat = 1;
			} else {
				idTextDeAdnotat = Integer.parseInt(result.get(0).get(0).toString()) + 1;
			}
			int articleid = 0;
			ArrayList<ArrayList<Object>> resultForText, resultForEntities;
			String textQuery = "SELECT article, articleid FROM articles WHERE id = " + idTextDeAdnotat;
			resultForText = DataBaseConnection.executeQuery(Constants.ADNOTARI_DB, textQuery, 2);
			while (resultForText.isEmpty()) {
				idTextDeAdnotat++;
				textQuery = "SELECT article, articleid FROM articles WHERE id = " + idTextDeAdnotat;
				resultForText = DataBaseConnection.executeQuery(Constants.ADNOTARI_DB, textQuery, 2);
			}
			if (!resultForText.isEmpty()) {
				ArrayList<Object> value = resultForText.get(0);
				textDeAdnotat = value.get(0).toString();
				campText.setText(textDeAdnotat);
				articleid = Integer.parseInt(value.get(1).toString());
			}
			String entitiesQuery = "SELECT entity_name, entity_versions from article_entities WHERE articleid = " + articleid;
			resultForEntities = DataBaseConnection.executeQuery(Constants.ADNOTARI_DB, entitiesQuery, 2);

			if (!resultForEntities.isEmpty()) {
				HashMap<String, String> entities = new HashMap<>();
				for (ArrayList<Object> value : resultForEntities) {
					//entities.add(value.get(0).toString());
					entities.put(value.get(0).toString(), value.get(1).toString());
				}
				ArrayList<String> entitiesList = new ArrayList<>();
				Iterator<Entry<String, String>> it = entities.entrySet().iterator();
				while (it.hasNext()) {
					Entry<String, String> entry = it.next();
					entitiesList.add(entry.getKey() + "(" + entry.getValue() + ")");
				}
				Collections.sort(entitiesList);

				listaEntitatiDeAdnotat = new ArrayList<>(entitiesList);
				populateEntitatiDeAdnotat();
			}

		} catch (SQLException e) {
			System.out.println("Eroare: nu s-a putut trimite cererea la baza de date");
		}
	}
	
	@FXML
	public void onReportDuplicateButton() {
		
	}

	@Override
	public void handle(Event arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// initializeaza coloanele din cele doua tabele
		initColumnsAnnotatedEntities();
		initColumnsNotAnnotatedEntities();
		
		selectedEntity	= null;
		lastIndex		= -1;
		restoreParagraphId = false;
		
		campText.setEditable(false);
		nextButton.setDisable(true);
		removeButton.setDisable(true);
		saveButton.setDisable(true);
		slider1.setDisable(true);
		campText.setText("Apasa butonul Start pentru a incepe sa adnotezi");
	}

	public boolean annotationExists(String entityName, double sliderValue) {
		String query = "SELECT articleid FROM asocUserVoteArticleEntity WHERE "
				+ "userid = " + idUserLogat + " AND "
				+ "articleid = " + idTextDeAdnotat + " AND "
				+ "BINARY entityName = \"" + entityName + "\"";
		try {
			ArrayList<ArrayList<Object>> result = DataBaseConnection.executeQuery(Constants.ADNOTARI_DB, query, 1);
			if (result.isEmpty()) {
				System.out.println("Nu exista");
				return false;
			}
			else {
				System.out.println("Exista " + entityName);
				return true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	@FXML
	public void rowMouseClickDeAdnotatTable() {
		//extrage numele entitatii
		Entity ent = ((Entity)tabelEntitatiDeAdnotat.getSelectionModel().getSelectedItem());
        ArrayList<String> values = null;
        if (ent != null) {
        	values = ent.getValues();
			selectedEntity = values.get(0);
			System.out.println("selected entity: " + selectedEntity);
        }
	}
	
	@FXML
	public void rowMouseClickAdnotateTable() {
		//extrage numele entitatii
		Entity ent = ((Entity)tabelEntitatiAdnotate.getSelectionModel().getSelectedItem());
        ArrayList<String> values = null;
        if (ent != null) {
        	values = ent.getValues();
			selectedEntity = values.get(0);
			System.out.println("selected entity to remove: " + selectedEntity);
        }
	}
	
	@FXML
	public void mergiLaParagraf() {
		if (textIndexParagraf.getText().isEmpty()) {
			errorLabel.setText("Nu ai precizat indexul!");
			return;
		}
		
		if (!restoreParagraphId) {
			lastIndex = idTextDeAdnotat;
		}
		restoreParagraphId = true;
		idTextDeAdnotat = Integer.parseInt(textIndexParagraf.getText());
		try {
			getText();
			getAnnotatedEntities();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void initColumnsAnnotatedEntities() {
		coloanaEntitatiAdnotate.setCellValueFactory(new PropertyValueFactory<Entity, String>("entitate"));
		coloanaPolaritati.setCellValueFactory(new PropertyValueFactory<Entity, String>("polaritate"));
	}
	
	public void initColumnsNotAnnotatedEntities() {
		coloanaEntitatiDeAdnotat.setCellValueFactory(new PropertyValueFactory<Entity, String>("entitate"));
	}
	
	public void populateEntitatiAdnotate() {
		try {
			ObservableList<Entity> data = FXCollections.observableArrayList();
			int index = 0;
			for (String entitate : listaEntitatiAdnotate) {
				String nota = getNameForAnnotationValue(noteEntitatiAdnotate.get(index));
				data.add(getCurrentEntity(entitate, nota));
				index++;
			}
			tabelEntitatiAdnotate.setItems(data);
		} catch (Exception exception) {
			System.out.println ("exceptie: "+exception.getMessage());
			exception.printStackTrace();
		}
	}
	
	public void populateEntitatiDeAdnotat() {
		try {
			ObservableList<Entity> data = FXCollections.observableArrayList();
			for (String entitate : listaEntitatiDeAdnotat) {
				data.add(getCurrentEntity(entitate));
			}
			tabelEntitatiDeAdnotat.setItems(data);
		} catch (Exception exception) {
			System.out.println ("exceptie: "+exception.getMessage());
			exception.printStackTrace();
		}
	}
	
	private Entity getCurrentEntity(String entitate, String polaritate) {
		return new EntitatiVotate(entitate, polaritate);
	}
	
	private Entity getCurrentEntity(String entitate) {
		return new EntitatiVot(entitate);
	}

	public String getNameForAnnotationValue(Double value) {
		if (value >= -1.1 && value <= -0.99) {
			return "Very Negative";
		}
		if (value >= -0.67 && value <= -0.66) {
			return "Negative";
		}
		if (value >= -0.34 && value <= -0.33) {
			return "Somewhat Negative";
		}
		if (value >= -0.1 && value <= 0.1) {
			return "Neutral";
		}
		if (value >= 0.33 && value <= 0.34) {
			return "Somewhat Positive";
		}
		if (value >= 0.66 && value <= 0.67) {
			return "Positive";
		}
		if (value >= 0.99 && value <= 1.0) {
			return "Very Positive";
		}
		return null;
	}
}
