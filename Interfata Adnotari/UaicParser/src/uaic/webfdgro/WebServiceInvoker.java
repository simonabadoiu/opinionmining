package uaic.webfdgro;

import uaic.webfdgro.FdgParserRoWSStub.ParseText;

public class WebServiceInvoker {

	public String queryServer(String input) throws Exception {
		FdgParserRoWSStub serviceParserStub = new FdgParserRoWSStub();
		
		FdgParserRoWSStub.ParseTextE textParser = new FdgParserRoWSStub.ParseTextE();
		
		ParseText parseText = new ParseText();
		parseText.setTxt(input);
		
		textParser.setParseText(parseText);
		FdgParserRoWSStub.ParseTextResponseE textResponse = serviceParserStub.parseText(textParser);
		
		String output = textResponse.getParseTextResponse().get_return();
		
		return output;
	}
	
	public static void main(String[] args) throws Exception {
		WebServiceInvoker wbs = new WebServiceInvoker();
		System.out.println(wbs.queryServer("Ana are mere și Ionel are pere! Ce frumos este afară."));
	}
}
