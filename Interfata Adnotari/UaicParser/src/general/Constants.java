package general;

public class Constants {
	final public static String      DATABASE_CONNECTION         = "jdbc:mysql://10.6.35.99:3306/adnotari?useUnicode=yes&characterEncoding=utf8";
    final public static String      DATABASE_NAME               = "adnotari";
    final public static String      DATABASE_USER               = "root";
    final public static String      DATABASE_PASSWORD           = "";
    
	public static final boolean 	DEBUG 						= false;
	
	public static final String MYSQL_OPINIONMINING_PROPERTIES_FILEPATH = "properties/mysql-opinionmining-properties.xml";
	
	public enum AffectiveDB {
        SENTICNET, SENTIWN, ANEW
    }
	
	public enum Language {
        RO {
            @Override
            public String toString() {
                return "ro";
            }
        },
        EN {
            @Override
            public String toString() {
                return "en";
            }
        }
    }
}
