package dataaccess;

import general.Constants;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DataBaseConnection {
	public static Connection        dbConnection;
	public static DatabaseMetaData  dbMetaData;
	public static Statement         stmt;

	public DataBaseConnection() { }

	public static void openConnection() throws SQLException {
		dbConnection    = DriverManager.getConnection(Constants.DATABASE_CONNECTION, Constants.DATABASE_USER, Constants.DATABASE_PASSWORD);
		dbMetaData      = dbConnection.getMetaData();
		stmt            = dbConnection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
		return;
	}

	public static void closeConnection() throws SQLException {
		stmt.close();
		dbConnection.close();
	}

	public static ArrayList<ArrayList<Object>> executeQuery(String query, int numberOfColumns) throws SQLException {
		if (Constants.DEBUG)
			System.out.println("SQLquery: " + query);
		openConnection();
		ArrayList<ArrayList<Object>> dataBaseContent = new ArrayList<>();
		ResultSet result = stmt.executeQuery(query);  
		int currentRow = 0;
		while (result.next()) {
			dataBaseContent.add(new ArrayList<>());
			for (int currentColumn = 0; currentColumn < numberOfColumns; currentColumn++) {
				dataBaseContent.get(currentRow).add(result.getString(currentColumn+1));
			}
			currentRow++;
		}
		closeConnection();
		return dataBaseContent;
	}

	public static void insertQuery(String expression) throws SQLException {
		//System.out.println("SQLexpression: " + expression);
		openConnection();
		stmt.execute(expression);
		closeConnection();
	}
}
