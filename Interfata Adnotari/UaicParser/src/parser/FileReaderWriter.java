package parser;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.OutputStreamWriter;

public class FileReaderWriter {
	public static void writeToFile(String fileName, String content) {
		try{
			// Create file 
			FileOutputStream fstream = new FileOutputStream(fileName);
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(fstream, "UTF-8"));
			out.write(content);
			//Close the output stream
			out.close();
		}catch (Exception e){//Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}
	}
}
