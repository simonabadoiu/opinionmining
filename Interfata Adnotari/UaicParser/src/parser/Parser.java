package parser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashSet;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import uaic.webfdgro.WebServiceInvoker;

public class Parser {


	public static void main(String[] args) {
		HashSet<String> uniquePOS = new HashSet<>();
		HashSet<String> uniqueDeprel = new HashSet<>();
		
		int lastIndex = DatabaseParagraphs.getLastIndexProcessed();
		ArrayList<TextAndId> paragraphs = null;
		paragraphs = DatabaseParagraphs.getTextFromDB(lastIndex, 300);

		String xmlFileName = "xmlParseOutput";
		WebServiceInvoker wsi = new WebServiceInvoker();
		// retine un fisier care contine in nume ultimul id la care am ramas
		int lastID = paragraphs.get(paragraphs.size() - 1).getId();
		try {
			File fileToDelete = new File("LastIndex/" + lastIndex);
			fileToDelete.delete();
			PrintWriter writer = new PrintWriter("LastIndex/" + lastID, "UTF-8");
		} catch (FileNotFoundException | UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		for (int k = 0; k < paragraphs.size(); k++) {
			TextSplitter textSplitter = new TextSplitter(paragraphs.get(k).getText());
			ArrayList<String> sentences = textSplitter.getSentences();
			for (int j = 0; j < sentences.size(); j++) {
				String xmlFormat = null;
				String mstOutputFile = new String();
				String text = sentences.get(j);

				try {
					xmlFormat = wsi.queryServer(text);
					FileReaderWriter.writeToFile(xmlFileName, xmlFormat);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				try {
					File xmlFile = new File(xmlFileName);
					DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
					DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
					Document doc = dBuilder.parse(xmlFile);
					doc.getDocumentElement().normalize();

					NodeList nodes = doc.getElementsByTagName("W");

					String allWords = new String();
					String allPos = new String();
					String allDeprel = new String();
					String allHeads = new String();

					for (int i = 0; i < nodes.getLength(); i++) {

						Node node = nodes.item(i);
						NamedNodeMap nnm = node.getAttributes();
						String word = node.getTextContent();
						String pos;
						if (nnm.getNamedItem("POS") == null) {
							pos = "UNKNOWN";
						} else {
							pos = nnm.getNamedItem("POS").getNodeValue();
						}
						String deprel = nnm.getNamedItem("deprel").getNodeValue();
						String head = nnm.getNamedItem("head").getNodeValue();
//						System.out.println(word + ", " + pos + ", " + deprel + ", " + head);
//						System.out.println("=========== " + xmlFileName + " ================");

						allWords += word + "\t";
						allPos += pos + "\t";
						uniquePOS.add(pos);
						allDeprel += deprel + "\t";
						uniqueDeprel.add(deprel);
						allHeads += head + "\t";

						mstOutputFile = "TaggedSentences/mstOutput_" + paragraphs.get(k).getId() + "_" + j + ".txt";
						FileReaderWriter.writeToFile(mstOutputFile,
								allWords.replaceAll("\\s+$", "") + "\n" +
										allPos.replaceAll("\\s+$", "") + "\n" + 
										allDeprel.replaceAll("\\s+$", "") + "\n" + 
										allHeads.replaceAll("\\s+$", ""));
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
		
		FileReaderWriter.writeToFile("partiDeVorbireSiFunctii", uniquePOS + "\n" + uniqueDeprel);

	}

}
