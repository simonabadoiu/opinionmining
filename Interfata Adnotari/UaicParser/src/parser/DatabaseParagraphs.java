package parser;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;

import dataaccess.DataBaseConnection;

public class DatabaseParagraphs {
	public static int getLastIndexProcessed() {
		File folder = new File("LastIndex");
		File[] listOfFiles = folder.listFiles();
		
		if (listOfFiles.length == 0) {
			return 0;
		}

		//the files must be sorted by file name or by date
		File lastFile = listOfFiles[listOfFiles.length - 1];
		
		String lastFileName = lastFile.getName();
		System.out.println(lastFileName);
//		int beginIndex = lastFileName.indexOf("_") + 1;
//		int endIndex = lastFileName.lastIndexOf("_");
//		System.out.println(endIndex + ", " + beginIndex);
//		int lastIndex = Integer.parseInt(lastFileName.substring(beginIndex, endIndex));
		int lastIndex = Integer.parseInt(lastFileName);
		
		System.out.println(lastIndex);
		return lastIndex;
	}

	public static ArrayList<TextAndId> getTextFromDB(int lastIndex, int limit) {
		ArrayList<TextAndId> paragraphs = new ArrayList<>();
		String query = "SELECT id, article FROM articles "
						+ "WHERE id > " + lastIndex + " LIMIT " + limit;
		ArrayList<ArrayList<Object>> result = null;
		try {
			result = DataBaseConnection.executeQuery(query, 2);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (result != null && !result.isEmpty()) {
			for (int i = 0; i < result.size(); i++) {
				ArrayList<Object> value = result.get(i);
				int index = Integer.parseInt(value.get(0).toString());
				String text = value.get(1).toString();
				paragraphs.add(new TextAndId(index, text));
			}
		}
		return paragraphs;
	}
}
