import java.util.HashMap;


public class Test {
	public static void main(String[] args) {
		MyLinkedList linkedList = new MyLinkedList();
		linkedList.addFirst(2);
		linkedList.addFirst(3);
		linkedList.addFirst(15);
		linkedList.addLast(0);
		
		linkedList.print();
		System.out.println();
		
		linkedList.addFirst(12);
		linkedList.addLast(19);
		
		linkedList.print();
		System.out.println();
		
		linkedList.removeFirst();
		linkedList.removeLast();
		linkedList.removeLast();
		linkedList.removeLast();
		linkedList.removeLast();
		linkedList.removeLast();
		
		linkedList.print();
	}
}
