
public class MyLinkedList {
	
	private LinkedListNode first;
	
	public LinkedListNode getfirst() {
		return first;
	}
	
	public MyLinkedList() {
		first = null;
	}
	
	public void addFirst(int value) {
		if (first == null) {
			first = new LinkedListNode(value);
		} else {
			LinkedListNode newNode = new LinkedListNode(value);
			newNode.next = first;
			first = newNode;
		}
	}
	
	
	public void addLast(int value) {
		if (first == null) {
			first = new LinkedListNode(value);
		} else {
			LinkedListNode newNode = new LinkedListNode(value);
			LinkedListNode firstNode = first;
			while (firstNode.next != null) {
				firstNode = firstNode.next;
			}
			firstNode.next = newNode;
		}
	}
	
	public void removeFirst() {
		if (first == null) {
			return;
		}
		first = first.next;
	}
	
	public void removeLast() {
		if (first == null) {
			return;
		}
		if (first.next == null) {
			first = null;
			return;
		}
		LinkedListNode firstNode = first;
		while (firstNode.next.next != null) {
			firstNode = firstNode.next;
		}
		firstNode.next = null;
	}
	
	public void print() {
		LinkedListNode firstNode = first;
		while (firstNode != null) {
			System.out.print(firstNode.value + ", ");
			firstNode = firstNode.next;
		}
	}
}
