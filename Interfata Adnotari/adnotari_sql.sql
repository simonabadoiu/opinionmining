DROP DATABASE adnotari;
CREATE DATABASE adnotari;

USE adnotari;

-- Table structure for table articles

CREATE TABLE articles (
	id int(11) NOT NULL AUTO_INCREMENT,
	article varchar(15000) CHARACTER SET utf8 NOT NULL,
	date_added DATETIME DEFAULT NULL,
	articleid int(11) NOT NULL,
	PRIMARY KEY (id)
	);
	
-- Table structure for table article_entities

CREATE TABLE article_entities (
	id int(11) NOT NULL AUTO_INCREMENT,
	entity_name varchar(500) CHARACTER SET utf8 NOT NULL,
	articleid int(11) NOT NULL,
	PRIMARY KEY (id)
);

-- Table structure for table users

CREATE TABLE IF NOT EXISTS users (
  userid int(11) NOT NULL AUTO_INCREMENT,
  firstname varchar(30) NOT NULL,
  lastname varchar(30) NOT NULL,
  username varchar(30) NOT NULL,
  password varchar(30) NOT NULL,
  PRIMARY KEY (`userid`)
);

-- Table structure for table asocUserVoteArticle
	
/*CREATE TABLE asocUserVoteArticle (
  asocid int(11) NOT NULL AUTO_INCREMENT,
  userid int(11) NOT NULL,
  articleid int(11) NOT NULL,
  articlepolarity decimal(10,2) DEFAULT NULL,
  vote_date datetime DEFAULT NULL,
  PRIMARY KEY (asocid),
  FOREIGN KEY (articleid) REFERENCES articles(id) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (userid) REFERENCES users(userid) ON DELETE CASCADE ON UPDATE CASCADE
);*/

CREATE TABLE asocUserVoteArticleEntity (
  asocid int(11) NOT NULL AUTO_INCREMENT,
  userid int(11) NOT NULL,
  articleid int(11) NOT NULL,
  entityname varchar(500) CHARACTER SET utf8 NOT NULL,
  polarity decimal(10,2) DEFAULT NULL,
  vote_date datetime DEFAULT NULL,
  PRIMARY KEY (asocid),
  FOREIGN KEY (articleid) REFERENCES articles(id) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (userid) REFERENCES users(userid) ON DELETE CASCADE ON UPDATE CASCADE
);

