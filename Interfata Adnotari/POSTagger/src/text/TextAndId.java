package text;

public class TextAndId {
	private int id;
	private String text;
	
	public TextAndId(int id, String text) {
		this.id = id;
		this.text = text;
	}
	
	public int getId() {
		return id;
	}
	
	public String getText() {
		return text;
	}
}
