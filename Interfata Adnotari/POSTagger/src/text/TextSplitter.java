package text;

import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.Locale;

public class TextSplitter {
	String text;
	
	public TextSplitter(String text) {
		this.text = text;
	}
	
	public ArrayList<String> getSentences() {
		ArrayList<String> sentences = new ArrayList<>();
		
		BreakIterator iterator = BreakIterator.getSentenceInstance(Locale.FRENCH);
		String source = text;
		iterator.setText(source);
		int start = iterator.first();
		for (int end = iterator.next();
		    end != BreakIterator.DONE;
		    start = end, end = iterator.next()) {
			sentences.add(source.substring(start,end));
		}
		return sentences;
	}
	
	public static void main(String[] args) {
		TextSplitter textSplitter = new TextSplitter("Ana are mere, pere si alune.");
		System.out.println(textSplitter.getSentences());
	}
	
}
