package text;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.bermuda.ws.WebServiceInvoker;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import sun.security.action.GetLongAction;




public class TextTagger {

	public static String getTaggedText(String input) {
		WebServiceInvoker wsi = new WebServiceInvoker();
		String xmlFormat = null;
		try {
			xmlFormat = wsi.queryServer(input);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return xmlFormat;
	}

	public static String getPOS(String pos) {
		System.out.println("POS: " + pos);
		String partOfSpeech = "substantiv";

		if (pos.startsWith("N")) {
			partOfSpeech = "subst";
			return partOfSpeech;
		}
		if (pos.startsWith("V")) {
			if (pos.charAt(1) == 'm' || pos.charAt(1) == 'o') {
				partOfSpeech = "verb";
			} else {
				if (pos.charAt(1) == 'a') {
					partOfSpeech = "verb aux";
				}
			}
			return partOfSpeech;			
		}
		if (pos.startsWith("A")) {
			partOfSpeech = "adjectiv";
			return partOfSpeech;
		}
		if (pos.startsWith("P")) {
			if (pos.charAt(1) == 'x') {
				partOfSpeech = "pron. reflex.";
				return partOfSpeech;
			}
			if (pos.charAt(1) == 'd') {
				partOfSpeech = "pron. dem.";
				return partOfSpeech;
			}
			partOfSpeech = "pronume";
			return partOfSpeech;
		}
		if (pos.startsWith("D")) {
			if (pos.charAt(1) == 's') {
				partOfSpeech = "art. poses.";
				return partOfSpeech;
			}
			if (pos.charAt(1) == 'i') {
				partOfSpeech = "art. nehot.";
				return partOfSpeech;
			}
			if (pos.charAt(1) == 'd') {
				partOfSpeech = "art. dem.";
				return partOfSpeech;
			}
			partOfSpeech = "art. hot.";
			return partOfSpeech;
		}
		if (pos.startsWith("T")) {
			if (pos.charAt(1) == 'f') {
				partOfSpeech = "art. hot.";
				return partOfSpeech;
			}
			if (pos.charAt(1) == 'i') {
				partOfSpeech = "art. nedef.";
				return partOfSpeech;
			}
			if (pos.charAt(1) == 's') {
				partOfSpeech = "art. poses.";
				return partOfSpeech;
			}
			if (pos.charAt(1) == 'd') {
				partOfSpeech = "art. dem.";
				return partOfSpeech;
			}
		}
		if (pos.startsWith("R")) {
			partOfSpeech = "adverb";
			return partOfSpeech;
		}
		if (pos.startsWith("S")) {
			partOfSpeech = "prepozitie";
			return partOfSpeech;
		}
		if (pos.startsWith("C")) {
			if (pos.charAt(1) == 'c') {
				partOfSpeech = "conj. coord.";
				return partOfSpeech;
			}
			partOfSpeech = "conj. aux.";
			return partOfSpeech;
		}
		if (pos.startsWith("M")) {
			partOfSpeech = "numeral";
			return partOfSpeech;
		}
		if (pos.startsWith("I")) {
			partOfSpeech = "substantiv";
			return partOfSpeech;
		}
		if (pos.startsWith("Y")) {
			if (pos.charAt(1) == 'n') {
				partOfSpeech = "substantiv";
				return partOfSpeech;
			}
			if (pos.charAt(1) == 'v') {
				partOfSpeech = "verb";
				return partOfSpeech;
			}
			if (pos.charAt(1) == 'a') {
				partOfSpeech = "adjectiv";
				return partOfSpeech;
			}
			if (pos.charAt(1) == 'r') {
				partOfSpeech = "adverb";
				return partOfSpeech;
			}
		}
		if (pos.startsWith("Q")) {
			if (pos.charAt(1) == 'z') {
				partOfSpeech = "adverb";
				return partOfSpeech;
			}
			if (pos.charAt(1) == 'n') {
				partOfSpeech = "verb aux.";
				return partOfSpeech;
			}
			if (pos.charAt(1) == 's') {
				partOfSpeech = "conj. aux.";
				return partOfSpeech;
			}
			partOfSpeech = "verb la infinitiv";
			return partOfSpeech;
		}
		partOfSpeech = "substantiv";

		return partOfSpeech;
	}

	public static void getDependencies(String inputFileName, String outputFileName) {
		try {
			String commmand = "java -jar -Xmx3g MSTParser.jar test test-file:" + inputFileName + " model-name:model.romanian output-file:" + outputFileName + " decode-type:non-proj";
			Process p = Runtime.getRuntime().exec(commmand);
			try {
				p.waitFor();
//				p.wait();
//				p.waitFor();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void main(String args[]) {
		int lastIndex = DatabaseParagraphs.getLastIndexProcessed();
		ArrayList<TextAndId> paragraphs = null;
		paragraphs = DatabaseParagraphs.getTextFromDB(lastIndex, 5);

		for (int k = 0; k < paragraphs.size(); k++) {
			TextSplitter textSplitter = new TextSplitter(paragraphs.get(k).getText());
			ArrayList<String> sentences = textSplitter.getSentences();
			for (int j = 0; j < sentences.size(); j++) {
				String mstInputFile = null;
				String mstOutputFile = null;
				
				String words = new String();
				String pos = new String();
				String zeroLine = new String();

				String text = sentences.get(j);
				String xmlFileName = "TaggedSentences/xmlOutput" + paragraphs.get(k).getId() + "_" + j + ".xml";

				String taggedText = getTaggedText(text);
				FileReaderWriter.writeToFile(xmlFileName, taggedText);

				try {
					File xmlFile = new File(xmlFileName);
					DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
					DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
					Document doc = dBuilder.parse(xmlFile);
					doc.getDocumentElement().normalize();

					NodeList nodes = doc.getElementsByTagName("t");

					for (int i = 0; i < nodes.getLength(); i++) {
						Node node = nodes.item(i);
						NamedNodeMap nnm = node.getAttributes();
						String msd = nnm.getNamedItem("MSD").getNodeValue();
						String word = nnm.getNamedItem("word").getNodeValue();
						System.out.println("=========== " + xmlFileName + " ================");
						String partOfSpeech = getPOS(msd);
						if (!msd.equals("QUEST") && !msd.equals("PERIOD") && !msd.equals("EXCL")) {
							if (i != 0) {
								words += "\t";
								pos += "\t";
								zeroLine += "\t";
							}
							words += word;
							pos += partOfSpeech;
							zeroLine += "0";
						}
						mstInputFile = "TaggedSentences/mstInput_" + paragraphs.get(k).getId() + "_" + j + ".txt";
						mstOutputFile = "TaggedSentences/mstOutput_" + paragraphs.get(k).getId() + "_" + j + ".txt";
						FileReaderWriter.writeToFile(mstInputFile, words + "\n" + pos + "\n" + zeroLine + "\n" + zeroLine);
					}
					getDependencies(mstInputFile, mstOutputFile);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}


		//		getDependencies("mstInput.txt", "mstOut.txt");
	}
}
