package ro.racai.ws.pdk.ttlws;

import ro.racai.ws.pdk.ttlws.TTLStub.SentenceSplitter;
import ro.racai.ws.pdk.ttlws.TTLStub.SentenceSplitterResponse;
import ro.racai.ws.pdk.ttlws.TTLStub.Tagger;
import ro.racai.ws.pdk.ttlws.TTLStub.TaggerResponse;
import ro.racai.ws.pdk.ttlws.TTLStub.TokenizerResponse;

public class WebServiceInvoker {
	
	class TTLCallbackHandlerStub extends TTLCallbackHandler {
		
	}
	
	public String queryServer(String input) throws Exception {
		TTLStub serviceTextStub = new TTLStub();
//		NlpStub serviceTextStub = new NlpStub();
//		TextProcessingWebServiceStub.Process process = new TextProcessingWebServiceStub.Process();
//		ProcessTextE process = new NlpStub.ProcessTextE();
		TTLStub.Tokenizer tokenizer = new TTLStub.Tokenizer();
		SentenceSplitter sentenceSplitter = new SentenceSplitter();
//		TextProcessingWebServiceStub.ProcessResponse processResp = new TextProcessingWebServiceStub.ProcessResponse();
//		NlpStub.ProcessTextResponseE processResp = new NlpStub.ProcessTextResponseE();
		TaggerResponse processResp = new TaggerResponse();
		TokenizerResponse tokenizerResponse = new TokenizerResponse();
		SentenceSplitterResponse sentenceSplitterResponse = new SentenceSplitterResponse();
		String output = null;
		
		sentenceSplitter.setInstr(input);
		sentenceSplitter.setLang("ro");
 
//		process.setProcessText(pt);
		/*System.out.println("Input: " + input);
		UTF8ToSGML sgml = new UTF8ToSGML();
		sgml.setInstr(input);
		System.out.println("SGML: " + serviceTextStub.uTF8ToSGML(sgml));
		tagger.setInstr(input);
		tagger.setLang("ro");*/
//		process.setInput(o"nput);
//		process.setLang("ro");
//		processResp = serviceTextStub.process(process);
//		TTLCallbackHandler ttl = new TTLCallbackHandler() {
//		};
//			serviceTextStub.starttagger(tagger, ttl);
		/*processResp = serviceTextStub.tagger(tagger);
		output = processResp.getTaggerReturn();*/
		
	/*	tokenizerResponse = serviceTextStub.tokenizer(tokenizer);
		output = tokenizerResponse.getTokenizerReturn();*/
		
		sentenceSplitterResponse = serviceTextStub.sentenceSplitter(sentenceSplitter);
		output = sentenceSplitterResponse.getSentenceSplitterReturn();
		
		return output;
	}
	
	public static void main(String[] args) throws Exception {
		WebServiceInvoker wbs = new WebServiceInvoker();
		System.out.println(wbs.queryServer("Ana are mere!"));
	}
}
