
/**
 * TTLCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */

    package ro.racai.ws.pdk.ttlws;

    /**
     *  TTLCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class TTLCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public TTLCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public TTLCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for lemmatizer method
            * override this method for handling normal response from lemmatizer operation
            */
           public void receiveResultlemmatizer(
                    ro.racai.ws.pdk.ttlws.TTLStub.LemmatizerResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from lemmatizer operation
           */
            public void receiveErrorlemmatizer(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for sGMLtoUTF7 method
            * override this method for handling normal response from sGMLtoUTF7 operation
            */
           public void receiveResultsGMLtoUTF7(
                    ro.racai.ws.pdk.ttlws.TTLStub.SGMLtoUTF7Response result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from sGMLtoUTF7 operation
           */
            public void receiveErrorsGMLtoUTF7(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for tagger method
            * override this method for handling normal response from tagger operation
            */
           public void receiveResulttagger(
                    ro.racai.ws.pdk.ttlws.TTLStub.TaggerResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from tagger operation
           */
            public void receiveErrortagger(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for sGMLtoUTF8 method
            * override this method for handling normal response from sGMLtoUTF8 operation
            */
           public void receiveResultsGMLtoUTF8(
                    ro.racai.ws.pdk.ttlws.TTLStub.SGMLtoUTF8Response result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from sGMLtoUTF8 operation
           */
            public void receiveErrorsGMLtoUTF8(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for tokenizer method
            * override this method for handling normal response from tokenizer operation
            */
           public void receiveResulttokenizer(
                    ro.racai.ws.pdk.ttlws.TTLStub.TokenizerResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from tokenizer operation
           */
            public void receiveErrortokenizer(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for xCES method
            * override this method for handling normal response from xCES operation
            */
           public void receiveResultxCES(
                    ro.racai.ws.pdk.ttlws.TTLStub.XCESResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from xCES operation
           */
            public void receiveErrorxCES(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for sentenceSplitter method
            * override this method for handling normal response from sentenceSplitter operation
            */
           public void receiveResultsentenceSplitter(
                    ro.racai.ws.pdk.ttlws.TTLStub.SentenceSplitterResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from sentenceSplitter operation
           */
            public void receiveErrorsentenceSplitter(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for uTF8ToSGML method
            * override this method for handling normal response from uTF8ToSGML operation
            */
           public void receiveResultuTF8ToSGML(
                    ro.racai.ws.pdk.ttlws.TTLStub.UTF8ToSGMLResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from uTF8ToSGML operation
           */
            public void receiveErroruTF8ToSGML(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for chunker method
            * override this method for handling normal response from chunker operation
            */
           public void receiveResultchunker(
                    ro.racai.ws.pdk.ttlws.TTLStub.ChunkerResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from chunker operation
           */
            public void receiveErrorchunker(java.lang.Exception e) {
            }
                


    }
    