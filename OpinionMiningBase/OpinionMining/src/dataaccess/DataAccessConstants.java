package dataaccess;

public class DataAccessConstants {
	final public static String      LOCALHOST_CONNECTION_OPINION_MINING		= "jdbc:mysql://localhost:3306/opinion_mining?useUnicode=yes&characterEncoding=utf8";
    final public static String      LOCALHOST_DATABASE_NAME_OPINION_MINING	= "opinion_mining";
    final public static String      LOCALHOST_DATABASE_USER               	= "root";
    final public static String      LOCALHOST_DATABASE_PASSWORD           	= "root";
    
    final public static String      REMOTE_CONNECTION_ADNOTARI				= "jdbc:mysql://10.6.35.99:3306/adnotari?useUnicode=yes&characterEncoding=utf8";
    final public static String      REMOTE_DATABASE_NAME_ADNOTARI			= "adnotari";
    
    final public static String      REMOTE_DATABASE_USER 	              	= "root";
    final public static String      REMOTE_DATABASE_PASSWORD           		= "";
    

    final public static String      REMOTE_CONNECTION_OPINION_MINING		= "jdbc:mysql://10.6.35.99:3306/opinion_mining?useUnicode=yes&characterEncoding=utf8";
    final public static String      REMOTE_DATABASE_NAME_OPINION_MINING		= "opinion_mining";
    
    final public static int			WORDS_DB					= 1;
    final public static int			ADNOTARI_DB					= 2;
    final public static int			ENTITATI_ARTICLES_DB		= 3;
    
    final public static String		ADJECTIVES_RO_SCORES					= "adjectives_ro_scores";
    final public static String		ADVERBS_RO_SCORES						= "adverbs_ro_scores";
    final public static String		VERBS_RO_SCORES							= "verbs_ro_scores";
    final public static String		NOUNS_RO_SCORES							= "nouns_ro_scores";
    
	public static final boolean 	DEBUG 						= false;
	
	public static final String MYSQL_OPINIONMINING_PROPERTIES_FILEPATH = "properties/mysql-opinionmining-properties.xml";
	
	public enum AffectiveDB {
        SENTICNET, SENTIWN, ANEW
    }
	
	public enum Language {
        RO {
            @Override
            public String toString() {
                return "ro";
            }
        },
        EN {
            @Override
            public String toString() {
                return "en";
            }
        }
    }
}
