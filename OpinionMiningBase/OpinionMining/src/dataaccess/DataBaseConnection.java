package dataaccess;


import java.sql.*;
import java.util.ArrayList;

public class DataBaseConnection {
    public static Connection        dbConnection;
    public static DatabaseMetaData  dbMetaData;
    public static Statement         stmt;

    public DataBaseConnection() { }

    public static void openConnection(int database) throws SQLException {
        switch(database) {
        case DataAccessConstants.WORDS_DB:
        	dbConnection    = DriverManager.getConnection(DataAccessConstants.LOCALHOST_CONNECTION_OPINION_MINING, DataAccessConstants.LOCALHOST_DATABASE_USER, DataAccessConstants.LOCALHOST_DATABASE_PASSWORD);
	        dbMetaData      = dbConnection.getMetaData();
	        stmt            = dbConnection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        	break;
        case DataAccessConstants.ADNOTARI_DB:
        	dbConnection    = DriverManager.getConnection(DataAccessConstants.REMOTE_CONNECTION_ADNOTARI, DataAccessConstants.REMOTE_DATABASE_USER, DataAccessConstants.REMOTE_DATABASE_PASSWORD);
	        dbMetaData      = dbConnection.getMetaData();
	        stmt            = dbConnection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        	break;
        case DataAccessConstants.ENTITATI_ARTICLES_DB:
        	dbConnection    = DriverManager.getConnection(DataAccessConstants.REMOTE_CONNECTION_OPINION_MINING, DataAccessConstants.REMOTE_DATABASE_USER, DataAccessConstants.REMOTE_DATABASE_PASSWORD);
	        dbMetaData      = dbConnection.getMetaData();
	        stmt            = dbConnection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        	break;
        default:
        	break;
        }
    }

    public static void closeConnection() throws SQLException {
        stmt.close();
        dbConnection.close();
    }  
    
    public static String executeProcedure(int database, String procedureName, ArrayList<String> parameterTypes, ArrayList<String> parameterValues, ArrayList<Integer> parameterDataTypes) throws SQLException {
        String result = new String();
        int resultIndex = -1;
        openConnection(database);
        String query = "{CALL "+procedureName+"(";
        int parameterNumber = parameterTypes.size();
        for (int count = 1; count <= parameterNumber; count++)
            query += "?, ";
        if (parameterNumber != 0)
            query = query.substring(0,query.length()-2);
        query += ")}";
        CallableStatement cstmt = dbConnection.prepareCall(query);
        int i = 0, j = 0, k = 1;
        for (String parameterType:parameterTypes) {
            switch (parameterType) {
                case "IN":
                    cstmt.setString(k,parameterValues.get(i++));
                    break;
                case "OUT":
                    cstmt.registerOutParameter(k,parameterDataTypes.get(j++).intValue());
                    resultIndex = k;
                    break;
                case "INOUT":
                    cstmt.setString(k,parameterValues.get(i++));
                    cstmt.registerOutParameter(k, parameterDataTypes.get(j++).intValue());
                    resultIndex = k;
                    break;
            }
            k++;
        }
        cstmt.execute();
        result = cstmt.getString(resultIndex);
        closeConnection();
        return result;
    }
    
    public static ArrayList<ArrayList<Object>> executeQuery(int database, String query, int numberOfColumns) throws SQLException {
    	if (DataAccessConstants.DEBUG)
    		System.out.println("SQLquery: " + query);
    	openConnection(database);
        ArrayList<ArrayList<Object>> dataBaseContent = new ArrayList<>();
        ResultSet result = stmt.executeQuery(query);  
        int currentRow = 0;
        while (result.next()) {
            dataBaseContent.add(new ArrayList<>());
            for (int currentColumn = 0; currentColumn < numberOfColumns; currentColumn++) {
                dataBaseContent.get(currentRow).add(result.getString(currentColumn+1));
            }
            currentRow++;
        }
        closeConnection();
        return dataBaseContent;
    }
    
    public static void insertQuery(int database, String expression) throws SQLException {
    	if (DataAccessConstants.DEBUG)
    		System.out.println("SQLexpression: " + expression);
    	openConnection(database);
        stmt.execute(expression);
        closeConnection();
    }

} 