package naivemethod;

import java.io.StringReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.TreeMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;


//import webservices.org.bermuda.ws.WebServiceInvoker;
import uaic.webfdgro.WebServiceInvoker;
import dataaccess.DataAccessConstants;
import dataaccess.DataBaseConnection;
import dbdump.DbDump;
import general.ArticleAndEntities;
import general.WordPolarity;

public class NaiveMethodOpinionExtractor {
	public static final int SIDE_WORDS = 30;
	
	String text;
	ArrayList<String> entities;

	public NaiveMethodOpinionExtractor() {

	}

	public NaiveMethodOpinionExtractor(ArticleAndEntities articleAndEntities) {
		this.text = articleAndEntities.getText();
		this.entities = articleAndEntities.getEntitati();
	}

	/**
	 * parcurge toate cuvintele din text(forma de baza a cuvintelor) si
	 * calculeaza un scor total facand suma polaritatilor fiecarui cuvant
	 */
	public void computeNaivePolarity() {

	}

	public void computePolarityForEntity(String entityName) throws Exception {
		ArrayList<String> listOfEntityVersions = extractEntityVersions(entityName);
		String baseWordsArticle = getBaseWordsArticle();

		ArrayList<WordPos> entityWordPos = new ArrayList<>();
		ArrayList<WordPos> wordsPos = new ArrayList<>();
		
		TreeMap<Integer, Integer> entityOccs = entityOccurences(entityName, baseWordsArticle);
		wordsPos = computeWordsPolarity(baseWordsArticle, entityOccs, entityWordPos);
		
		ArrayList<Double> polaritySum = new ArrayList<>();
		ArrayList<Double> weightSum = new ArrayList<>();
		for (int i = 0; i < entityWordPos.size(); i++) {
			polaritySum.add(0.0);
			weightSum.add(0.0);
		}
		for (WordPos wp : wordsPos) {
			for (int i = 0; i < entityWordPos.size(); i++) {
				double sum = polaritySum.get(i);
				double weight = weightSum.get(i);
				
				int wordIndex = wp.pos;
				int entityIndex = entityWordPos.get(i).pos;
				
				int dist = Math.abs(wordIndex - entityIndex);
				
				if (dist <= SIDE_WORDS) {
					sum += wp.polarity.senticnet*Math.log(dist*2);
					weight += Math.log(dist*2);
					polaritySum.set(i, sum);
					weightSum.set(i, weight);
				}
			}
		}
		
		for (int i = 0; i < polaritySum.size(); i++) {
			System.out.println(polaritySum.get(i)/weightSum.get(i));
		}
	}

	public TreeMap<Integer, Integer> entityOccurences(String entity, String article) throws SQLException {
		System.out.println(article);
		System.out.println(entity);
		int lastIndex = 0;
		int count = 0;
		ArrayList<String> entitieVersions = extractEntityVersions(entity);
		//retine pozitia entitatii in text si lungimea acesteia, iar restul textului este impartit in cuvinte
		TreeMap<Integer, Integer> entityPos = new TreeMap<>();
		System.out.println("Versions: " + entitieVersions);
		for (String ent : entitieVersions) {
			lastIndex = 0;
			while(lastIndex != -1){

				lastIndex = article.indexOf(ent, lastIndex);

				//imparte in cuvinte textul care se gaseste inainte de last index
				//String[] words = text.split("[\\p{IsPunctuation}\\p{IsWhite_Space}]+");

				if (lastIndex != -1) {
					entityPos.put(lastIndex, ent.length());
					count++;
					lastIndex += entity.length();
				}
			}
		}

		System.out.println("tree: " + entityPos);
		return entityPos;
	}

	/**
	 * 
	 * @param article - textul articolului	
	 * @param entityPos - pozitiile si lungimea entitatilor din text
	 * @return un ArrayList in care se gaseste: cuvantul, numarul de ordin in propozitie, polaritatea
	 * @throws SQLException 
	 */
	public ArrayList<WordPos> computeWordsPolarity(String article, TreeMap<Integer, Integer> entityPos, ArrayList<WordPos> entityWordPos) throws SQLException {
		//create dbDump
		ArrayList<String> columns = new ArrayList<>();
		columns.add("rophrase");
		columns.add("senticnet");
		columns.add("sentiwordnet");
		columns.add("anew");
		HashMap<String, WordPolarity> dbDump = DbDump.dump(DataAccessConstants.WORDS_DB, DataAccessConstants.ADJECTIVES_RO_SCORES, columns);

		String articleSubstr = new String();
		ArrayList<WordPos> articleWords = new ArrayList<>();
		
		int word_pos = 0;
		int beginPos = 0;
		
		String art = new String(article);
		for(Map.Entry<Integer,Integer> entry : entityPos.entrySet()) {
			Integer index = entry.getKey();
			Integer length = entry.getValue();

			articleSubstr = art.substring(beginPos, index);
			String[] words = articleSubstr.split("[\\p{IsPunctuation}\\p{IsWhite_Space}]+");
			for (int i = 0; i < words.length; i++) {
//				System.out.println(words[i]);
				WordPolarity polarity = dbDump.containsKey(words[i]) ? dbDump.get(words[i]) : null;
				if (polarity != null)
					articleWords.add(new WordPos(words[i], word_pos, polarity));
				word_pos++;
			}
			//numara si cuvantul care reprezinta entitatea
			entityWordPos.add(new WordPos(art.substring(index, index+length), word_pos, null));
			word_pos++;
			//articleSubstr = art.substring(index + length);
			beginPos = index+length;
		}
		articleSubstr = art.substring(beginPos);
		String[] words = articleSubstr.split("[\\p{IsPunctuation}\\p{IsWhite_Space}]+");
		for (int i = 0; i < words.length; i++) {
//			System.out.println(words[i]);
			WordPolarity polarity = dbDump.containsKey(words[i]) ? dbDump.get(words[i]) : null;
			if (polarity != null)
				articleWords.add(new WordPos(words[i], word_pos, polarity));
			word_pos++;
		}
		return articleWords;
	}
	
	public Double getPolarity(String columnName, HashMap<String, WordPolarity> dbDump, String word) {
		if (!dbDump.containsKey(word))
			return null;
		if (columnName.equals("senticnet"))
			return dbDump.get(word).senticnet;
		
		if (columnName.equals("sentiwordnet"))
			return dbDump.get(word).sentiwordnet;
		
		if (columnName.equals("anew"))
			return dbDump.get(word).anew;
		
		System.err.println("Field nod existent!");
		return null;
	}

	public String getBaseWordsArticle() throws Exception {
		WebServiceInvoker wsi = new WebServiceInvoker();
		String xml = wsi.queryServer(text);
		//		System.out.println(xml);
		Document doc = loadXMLFromString(xml);
		doc.getDocumentElement().normalize();

		NodeList nodes = doc.getElementsByTagName("W");
		String baseWordsarticle = new String();
		String article = new String();
		for (int i = 0; i < nodes.getLength(); i++) {
			Node node = nodes.item(i);
			NamedNodeMap nnm = node.getAttributes();
			String lemma = nnm.getNamedItem("LEMMA").getNodeValue();
			//			String word = node.getTextContent();
			baseWordsarticle += lemma + " ";
			//			article += word + " ";
		}
		//		System.out.println(article);
		//		System.out.println(baseWordsarticle);
		return baseWordsarticle;
	}

	public static Document loadXMLFromString(String xml) throws Exception
	{
//		System.out.println(xml);
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		InputSource is = new InputSource(new StringReader(xml));
		return builder.parse(is);
	}

	/**
	 * 
	 * @param entity entitatea pentru care vrem sa calculam opinia
	 * @return toate versiunile pentru entitatea respectiva
	 * @throws SQLException
	 */
	private static ArrayList<String> extractEntityVersions(String entity) throws SQLException {
		HashSet<String> versions = new HashSet<>();

		entity = entity.replace("ş", "ș");
		String query = "SELECT versiuni FROM clasificator3 "
				+ "WHERE nume = _utf8\"" + entity + "\"";

		int numberOfColumns = 1;
		ArrayList<ArrayList<Object>> result = DataBaseConnection.executeQuery(DataAccessConstants.ENTITATI_ARTICLES_DB, query, numberOfColumns);

		for (int i = 0; i < result.size(); i++) {
			ArrayList<Object> value = result.get(i);
			String[] entityVersions = value.get(0).toString().split(", "); 
			for (int j = 0; j < entityVersions.length; j++) {
				versions.add(entityVersions[j].trim().replaceAll("\\xa0+$", "").toLowerCase());
			}
		}

		ArrayList<String> listOfVersions = new ArrayList<>(versions);
		return listOfVersions;
	}

	public static void main(String[] args) throws Exception {
		NaiveMethodTextExtractor nmte = new NaiveMethodTextExtractor();
		ArticleAndEntities article = nmte.extractArticle("http://www.mediafax.ro/stiinta-sanatate/celulele-stem-pot-incetini-evolutia-bolilor-alzheimer-si-parkinson-11626028");	// foarte negativ
//		ArticleAndEntities article = nmte.extractArticle("http://sport.hotnews.ro/stiri-sport-7004954-oscar-2010-sport-schumacher-favorit-filmul-banii-fugi-caster-semenya-nominalizata-cea-mai-buna-actrita-pentru-baietii-nu-plang.htm");	// foarte pozitiv
//		ArticleAndEntities article = nmte.extractArticle("http://www.gandul.info/stiri/gandul-live-lista-de-medicamente-compensate-din-romania-nu-a-mai-fost-actualizata-de-mai-bine-de-5-ani-cine-este-vinovatul-11728735");	//Austria pozitiv
		NaiveMethodOpinionExtractor nmoe = new NaiveMethodOpinionExtractor(article);
		System.out.println(article.getEntitati());
		System.out.println(article.getText());
		/*nmoe.computePolarityForEntity(article.getEntitati().get(8));
		
		nmoe.entityOccurences(article.getEntitati().get(8), article.getText());
		ArrayList<WordPos> entityWordPos = new ArrayList<>();
		System.out.println(nmoe.computeWordsPolarity(article.getText(), nmoe.entityOccurences(article.getEntitati().get(8), article.getText()), entityWordPos));
		System.out.println(entityWordPos);*/
		nmoe.computePolarityForEntity(article.getEntitati().get(11));
//		nmoe.computePolarityForEntity(article.getEntitati().get(9));
//		nmoe.computePolarityForEntity(article.getEntitati().get(25));
	}
}

class WordPos {
	public String word;
	public int pos;
	public WordPolarity polarity;
	
	public WordPos(String word, int pos, WordPolarity polarity) {
		this.word = word;
		this.pos = pos;
		this.polarity = polarity;
	}
	
	@Override
	public String toString() {
		return word + ": " + pos + ", " + polarity;
	}
}
