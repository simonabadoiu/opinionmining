package naivemethod;

import general.ArticleAndEntities;

public class NaiveMethodMain {

	public static void main(String[] args) throws Exception {
		String url = args[0];
		String entity = args[1];
		String articleText = NaiveMethodTextExtractor.extractArticleFromSolr(url);
		ArticleAndEntities articleAndEntity = new ArticleAndEntities(articleText, null);
		NaiveMethodOpinionExtractor nmoe = new NaiveMethodOpinionExtractor(articleAndEntity);
		nmoe.computePolarityForEntity(entity);
	}

}
