package naivemethod;

import general.ArticleAndEntities;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.lucene.search.MultiTermQuery.RewriteMethod;

import solraccess.SolrDocs;

/**
 * Servlet implementation class NaiveMethodServlet
 */
public class NaiveMethodServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public NaiveMethodServlet() {
        // TODO Auto-generated constructor stub
    	System.out.println("Aici");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String articleText = null;
		Double polarity = null;
		
		System.out.println("get");
		String entity = request.getParameter("entityName");
		System.out.println(entity);
		String url = request.getParameter("url");
		System.out.println(url);
		
		articleText = NaiveMethodTextExtractor.extractArticleFromSolr(url);
		NaiveMethodOpinionExtractor naiveMethodOpinionExtractor = new NaiveMethodOpinionExtractor(articleText);
		try {
			polarity = naiveMethodOpinionExtractor.computePolarityForEntity(entity);
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("Nu s-a putut calcula polaritatea entitatii! Este posibil ca serviciul web de la uaic sa nu functioneze!");
		}
		
		response.setContentType("text/xml;charset=UTF-8");
		PrintWriter out = response.getWriter();
		out.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
	    out.append("<response>");
	    out.append(polarity.toString());
        out.append("</response>");
        out.close();
        System.out.println(polarity);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Ruleaza");
		
	}

}
