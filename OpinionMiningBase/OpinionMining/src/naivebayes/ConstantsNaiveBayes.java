package naivebayes;

/**
 * 
 * @author andreea.badoiu
 * Here are defined the classes for NaiveBayes algorithm implementation
 */
public class ConstantsNaiveBayes {
	
	/**
	 * 
	 * @author andreea.badoiu
	 * There are two classes a text can be part of - Positive and Negative classes
	 */
	public enum OpinionClass {
		POSITIVE {
			public String toString() {
				return "Positive";
			}
		},
		
		NEGATIVE {
			public String toString() {
				return "Negative";
			}
		}
	}
}
