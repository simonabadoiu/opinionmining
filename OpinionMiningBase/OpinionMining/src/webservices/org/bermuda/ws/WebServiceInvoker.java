package webservices.org.bermuda.ws;

import webservices.org.bermuda.ws.NlpStub.ProcessText;
import webservices.org.bermuda.ws.NlpStub.ProcessTextE;
import webservices.org.bermuda.ws.NlpStub.ProcessTextResponse;

public class WebServiceInvoker {
	
	public String queryServer(String input) throws Exception {
		NlpStub serviceTextStub = new NlpStub();
		ProcessTextE process = new NlpStub.ProcessTextE();
		NlpStub.ProcessTextResponseE processResp = new NlpStub.ProcessTextResponseE();
		ProcessTextResponse output = null;
	 
		try {
			ProcessText pt = new ProcessText();
			pt.setText(input);
			pt.setModel("ro");
			process.setProcessText(pt);
			processResp = serviceTextStub.processText(process);
			output = processResp.getProcessTextResponse();
		} catch (org.apache.axis2.AxisFault e) {
			System.out.println(e.toString());
		}
		
		return output.get_return();
	}
	
	public static void main(String[] args) throws Exception {
		WebServiceInvoker wbs = new WebServiceInvoker();
		System.out.println(wbs.queryServer("Cel mai bine se descurca Traian Băsescu."));
	}
}
