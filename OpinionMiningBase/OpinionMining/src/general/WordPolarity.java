package general;

public class WordPolarity {
	public double senticnet;
	public double sentiwordnet;
	public double anew;
	
	public WordPolarity(double senticnet, double sentiwordnet, double anew) {
		this.senticnet = senticnet;
		this.sentiwordnet = sentiwordnet;
		this.anew = anew;
	}
	
	@Override
	public String toString() {
		return "Senticnet: " + senticnet + " Sentiwordnet: " + sentiwordnet + " Anew: " + anew;
	} 
}
