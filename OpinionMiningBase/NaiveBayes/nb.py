from sklearn.naive_bayes import GaussianNB
from sklearn import datasets

def main():
    iris = datasets.load_iris()
    gnb = GaussianNB()
    y_pred = gnb.fit(iris.data, iris.target).predict(iris.data)
    print("Number of mislabeled points : %d" % (iris.target != y_pred).sum())
    
if __name__=="__main__":
    main()